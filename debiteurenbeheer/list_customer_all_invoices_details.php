<?php
// error_reporting(E_ALL);
// Start API
require_once "wefact_api.php";
setlocale(LC_ALL, 'nl_NL');
$api = new WeFactAPI();
?>
<html>
<head>
    <style type="text/css">
        body,* {font-size:15px;font-family:Arial;}
        th {border-bottom:1px solid #ccc;}
        textarea {padding:10px;}
        small {font-size:12px;}
        .odd{
            background:#ddd;
        }
        .debtor-select {
            padding:20px;
            background:#ddd;
            margin-bottom:30px;
            width:400px;
        }
    </style>
</head>
<body>
<div class="debtor-select">
    <h3>Toon alle facturen inc. details voor een debiteur</h3>
    <form action="" method="post" onchange="this.submit()">
        <select name="debtor">
        <?php
            $params = array();
            $customers = $api->sendRequest('debtor', 'list', $params);
            
            foreach($customers as $cust) {
                foreach($cust as $cu) {
                    $debtorCode = $cu['DebtorCode'];
                    $company = $cu['CompanyName'];
                    if ($debtorCode == $_POST['debtor']){$selected = 'selected="selected"';} else {$selected = '';}
                    echo '<option '.$selected.' value="'.$debtorCode.'">' . $company . '</option>';
                }
            }
        ?>
        </select>    
    </form>
</div>
<?php    
if (isset($_POST['debtor'])) {
    $company = $_POST['debtor'];
    $parameters = array(
        "sort" => 'InvoiceCode',
        "order" => 'DESC',
        "searchat" => 'DebtorCode',
        "searchfor" => $company
        );
    $invoices_list = $api->sendRequest('invoice', 'list', $parameters);
    if ($invoices_list['status'] == 'success') {
        
        echo "
        <table width=100%' align='center' cellspacing='0' cellpadding='20px' style='padding-bottom:20px;' >
            <thead>
                <th height='80px' width='15%'>Factuur nummer</th>
                <th height='80px' width='15%'>Factuur datum</th>
                <th height='80px' width='15%'>Bedrag inc. BTW</th>
                <th height='80px' width='55%'>Details</th>
            </thead>
            <tbody>
        ";  
        
        
        $invoices = $invoices_list['invoices'];
        $nr = 0;
        foreach($invoices as $invoice) { $nr++;
            if ($nr % 2 == 0) {$order = 'even';} else {$order = 'odd';}
            $ID = $invoice['InvoiceCode'];
            $factuurdatum = $invoice['Date'];
            $factuurdatum_nice = strftime("%e %B %Y", strtotime($factuurdatum));
            $bedrag = $invoice['AmountIncl'];
            $invoicedetails = $api->sendRequest('invoice', 'show', array('InvoiceCode' => $invoice['InvoiceCode']));
            if ($invoicedetails['status']=='success') {
                $description = "";
                $invoiceLines = $invoicedetails['invoice']['InvoiceLines'];
                foreach($invoiceLines as $line) {
                    $description .= $line['Description'] . '<br /><br />';
                    
                }
            }
            
            echo "
                <tr height=\"55\" class=\"$order\">
                    <td align=\"center\">
                        {$ID}
                    </td>
                    <td align=\"center\">
                        {$factuurdatum_nice} 
                    </td>
                    <td align=\"center\">
                        &euro; {$bedrag}
                    </td>
                    <td align=\"center\">
                        {$description}
                    </td>
                    
                </tr>
                ";
            
        }
    }
}
?>