<?php
include_once('../wp-includes/class-phpmailer.php');
require_once "wefact_api.php";
setlocale(LC_ALL, 'nl_NL');
date_default_timezone_set('europe/amsterdam');
function check_ssl_abos() {
    $api = new WeFactAPI();
    // get SSL abos
    $abos = $api->sendRequest('subscription', 'list', array('status' => 'active','searchat' => 'ProductCode', 'searchfor' => 'P0016'));
    if($abos['status'] == 'success') {
        foreach($abos['subscriptions'] as $abo) {
            if($abo['ProductCode'] == 'P0016') { //ssl
                $debtorcode = $abo['DebtorCode'];
                $debtordetails = $api->sendRequest('debtor', 'show', array(
                    'DebtorCode' => $debtorcode
                ));
                $websites = $debtordetails['debtor']['CustomFields']['websites1urlperregel']; 
                $aanhef = $debtordetails['debtor']['InvoiceSex'];
                if (empty($aanhef)) {$aanhef = $debtordetails['debtor']['Sex'];}
                if ($aanhef == 'm'){$aanhef  = 'heer';}
                elseif ($aanhef == 'f'){$aanhef = 'mevrouw';}
                elseif ($aanhef == 'd'){$aanhef = 'heer/mevrouw';} 
                $achternaam = $debtordetails['debtor']['SurName'];
                if ($aanhef == 'heer' && !empty($achternaam) || $aanhef == 'mevrouw' && !empty($achternaam)) {
                    $contactpersoon = $aanhef . ' ' . $achternaam; 
                }
                elseif ($aanhef == 'heer/mevrouw') {
                    $contactpersoon = $aanhef; 
                }
                
                $email = $debtordetails['debtor']['EmailAddress'];
                //$email = 's.a.l.gruijters@gmail.com';
                
                $indiv_websites = explode('\n\r',$websites);
                foreach($indiv_websites as $url) {
                    if(strpos($url,'https') !== false) {// ssl website, doe check
                        
                        $orignal_parse = parse_url($url, PHP_URL_HOST);
                        $get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
                        $read = stream_socket_client("ssl://".$orignal_parse.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
                        $cert = stream_context_get_params($read);
                        $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);

                        $validTo = date('Y-m-d', $certinfo['validTo_time_t']);
                        $validToNice = strftime('%e %B %Y',strtotime($validTo));

                        $currentDate = date('Y-m-d');

                        $diff = abs(strtotime($validTo) - strtotime($currentDate));

                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                        if ($months == 1 && $days == 1) {
                            //mail klant dat abo over 31 verloopt
                            $mail = new PHPMailer();
                            $mail->IsSMTP();
                            $mail->CharSet = 'UTF-8';
                            $mail->isHTML(true);  
                            $mail->SMTPSecure = 'ssl';         
        
                            $mail->Host       = "mail.inuit-internet.nl"; // SMTP server example
                            $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                            $mail->SMTPAuth   = true;                  // enable SMTP authentication
                            $mail->Port       = 465;                    // set the SMTP port for the GMAIL server
                            $mail->Username   = "info@inuit-internet.nl"; // SMTP account username example
                            $mail->Password   = "7XE6gAD5wqVz5JWUC6MB";        // SMTP account password example
                            
                            $mail->setFrom('administratie@inuit-internet.nl', 'Inuit Internet Diensten');
                            $mail->addAddress("$email");
                            $mail->addAddress('administratie@inuit-internet.nl');  
                            $mail->Subject = 'Verlenging SSL Certificaat';
                            $mail->Body    = "Geachte $contactpersoon,<br><br>Hierbij willen wij u er op attent maken dat de geldigheid van het onderstaande SSL certificaat bijna is verstreken.<br><br>Neem alstublieft binnen nu en 3 weken contact met ons op om het onderstaande SSL abonnement te verlengen. Wij raden u aan om niet tot het laatste moment te wachten met het verlengen van uw certificaat.<br><br>Wilt u niet verlengen? Dan hoeft u niets te doen; uw certificaat verloopt automatisch.<br><br>Neem gerust contact met ons op als u nog vragen heeft over de verlenging; we helpen u graag!<br><br>Hieronder het SSL Certificaat dat binnenkort zal vervallen: <br><br><br><table border='1' style='border-collapse:collapse;border-color:#ccc;' cellpadding='15px'><tbody><tr><td><strong>{$url}</strong></td><td>SSL Certificaat vervaldatum: {$validToNice}</td></tr></tbody></table>";
                            
                            if(!$mail->send()) {
                                echo 'Message could not be sent.';
                                echo 'Mailer Error: ' . $mail->ErrorInfo;
                            } else {
                                echo 'Message has been sent';
                            }
                        }
                        
                    }    
                }
            }
        }
    }
}

check_ssl_abos();

/*
$url = "https://www.osintelink.nl";
$orignal_parse = parse_url($url, PHP_URL_HOST);
$get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
$read = stream_socket_client("ssl://".$orignal_parse.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
$cert = stream_context_get_params($read);
$certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);

//print_r($certinfo);
$validFrom = date('Y-m-d', $certinfo['validTo_time_t']);
echo $validFrom . '<br>';

$currentDate = date('Y-m-d');

$diff = abs(strtotime($validFrom) - strtotime($currentDate));

$years = floor($diff / (365*60*60*24));
$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

if ($months == 1 && $days == 15) {
    //mail klant dat abo over 6 weken verloopt
}
*/
?>