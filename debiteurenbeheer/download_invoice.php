<?php
require_once "wefact_api.php";
$invoiceID = $_GET['download_invoice'];
function download_invoice($invoiceID) {
    $api = new WeFactAPI(); 
    $invoiceParams = array(
				'InvoiceCode'	=> $invoiceID
    );
    $response = $api->sendRequest('invoice', 'download', $invoiceParams);
    $filename = $response['invoice']['Filename'];
    $pdf_base64 = $response['invoice']['Base64'];
    
    $file = fopen("files/$filename", "w+"); 
    $pdf_decoded = base64_decode($pdf_base64);
    fwrite($file, $pdf_decoded);
    
    if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');	}
    
    if(is_file("files/$filename")) {
        //echo 'yes';
        header('Pragma: public'); 	// required
        header('Expires: 0');		// no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private',false);
        header('Content-Type:application/pdf');
        header('Content-Disposition: attachment; filename="'.basename($filename).'"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize("files/$filename"));	// provide file size
        header('Connection: close');
       
        readfile("files/$filename");
        exit;
    }
}
download_invoice($invoiceID);
?>