<?php
// error_reporting(E_ALL);
// Start API
require_once "wefact_api.php";
setlocale(LC_ALL, 'nl_NL');
//error_reporting(E_ALL);

function send_comment_notification_email($comment,$location,$invoiceID) {
    $pieces = explode('---',$comment);
    $lastcomment = current($pieces);
    $message = "---<br />$lastcomment<br />---<br /><br />Bekijk alle notities voor factuur $invoiceID hier: <br><br><a href=\"$location\" target=\"_blank\">$location</a>";
    $to      = 'administratie@inuit-internet.nl' . ', ';
    $to     .= '';
    $subject = "Nieuwe notitie voor factuur $invoiceID";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: Administratie ~ Inuit Internet Diensten <administratie@inuit-internet.nl>' . "\r\n" .
    'Reply-To: administratie@inuit-internet.nl' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
    mail($to, $subject, $message, $headers);
}
function send_company_comment_notification_email($comment,$location,$bedrijfsnaam) {
    $pieces = explode('---',$comment);
    $lastcomment = current($pieces);
    $message = "---<br />$lastcomment<br />---<br /><br />Bekijk alle notities voor $bedrijfsnaam hier: <br><br><a href=\"$location\" target=\"_blank\">$location</a>";
    $to      = 'administratie@inuit-internet.nl' . ', ';
    $to     .= '';
    $subject = "Nieuwe notitie voor $bedrijfsnaam";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: Administratie ~ Inuit Internet Diensten <administratie@inuit-internet.nl>' . "\r\n" .
    'Reply-To: administratie@inuit-internet.nl' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
    mail($to, $subject, $message, $headers);
}
function list_send_invoices() {
    $api = new WeFactAPI();
    $date = date('d-m-Y');
    $datenice = strftime("%e %B %Y", strtotime($date));
    
    $bedrijf = $_GET['bedrijf'];
    if (empty($bedrijf)) {
        $parameters = array(
        "status"	=> 2,
        "sort" => 'InvoiceCode',
        "order" => 'ASC'
        );
        $parameters_partial = array(
        "status"	=> 3,
        "sort" => 'InvoiceCode',
        "order" => 'ASC'
        );
    } 
    else 
    {
        echo '  <a href="?=" style="text-align: center; width: 100%; display: inline-block;"><strong>Toon alle organisaties</strong></a>';
        $bedrijf = trim($bedrijf);
        
        $parameters = array(
        "status"	=> 2,
        "sort" => 'InvoiceCode',
        "order" => 'ASC',
        "searchat" => 'DebtorCode',
        "searchfor" => $bedrijf
        );
        $parameters_partial = array(
        "status"	=> 3,
        "sort" => 'InvoiceCode',
        "order" => 'ASC',
        "searchat" => 'DebtorCode',
        "searchfor" => $bedrijf
        );
        
    }
    
    $send_invoices_list = $api->sendRequest('invoice', 'list', $parameters);
    $partial_payed_invoice_list = $api->sendRequest('invoice', 'list', $parameters_partial);
    
    if ($send_invoices_list['status'] == 'success' && $partial_payed_invoice_list['status'] == 'success' ) {
        echo "
        <html>
        <head>
        <style type=\"text/css\">
        body,* {font-size:15px;font-family:Arial;}
        th {border-bottom:1px solid #ccc;}
        textarea {padding:10px;}
        small {font-size:12px;}
        </style>
        </head>
        <body>
        <table width=100%' align='center' cellspacing='5px' style='padding-bottom:20px;'>
            <thead>
                <th height='80px' width='14.28%'>Factuur nummer</th>
                <th height='80px' width='14.28%'>Factuur datum</th>
                <th height='80px' width='14.28%'>Openstaand bedrag</th>
                <th height='80px' width='14.28%'>Dagen openstaand</th>
                <th height='80px' width='14.28%'>Dagen overtijd</th>
                <th height='80px' width='14.28%'>Organisatie</th>
                <th height='80px' width='14.28%'>Details</th>
            </thead>
            <tbody>
        ";  
        
        $invoicearray = array();
        if ($send_invoices_list['totalresults'] > 0 && $partial_payed_invoice_list['totalresults'] > 0) {
            $invoicearray = array_merge($send_invoices_list['invoices'],$partial_payed_invoice_list['invoices']);
            //print_r_pre($invoicearray);
            usort($invoicearray, function($a,$b) {
            return $a['Identifier'] - $b['Identifier'];
            });
        }
        elseif ($send_invoices_list['totalresults'] > 0 && $partial_payed_invoice_list['totalresults'] < 1) {
            $invoicearray = $send_invoices_list['invoices'];
        }
        elseif ($send_invoices_list['totalresults'] < 1 && $partial_payed_invoice_list['totalresults'] > 0) {
            $invoicearray = $partial_payed_invoice_list['invoices'];
        }
        else { 
            echo    "
                    <tr>
                        <td>0 facturen gevonden</td>
                    </tr>
                    ";
            continue;
        }
        
        $now = time();
        $totaalbedrag = 0;
        foreach($invoicearray as $invoice) {
            $ID = $invoice['InvoiceCode'];
            $bedrijf = $invoice['CompanyName'];
            $bedrijfID = $invoice['DebtorCode'];
            $factuurdatum = $invoice['Date'];
            $factuurdatum_nice = strftime("%e %B %Y", strtotime($factuurdatum));
            $duedate = strtotime($factuurdatum);
            $diff = abs($now - $duedate);
            $dagen = floor($diff/(60*60*24));
            $dagen_overtijd = $dagen - 30;
            $bedrag = $invoice['AmountIncl'];  
            
            if ($invoice['Status'] == 3) {
                $invoicedetails = $api->sendRequest('invoice', 'show', array('InvoiceCode' => $invoice['InvoiceCode']));
                if ($invoicedetails['status']=='success') {
                    //print_r_pre($invoicedetails);
                    $betaald = $invoicedetails['invoice']['AmountPaid'];
                    $bedrag = $bedrag - $betaald;
                }
            }
           
            if ($dagen_overtijd > 0) {
                $totaalbedrag = $totaalbedrag + $bedrag;
                echo "
                <tr height=\"35\">
                    <td align=\"center\">
                        {$ID}
                    </td>
                    <td align=\"center\">
                        {$factuurdatum_nice} 
                    </td>
                    <td align=\"center\">
                        &euro; {$bedrag}
                    </td>
                    <td align=\"center\">
                        {$dagen}
                    </td>
                    <td align=\"center\">
                        {$dagen_overtijd}
                    </td>
                    <td align=\"center\">
                        <a href=\"?bedrijf={$bedrijfID}\">{$bedrijf}</a>
                    </td>
                    <td align=\"center\">
                        <a href=\"?invoiceid={$ID}#details\">Factuur details</a>
                    </td>
                </tr>
                ";
            }
        }
        echo "
                <tr>
                    <td></td>
                    <td></td>
                    <td align=\"center\" height='80px' style='border-top: 1px solid #ccc;'>
                        &euro; {$totaalbedrag}
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
            </tbody>
        </table>
        ";    
    }
    else {
        echo 'Geen resultaten gevonden';
    }
    
    if (isset($_POST['commentfield'])) {
        $commentUpdated = $_POST['commentfield'];
        $invoiceID = $_POST['invoiceID'];
        $identifier = $_POST['identifier'];
        $bedrijfsnaam = $_POST['bedrijfsnaam'];
        
        if (!empty( $identifier )) {
            $api->sendRequest('invoice', 'edit', array(
            'Identifier'		=> $identifier, 
            'Comment'		=> $commentUpdated
            ));
        }
        $location = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#details";
        send_comment_notification_email($commentUpdated,$location,$invoiceID);
    }
    
     if (isset($_POST['companycommentfield'])) {
        $commentUpdated = $_POST['companycommentfield'];
        $bedrijfID = $_POST['bedrijfID'];
        $bedrijfsnaam = $_POST['bedrijfsnaam'];
        
        if (!empty( $bedrijfID )) {
            $api->sendRequest('debtor', 'edit', array(
            'DebtorCode'		=> $bedrijfID, 
            'Comment'		=> $commentUpdated
            ));
        }
        $location = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]#company_details";
        send_company_comment_notification_email($commentUpdated,$location,$bedrijfsnaam);
    }
    
    if ( (!empty($_GET['invoiceid'])) || (!empty($_POST['commentfield'])) ) {  //factuur details
        $invoiceID = $_GET['invoiceid'];
        
        $invoicedata = $api->sendRequest('invoice', 'show', array('InvoiceCode'	=> $invoiceID));
        
        $comments = $invoicedata['invoice']['Comment'];
        $identifier = $invoicedata['invoice']['Identifier'];
        $lastreminder = $invoicedata['invoice']['ReminderDate'];
        $lastremindernice = strftime("%e %B %Y", strtotime($lastreminder));
        
        $reminders = $invoicedata['invoice']['Reminders'];
        $reminderstring = '';
        $summationstring = '';
        
        if ($reminders > 2) {$reminderType = 'aanmaning';} else {$reminderType = 'herinnering';}
        if ($reminders > 0) {
            $reminderstring .= "<li>Aantal verzonden herinneringen: <strong>$reminders</strong></li>";
            $reminderstring .= "<li>Laatste herinnering verzonden op: <strong>$lastremindernice</strong></li>";
        }
     
        $lastsummation = $invoicedata['invoice']['SummationDate']; //aanmaningen
        $lastsummationnice = strftime("%e %B %Y", strtotime($lastsummation));
        $summations = $invoicedata['invoice']['Summations'];
       
        if ($summations > 0) {
            $summationstring .= "<li>Aantal verzonden aanmaningen: <strong>$summations</strong></li>";
            $summationstring .= "<li>Aanmaning met ingebrekestelling verzonden op: <strong>$lastsummationnice</strong></li>";
        }
        
        if (empty($reminderstring) && empty($summationstring)) {$reminderstring = "<li>Er zijn nog geen herinneringen of aanmaningen verzonden voor deze factuur.</li>";}
        
        echo "
        <div style=\"padding:30px;background:#eee;\" id=\"details\" name=\"details\">
            <h3>Details voor factuur {$invoiceID} </h3>
            <ul style=\"list-style:none;line-height:50px;background:#fff;border:1px solid #ccc;\">
                {$reminderstring}
                {$summationstring}
                <li><a target=\"_blank\" href=\"download_invoice.php?download_invoice={$invoiceID}\">Download factuur</a></li>
            </ul>
            <br>
            <h3>Notities voor factuur {$invoiceID}</h3>
            <em><small>notities altijd beginnen met huidige datum: {$datenice}<br>scheiden met '---'<br>nieuwste notitie bovenaan</small></em><br />
            <br />
            <form id=\"commentform\" method=\"post\" action=\"\" style=\"width:825px;\">
                <textarea rows=\"25\" cols=\"100\" name=\"commentfield\" placeholder=\"Er zijn nog geen notities voor deze factuur\">{$comments}</textarea>
                <input type=\"hidden\" name=\"invoiceID\" value=\"{$invoiceID}\" />
                <input type=\"hidden\" name=\"identifier\" value=\"{$identifier}\" />
                
                <br />
                <input type=\"submit\" value=\"Bijwerken\" style=\"padding:5px;margin-top:5px;font-weight:bold;float:left;cursor:pointer;\" />
            </form>
            <br />
        </div>
        ";
    }
    
    elseif (!empty($_GET['bedrijf'])) { // bedrijf details
    $bedrijfdata = $api->sendRequest('debtor', 'show', array('DebtorCode'	=> $bedrijfID));
    $companycomments = $bedrijfdata['debtor']['Comment'];
    $bedrijfsnaam = $bedrijfdata['debtor']['CompanyName'];
    $persoonvoornaam = $bedrijfdata['debtor']['Initials'];
    $persoonachternaam = $bedrijfdata['debtor']['SurName'];
    $tel = $bedrijfdata['debtor']['PhoneNumber'];
    $mob = $bedrijfdata['debtor']['MobileNumber'];
    /*if (!empty($mob)) {
        $tel = $mob;
    }*/
    $email = $bedrijfdata['debtor']['EmailAddress'];
    
    $date = date('d-m-Y');
        echo "
        <div style=\"padding:30px;background:#eee;\" id=\"company_details\" name=\"company_details\">
            <h1>{$bedrijfsnaam}</h1>
            <ul style=\"list-style:none;line-height:50px;\">
                <li><strong>Contactpersoon:</strong> {$persoonvoornaam} {$persoonachternaam}</li>
                <li><strong>Telefoon:</strong> {$tel}</li>
                <li><strong>Mobiel:</strong> {$mob}</li>
                <li><strong>E-mail:</strong> <a href=\"mailto:{$email}\" target=\"_blank\">{$email}</a></li>
            </ul>
            <br>
            <h3>Notities voor {$bedrijfsnaam}</h3>
            <em><small>notities altijd beginnen met huidige datum: {$date}<br>scheiden met '---'<br>nieuwste notitie bovenaan</small></em><br />
            <br />
            <form id=\"commentform\" method=\"post\" action=\"\" style=\"width:825px;\">
                <textarea rows=\"25\" cols=\"100\" placeholder=\"Er zijn nog geen notities voor deze organisatie\" name=\"companycommentfield\">{$companycomments}</textarea>
                <input type=\"hidden\" name=\"bedrijfID\" value=\"{$bedrijfID}\" />
                <input type=\"hidden\" name=\"bedrijfsnaam\" value=\"{$bedrijfsnaam}\" />
                <br />
                <input type=\"submit\" value=\"Bijwerken\" style=\"padding:5px;margin-top:5px;font-weight:bold;float:left;cursor:pointer;\" />
            </form>
            <br />
            <br />
            <br />
            <div style=\"width:743px;background:#fff;border:1px solid #ccc;padding:10px;\">
            <h3>Verstuur een facturen overzicht met gekoppeld betalingsverzoek naar {$bedrijfsnaam}</h3>
            <p>Let op: dit dient niet zomaar gedaan te worden maar altijd na overleg.</p>
            <a style=\"color:#000;text-decoration:none;padding:7px;margin-top:5px;font-weight:bold;cursor:pointer;display:inline-block;background:#eee;border:1px solid #ccc;\" target=\"_blank\" href=\"http://debiteurenbeheer.inuit-internet.nl/email_customer_overtime_invoices.php?bedrijf={$bedrijfID}\">Nu versturen</a>
            <br />
            <h3>Verstuur een algemeen facturen overzicht naar {$bedrijfsnaam}</h3>
            <a style=\"color:#000;text-decoration:none;padding:7px;margin-top:5px;font-weight:bold;cursor:pointer;display:inline-block;background:#eee;border:1px solid #ccc;\" target=\"_blank\" href=\"http://debiteurenbeheer.inuit-internet.nl/email_customer_invoices.php?bedrijf={$bedrijfID}\">Nu versturen</a>
            </div>
        </div>
        ";
   }
   else { // aanwijzingen
     echo "
        <div style=\"padding:30px;background:#eee;min-height:400px;\">
            <h3>Uitleg</h3>
            <ul>
                <li>
                    Klik op een <em>bedrijfsnaam</em> in het bovenstaande overzicht om een overzicht van alle facturen verstuurd naar het betreffende bedrijf te bekijken;
                </li>
                <li>
                    Klik op een <em>bedrijfsnaam</em> in het bovenstaande overzicht om details van het bedrijf te bekijken;
                </li>
                <li>
                    Klik op <em>Factuur details</em> in het bovenstaande overzicht om details voor de betreffende factuur in te zien;
                </li>
            </ul>
        </div>
        ";
   }
   echo "
   </body>
   </html>
   "; 
}
list_send_invoices();
?>