<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$creditorParams = array(
				'CompanyName' 		=> 'Supplier 1',
				'Initials' 			=> 'Curtis',
				'SurName' 			=> 'Johnson',
				'Sex' 				=> 'm',
				'Address' 			=> 'Adalbertstrasse 1',
				'ZipCode' 			=> '10999',
				'City' 				=> 'Berlin',
				'Country' 			=> 'DE',
				'EmailAddress' 		=> 'billing@supplier.de'
);

$response = $api->sendRequest('creditor', 'add', $creditorParams);

print_r_pre($response);

?>