<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();


$creditInvoiceParams = array(
				'CreditInvoiceCode'		=> 'CF0001',
				
				'InvoiceLines' => array(
					array(
						'Description'	=> 'Software licences',
						'PriceExcl'		=> 350
					)
				)
);

$response = $api->sendRequest('creditinvoiceline', 'add', $creditInvoiceParams);

print_r_pre($response);

?>