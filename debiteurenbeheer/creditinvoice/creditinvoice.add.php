<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$creditInvoiceParams = array(
				'CreditorCode'	=> 'CD0001',
				'InvoiceCode'	=> '2014-361',
				'Date' 			=> '2014-05-20',
				
				'InvoiceLines' => array(
					array(
						'Number'		=> 3,
						'Description'	=> 'Consultancy',
						'PriceExcl'		=> 125
					),
					array(
						'Description'	=> 'Traveling costs',
						'PriceExcl'		=> 155.35
					),
				)
);

$response = $api->sendRequest('creditinvoice', 'add', $creditInvoiceParams);

print_r_pre($response);

?>