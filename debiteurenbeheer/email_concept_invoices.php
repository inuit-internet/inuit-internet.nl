<?php
// Start API
date_default_timezone_set('Europe/Amsterdam');
require_once "wefact_api.php";

function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') == $date;
}

function email_invoice($invoice_id) {
    $api = new WeFactAPI();
    $invoiceParams = array('Identifier' => $invoice_id);
    $invoice = $api->sendRequest('invoice', 'sendbyemail', $invoiceParams);
}

function email_concept_invoices() {
    $api = new WeFactAPI();
    
    $parameters = array(
        "status"	=> 0
    );
    $concept_invoice_list = $api->sendRequest('invoice', 'list', $parameters);
    
    if ($concept_invoice_list['status'] == 'success') {
        $invoicearray = array();
        $invoicearray = $concept_invoice_list['invoices'];
        foreach($invoicearray as $invoice) {
            $ID = $invoice['Identifier']; 

            $invoiceDetails = $api->sendRequest('invoice','show',array('Identifier' => $ID));
            if ($invoiceDetails['status'] == 'success') {
                $invoiceDate = $invoiceDetails['invoice']['Date'];
                
                $AltDate = $invoiceDetails['invoice']['Comment'];
                $checkDate = validateDate($AltDate);
                //var_dump($checkDate);
                
                if ($checkDate == true && $AltDate !== $invoiceDate) {
                    $invoiceDate = $AltDate;
                }
                
                $currentDate = date("Y-m-d");
                if ($invoiceDate == $currentDate) {
                    email_invoice($ID);
                    //echo "emailing invoice $ID";
                }
            }  
        }
    }
}
email_concept_invoices();
?>