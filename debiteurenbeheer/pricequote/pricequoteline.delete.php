<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$priceQuoteLineParams = array(
							'PriceQuoteCode'	=> 'OF0001',
							
							'PriceQuoteLines'	=> array(
								array(
									'Identifier'	=> 3
								)
							)
);

$response = $api->sendRequest('pricequoteline', 'delete', $priceQuoteLineParams);

print_r_pre($response);

?>