<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$priceQuoteParams = array(
						// Example: search for all invoices on date 2014-05-16
						'searchat'		=> 'Date',
						'searchfor' 	=> '2014-05-16'
);

$response = $api->sendRequest('pricequote', 'list', $priceQuoteParams);

print_r_pre($response);

?>