<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$priceQuoteLineParams = array(
							'PriceQuoteCode' 	=> 'OF0001',
							
							'PriceQuoteLines' 	=> array(
								array(
									'Description'	=> 'Traveling costs',
									'PriceExcl'		=> 35
								)
							)
);

$response = $api->sendRequest('pricequoteline', 'add', $priceQuoteLineParams);

print_r_pre($response);

?>