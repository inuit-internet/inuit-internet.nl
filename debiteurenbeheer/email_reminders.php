<?php
// Start API
require_once "wefact_api.php";
setlocale(LC_ALL, 'nl_NL');
date_default_timezone_set('europe/amsterdam');
include_once('../wp-includes/class-phpmailer.php');


function send_email_notification($message) {
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->CharSet = 'UTF-8';
    $mail->isHTML(true);  
    $mail->SMTPSecure = 'ssl';         

    $mail->Host       = "mail.inuit-internet.nl"; 
    $mail->SMTPDebug  = 0;                     
    $mail->SMTPAuth   = true;                  
    $mail->Port       = 465;                  
    $mail->Username   = "info@inuit-internet.nl"; 
    $mail->Password   = "7XE6gAD5wqVz5JWUC6MB";        
    
    $mail->setFrom('administratie@inuit-internet.nl', 'Inuit Internet Diensten');
    $mail->addAddress('administratie@inuit-internet.nl');   
    $mail->Subject = "Dagelijks overzicht te versturen herinneringen en aanmaningen";
    $mail->Body  = $message;
    
    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
    }
}

/*
function email_reminder($invoiceCODE) {
    $invoiceParams = array('InvoiceCode' => $invoiceCODE);
    $api = new WeFactAPI();
    $reminder = $api->sendRequest('invoice', 'sendreminderbyemail', $invoiceParams);
}
function email_summation($invoiceCODE) {
    $invoiceParams = array('InvoiceCode' => $invoiceCODE);
    $api = new WeFactAPI();
    $summation = $api->sendRequest('invoice', 'sendsummationbyemail', $invoiceParams);
}
function email_notification($invoiceCODE) {
    $location = "http://debiteurenbeheer.inuit-internet.nl/list_send_overtime_invoices.php?invoiceid=$invoiceCODE#details";
    $message = "Factuur $invoiceCODE is ondanks een aanmaning met ingebrekestelling niet voldaan binnen de aangezegde termijn van 5 werkdagen. Er dient een telefoontje gepleegd te worden inzake deze factuur waarin nog een laatste kans gegeven wordt om binnen 3 werkdagen de factuur te voldoen.<br />Gebeurd dit niet dan dient een incasso procedure opgestart te worden.<br />---<br /><br />Bekijk details inzake factuur $invoiceCODE hier <small>(link opent in nieuw scherm)</small>: <br><br><a href=\"$location\" target=\"_blank\">$location</a>";
    $to      = 'administratie@inuit-internet.nl';
    $subject = "Telefonische actie nodig inzake factuur $invoiceID";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: Administratie ~ Inuit Internet Diensten <administratie@inuit-internet.nl>' . "\r\n" .
    'Reply-To: administratie@inuit-internet.nl' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
    mail($to, $subject, $message, $headers);
}
*/

function email_reminders() {
    $api = new WeFactAPI();
    $now = time();
    
    $message = "Overzicht van facturen waarvoor vanaf vandaag een herinnering of aanmaning verzonden kan worden (na controle bank!). <br><br>";
    
    $parameters_1 = array(
    "status"	=> 2
    );
    $parameters_2 = array(
    "status"	=> 3
    );
    
    $send_invoice_list = $api->sendRequest('invoice', 'list', $parameters_1);
    $partial_payed_invoice_list = $api->sendRequest('invoice', 'list', $parameters_2);
    
    if ($send_invoice_list['status'] == 'success' && $partial_payed_invoice_list['status'] == 'success') {
        $invoicearray = array();
        
        if ($send_invoice_list['totalresults'] > 0 && $partial_payed_invoice_list['totalresults'] > 0) {
            $invoicearray = array_merge($send_invoice_list['invoices'],$partial_payed_invoice_list['invoices']);
        }
        elseif ($send_invoice_list['totalresults'] > 0 && $partial_payed_invoice_list['totalresults'] < 1) {
            $invoicearray = $send_invoice_list['invoices'];
        }
        elseif ($send_invoice_list['totalresults'] < 1 && $partial_payed_invoice_list['totalresults'] > 0) {
            $invoicearray = $partial_payed_invoice_list['invoices'];
        }
        else { //geen relevante facturen gevonden, exit
            return false;
            exit();
        }
        
        $notes = 0;
        
        foreach($invoicearray as $invoice) {
            $invoiceCODE = $invoice['InvoiceCode'];           
            
            $factuurdatum = $invoice['Date'];
            $rekendatum = strtotime($factuurdatum);
            
            $invoicedata = $api->sendRequest('invoice', 'show', array('InvoiceCode'	=> $invoiceCODE));
            
            $reminders = $invoicedata['invoice']['Reminders'];
            $remindersint = (int)$reminders;
            
            $summations = $invoicedata['invoice']['Summations'];
            $summationsint = (int)$summations;
                        
            $diff = abs($now - $rekendatum);
            $age = floor($diff/(60*60*24));
            
            if ($age == 51 && $summationsint == 1) { //dag 51 - bel actie vereist
                $notes++;
                $message .= "<br><br>- Er is een <strong>bel actie</strong> vereist voor factuur <strong>$invoiceCODE</strong> wegens niet voldaan na aanmaning, zie <a href=\"http://debiteurenbeheer.inuit-internet.nl/list_send_overtime_invoices.php?invoiceid=$invoiceCODE#details\">details.</a>";
            }
            elseif ($age > 43 && $summationsint < 1 && $remindersint == 2) { //begin dag 45 - ingebrekestelling
                $notes++;
                $message .= "<br><br>- Er kan een <strong>aanmaning</strong> verstuurd worden voor factuur <strong>$invoiceCODE</strong>";
            } 
            elseif ($age > 36 && $remindersint == 1) { //begin dag 38 - 2de herinnering
                $notes++;
                $message .= "<br><br>- Er kan een <strong>2de herinnering</strong> verstuurd worden voor factuur <strong>$invoiceCODE</strong>";
            }
            elseif ($age > 30 && $remindersint == 0) { //begin dag 31 - 1ste herinnering
                $notes++;
                $message .= "<br><br>- Er kan een <strong>1ste herinnering</strong> verstuurd worden voor factuur <strong>$invoiceCODE</strong>";
            }  
            else { //niets versturen, check volgende factuur
                continue;
            }
        }
        
        if ($notes > 0) {
            $message .= "<br><br><a href=\"https://www.abnamro.nl/portalserver/nl/zakelijk/index.html?l\" target=\"_blank\">Zakelijke rekening</a><br><a target=\"_blank\" href=\"https://www.mijnwefact.nl/inloggen\">Wefact</a>";
            send_email_notification($message);
        }
        
    }
}
email_reminders();
?>