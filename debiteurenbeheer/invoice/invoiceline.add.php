<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$invoiceLineParams = array(
						'InvoiceCode'		=> 'F0001',
						
						'InvoiceLines' 		=> array(
							array(
								'Description'	=> 'Traveling costs',
								'PriceExcl'		=> 35
							)
						)
);

$response = $api->sendRequest('invoiceLine', 'add', $invoiceLineParams);

print_r_pre($response);

?>