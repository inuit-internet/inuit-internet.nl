<?php
// Start API
error_reporting(E_ALL);
$coredoc = $_SERVER['PHP_SELF'];
setlocale(LC_ALL, 'nl_NL');
date_default_timezone_set('Europe/Amsterdam');
require_once "wefact_api.php";

function create_concept($DID,$description,$time,$rate,$date) {
    $api = new WeFactAPI();
    $currentDate = date('Y-m-d');
    
    if ($date == $currentDate) {
        $comment = "";
    }
    elseif (!empty($date) && $date !== $currentDate) {
        $comment = $date;
    } 
    else {
        $comment = "";
    }
    
    if ($time !== 0) {
    
        $insert = $api->sendRequest('invoice', 'add', array(
            'Debtor' => $DID,
            'Comment' => $comment,
            'InvoiceLines'	=> array(
                array(
                    'Description'	=> $description,
                    'TaxPercentage' => 21,
                    'Number' => $time,
                    'NumberSuffix' => ' uur',
                    'PriceExcl'	=> $rate
                )
            )
        ));
    
    }
    else {
        
        $insert = $api->sendRequest('invoice', 'add', array(
            'Debtor' => $DID,
            'Comment' => $comment,
            'InvoiceLines'	=> array(
                array(
                    'Description'	=> $description,
                    'TaxPercentage' => 21,
                    'PriceExcl'	=> $rate
                )
            )
        ));
        
    }
    return $insert['status'];
}

function update_concept($invoiceID,$description,$time,$rate) {
    $api = new WeFactAPI();
    if ($time !== 0) {
        
        $update = $api->sendRequest('invoiceline', 'add', array(
            'Identifier'	=> $invoiceID,
            'InvoiceLines' 	=> array(
                array(
                    'Description'	=> $description,
                    'TaxPercentage' => 21,
                    'Number' => $time,
                    'NumberSuffix' => ' uur',
                    'PriceExcl'	=> $rate
                )
            )
        ));  
    }
    else {
        
        $update = $api->sendRequest('invoiceline', 'add', array(
            'Identifier'	=> $invoiceID,
            'InvoiceLines' 	=> array(
                array(
                    'Description'	=> $description,
                    'TaxPercentage' => 21,
                    'PriceExcl'	=> $rate
                )
            )
        ));
    }
    return $update['status'];
}

function get_debtors() {
    $api = new WeFactAPI();
    $debtors = $api->sendRequest('debtor', 'list', array());
    if ($debtors['status'] == 'success') {
        return $debtors['debtors'];
    }
    else {
        return 'debtor request failed';
    }
}

function get_debtor($CompanyName) {
    $api = new WeFactAPI();
    $debtor = $api->sendRequest('debtor', 'list', array(
        'searchat'	=> 'CompanyName',
        'searchfor' 	=> $CompanyName
    ));
    return $debtor;
}

function get_debtor_concepts($CompanyName) {
    $api = new WeFactAPI();
    $concepts = $api->sendRequest('invoice', 'list', array(
        "status"	=> 0,
        "searchat"	=> 'CompanyName',
        "searchfor" => $CompanyName
    )); 
    if  ($concepts['status'] == 'success') {
        return $concepts['invoices'];
    }
}

if (isset($_GET['def_submit']) && $_GET['def_submit'] == 'def') { //concept factuur toevoegen of bijwerken
    $date = $_GET['date'];
    $description = $_GET['description'];
    $rate = $_GET['rate'];
    $time = $_GET['time'];
    if (empty($time)) {$time = 0;}
    $add_rate = $_GET['add_rate'];
    if ($add_rate == 'add_rate' && $time !== 0) {
        $ratenice = money_format('%.2n', $rate);
        $description .=  "\r\n\r\n Tarief: $ratenice p/u excl. BTW";
    }
    
    if (isset($_GET['existing'])) {
        $existingID = $_GET['existing'];
        $updateResult = update_concept($existingID,$description,$time,$rate,$date);
    }
    else {
        $CompanyName = $_GET['debtor'];
        if(!empty($CompanyName)) {
                $debtor = get_debtor($CompanyName);
                if ($debtor['status'] == 'success') {
                    $DID = $debtor['debtors'][0]['Identifier']; 
                }
        } 
        
        if (!empty($DID)){
            $createResult = create_concept($DID,$description,$time,$rate,$date);
        }
    }
}
?>
<html>
    <head>
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <style type="text/css">
            body,* {
                font-size:15px;
                font-family:Arial!important;
                padding:0;
                margin:0;
            }
            h3 {
                padding-bottom:15px;
            }
            .container {
                padding:30px;
                background:#eee;
            }
            input[type="submit"] {
                color:#000;
                text-decoration:none;
                padding:7px;
                margin-top:5px;
                font-weight:bold;
                cursor:pointer;
                display:inline-block;
                background:#eee;
                border:1px solid #ccc;
            }
            input[type="text"] {
                padding:8px;
                font-size:15px;
                width:320px;
            }
            select,option {
                padding:8px;
                 width:320px;
            }
            textarea {
                padding:8px;
                font-family:inherit;
                width:600px;
                height:300px;
            }
            .results {
                padding:20px;
                width:600px;
                background:#fff;
                
            }
            input#time {
                margin-left:20px;
                width:193px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h3>Factureer tijd</h3>
            <form action="" method="GET">
                <select class="" name="debtor" onchange="this.form.submit()">
                    <option selected="selected" value="Selecteer klant">Selecteer klant</option>
                    <?php
                    $debtors = get_debtors(); 
                    if (is_array($debtors) && !empty($debtors)) :
                    foreach($debtors as $debtor) :
                    $debtorID = $debtor['Identifier'];
                    $DebtorCode = $debtor['DebtorCode'];
                    $CompanyName = $debtor['CompanyName'];
                    ?>
                    <option value="<?php echo $CompanyName; ?>" <?php if(isset($_GET['debtor']) && $_GET['debtor'] == $CompanyName) : ?>selected="selected"<?php endif; ?>><?php echo $CompanyName; ?></option>
                    <?php
                    endforeach;
                    else :
                    ?>
                    <option value="error">error</option>
                    <?php
                    endif;
                    ?>
                </select>
                <br /><br />
                <?php
                if (isset($_GET['debtor'])) :
                $concepts = get_debtor_concepts($_GET['debtor']);
                if (!empty($concepts)) :
                ?>
                <select name="existing">
                    <option selected="selected" value="Selecteer bestaande">Toevoegen aan bestaand concept als regel</option>
                    <?php
                    foreach($concepts as $concept) :
                    $InvoiceCode = $concept['InvoiceCode'];
                    $conceptID = $concept['Identifier'];
                    $conceptAmount = $concept['AmountIncl'];
                    $conceptDate = $concept['Date'];
                    ?>
                    <option value="<?php echo $conceptID; ?>"><?php echo $InvoiceCode . '  (&euro;' . $conceptAmount . ')'; ?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
                <br /><br />
                <?php
                endif;
                endif;
                ?> 
                <label for="time">Tijd in uren (x.x)</label>
                <input type="text" placeholder="Tijd in uren (x.x)" name="time" id="time" value="0" /><br /><br />
                <input type="text" placeholder="Totaalprijs of uurtarief in &euro; (xx.xx)" name="rate" value="" /><br /><br />
                <input type="text" placeholder="Factuur datum (YYYY-MM-DD)" name="date" value="<?php echo date('Y-m-d'); ?>" /><br /><br />
                <textarea type="text" name="description" placeholder="Beschrijving"></textarea><br /><br />
                <label for="add_rate">Uurtarief toevoegen aan beschrijving (alleen wanneer tijd > 0)</label>
                <input type="checkbox" name="add_rate" id="add_rate" value="add_rate" /><br /><br /> 
                <label for="def_submit">Alles klopt</label>
                <input type="checkbox" name="def_submit" id="def_submit" value="def" /><br /><br /> 
                <input type="submit" name="" value="Insturen"/>
            </form>
            <?php
            if ($updateResult == 'success' && $_GET['def_submit'] == 'def') : 
            ?>
            <p class="results">
                Toevoeging factuur regel succesvol.
                <br /><br />
                <a href="<?php echo $coredoc; ?>">Nieuwe toevoeging</a>
            </p>
            <?php
            elseif ($createResult == 'success' && $_GET['def_submit'] == 'def') : 
            ?>
            <p class="results">
                Toevoeging factuur succesvol.
                <br /> <br />
                <a href="<?php echo $coredoc; ?>">Nieuwe toevoeging</a>
            </p>
            <?php
            elseif($_GET['def_submit'] == 'def') :
            ?>
            <p class="results">
                Er is iets misgegaan, niets toegevoegd.
                <br /><br />
                <a href="<?php echo $coredoc; ?>">Nieuwe toevoeging</a>
            </p>
            <?php
            endif;
            ?>
        </div>
    </body>
</html>