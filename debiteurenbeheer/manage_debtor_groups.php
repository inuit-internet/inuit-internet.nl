<?php
// Start API
date_default_timezone_set('Europe/Amsterdam');
require_once "wefact_api.php";
error_reporting(E_ALL);

function manage_groups() {
$api = new WeFactAPI();
$debtorsresult = $api->sendRequest('debtor', 'list', array());
	if($debtorsresult['status'] == 'success') { 	
		$debtors = $debtorsresult['debtors'];
        foreach ($debtors as $debtor) {
			//continue;
			$debtorCode = $debtor['DebtorCode'];
			$debtordetails = $api->sendRequest('debtor', 'show', array('DebtorCode' => $debtorCode));
			$groups = $debtordetails['debtor']['Groups'];
			$actief = 0;
			$inactief = 0;
			
			if (is_array($groups) && !empty($groups)) {
				
				$groupsmod = array();
				
				foreach($groups as $array) {
					if (in_array('Actief',$array)) {
						$actief++;
						//voeg debiteur toe aan groep actief		
						$groupsmod[] = 17;
						//voeg debiteur toe aan groep actief + slapend
						$groupsmod[] = 16;
					}
					if (in_array('Inactief',$array)) {
						$inactief++;
						$groupsmod[] = 19;
					}
				}
								
				if ($actief + $inactief == 0) { //wanneer noch actief noch inactief moet debiteur slapend zijn / voeg ook toe aan actief + slapend
					$groupsmod[] = 18;
					$groupsmod[] = 16;
				}
				
				if (!empty($groupsmod)) {
					//print_r( $debtorCode . '- ' . $groupsmod );
					$api->sendRequest('debtor', 'edit', array('DebtorCode' => $debtorCode,'Groups'=>$groupsmod));
					echo "Groepen aangepast voor $debtorCode <br>";
				}
			
			}
			
			else { //geen groepen ingesteld voor debiteur >> stuur mail
				echo "Geen groepen ingesteld voor $debtorCode <br>";
			}
			
		}
	}
}
manage_groups();
?>