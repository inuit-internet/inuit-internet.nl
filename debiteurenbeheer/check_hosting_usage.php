<?php
//error_reporting(E_ALL);
setlocale(LC_ALL, 'nl_NL');
date_default_timezone_set('europe/amsterdam');
include_once('../wp-includes/class-phpmailer.php');
require_once "wefact_api.php";
include_once('httpsocket.php');

/*
$included_files = get_included_files();
foreach ($included_files as $filename) {
    echo "$filename\n";
}

$sock->query('/CMD_API_SHOW_USER_USAGE?user=bossenhoek');
$result = $sock->fetch_parsed_body();
print_r($result);
*/

function get_user_hostingaccount_quota($username) {
    $api = new WeFactAPI(); 
    $params = array(
        'searchat' => 'Description',
        'searchfor' => $username
    );
    $abos = $api->sendRequest('subscription', 'list', $params);
    if ($abos['status'] == 'success') { 
        $abosarray = array();
        foreach($abos['subscriptions'] as $abo) {
            $details = $api->sendRequest('subscription', 'show', array(
                'Identifier' => $abo['Identifier']
            ));
            if ($details['status'] == 'success') {
                $comment = $details['subscription']['Comment'];
                if (strpos($comment,'Disk') !== false) {
                    $return = array();
                    $items = explode(PHP_EOL, $comment);
                    $disk = trim(explode(':',$items[0])[1]);
                    $data = trim(explode(':',$items[1])[1]);
                    
                    $return['disk'] = $disk;
                    $return['data'] = $data;
                    
                    var_dump($return);
                    return $return;
                }
            }
        }
    }
}

function send_email_notification($email,$message) {
    if (!empty($email)) {
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);  
        $mail->SMTPSecure = 'ssl';         

        $mail->Host       = "mail.inuit-internet.nl"; // SMTP server example
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Port       = 465;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "info@inuit-internet.nl"; // SMTP account username example
        $mail->Password   = "7XE6gAD5wqVz5JWUC6MB";        // SMTP account password example
        
        $mail->setFrom('administratie@inuit-internet.nl', 'Inuit Internet Diensten');
        $mail->addAddress("$email");
        $mail->addAddress('administratie@inuit-internet.nl');   
        $mail->Subject = 'Uw webhosting account';
        $mail->Body  = $message;
        
        echo $email . ' >> ' . $message . '<br /><br />';
        
        /*
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
        */
    }
}

function check_cmd_users() {
    $api = new WeFactAPI();  //creeer tabel met wefact da usernames en primaire e-mail adressen
    $params = array();
    $custdata = array();
    $excluded_users = array('admin','gruiters','xuna');
    
    /*
    $customers = $api->sendRequest('debtor', 'list', $params);
    foreach($customers as $customer) {
        foreach($customer as $cus) {
             $dbcode = $cus['DebtorCode'];
             $debtordetails = $api->sendRequest('debtor', 'show', array(
                'DebtorCode'	=> $dbcode
            ));
            $DA_users = $debtordetails['debtor']['CustomFields']['directadminuser'];
            trim($DA_users);

            if(!empty($DA_users)) {
                $DA_users = (string)$DA_users;
                $DA_users = explode(" ", $DA_users);
                
                foreach($DA_users as $DA_user) {
                    trim($DA_user);
                    $email = $debtordetails['debtor']['EmailAddress'];
                    $custdata[$DA_user] = $email;
                }
            }
        }
    }
    */
    
    $sock = new HTTPSocket;
    $sock->connect('178.251.26.102',2222);
    $sock->set_login('admin','53xiZWddyCVz');

    
    //var_dump($userlines);
    
    
   // $sock->query('/CMD_API_ALL_USER_USAGE');
    $sock->query('/CMD_API_SHOW_ALL_USERS');
    //$result = $sock->fetch_body();
   $user_array_raw = explode("&", $sock->fetch_body());
   
   foreach ($user_array_raw as $key => $user) {
       $user = str_replace('list[]=','',$user);
       if (in_array($user,$excluded_users)) {
           unset($user_array_raw[$key]); 
       }
   }
  
    foreach ($user_array_raw as $user) {
        
        $user = str_replace('list[]=','',$user);
        
        echo $user . '<br>';
         
        $sock->query('/CMD_API_USER_HISTORY?user='.$user.'');
        $userlines = $sock->fetch_body();
        $userlines = urldecode( $userlines );
        
        /*
        $userarray = explode('&',$line);
        $user = explode('=',$userarray['0'])[0];
        $bandwidth = explode('=',$userarray['0'])[2];
        $bandwidth_currentuse = (int)explode('/',$bandwidth)[0];
        $bandwidth_maxuse = (int)explode('/',$bandwidth)[1];
        $domein = substr(strstr($userarray[2], '='),1);
        $schijfruimte = substr(strstr($userarray[7], '='),1);
        $schijfruimte_currentuse = (int)explode('/',$schijfruimte)[0];
        $schijfruimte_maxuse = (int)explode('/',$schijfruimte)[1];
        $message = '';
        $CMD_email = '';
        
        
        
        if ( $bandwidth_currentuse > $bandwidth_maxuse && !in_array($user,$excluded_users) ) { //check bandbreedte
            $bandwidth_overuse = $bandwidth_currentuse - $bandwidth_maxuse;
            $sock->query('/CMD_API_SHOW_USER_CONFIG?user='.$user.'');
            $result = $sock->fetch_parsed_body();
    
            $CMD_email = $custdata[$user]; //first use wefact address
            if (empty($CMD_email)) {
                $CMD_email = $result['email'];
            }
            
            $domein = $result['domain'];
            $message .= "Geachte klant,<br /><br />Uw hosting pakket kent een limiet in bandbreedte gebruik van $bandwidth_maxuse MB.<br /> Afgelopen maand bedroeg uw bandbreedte gebruik $bandwidth_currentuse MB.<br />  De overschrijding in bandbreedte gebruik voor uw account bedroeg afgelopen maand $bandwidth_overuse MB.<br /><br /> Om de toekomstige bereikbaarheid van uw domeinen te garanderen heeft u een hosting pakket met meer bandbreedte nodig. Hiertoe zullen wij binnenkort telefonisch contact met u opnemen. <br /><br /><table border='1' style='border-collapse:collapse;border-color:#ccc;' cellpadding='15px'><tbody><tr><td><strong>Hoofddomein binnen account</strong></td><td>{$domein}</td></tr></tbody></table><br /><br /> Met vriendelijke groet,<br /><br />Afd. administratie<br>Inuit Internet Diensten";
        }
        
        
        if ( $schijfruimte_currentuse > $schijfruimte_maxuse && !in_array($user,$excluded_users) ) { //check schijfruimte
            $schijfruimte_overuse = $schijfruimte_currentuse - $schijfruimte_maxuse;
            
            $sock->query('/CMD_API_SHOW_USER_CONFIG?user='.$user.'');
            $result = $sock->fetch_parsed_body();
           
            $CMD_email = $custdata[$user]; //first use wefact address
            if (empty($CMD_email)) {
                $CMD_email = $result['email'];
            }
            
            $domein = $result['domain'];
            $message .= "Geachte klant,<br /><br />Uw hosting pakket kent een limiet in schijfruimte gebruik van $schijfruimte_maxuse MB.<br /> Op dit moment bedraagd uw schijfruimte gebruik $schijfruimte_currentuse MB.<br />  De overschrijding in schijfruimte gebruik voor uw account bedraagd op dit moment $schijfruimte_overuse MB.<br /><br /> Om de toekomstige bereikbaarheid van uw domeinen te garanderen heeft u een hosting pakket met meer schijfruimte nodig. Hiertoe zullen wij binnenkort telefonisch contact met u opnemen. <br /><br /><table border='1' style='border-collapse:collapse;border-color:#ccc;' cellpadding='15px'><tbody><tr><td><strong>Hoofddomein binnen account</strong></td><td>{$domein}</td></tr></tbody></table><br /><br /> Met vriendelijke groet,<br /><br />Afd. administratie<br>Inuit Internet Diensten";
        }
        
        if (isset($message)) {
            if(function_exists('send_email_notification')) {
                //send_email_notification($CMD_email,$message);
            }
        }
        
        */
    }
    
}

if (function_exists('get_user_hostingaccount_quota')) {
    check_cmd_users();
    //get_user_hostingaccount_quota('rvr');
}
?>