<?php
require_once "wefact_api.php";
function get_company_invoices($bedrijf) {
    $api = new WeFactAPI();
    if (!empty($bedrijf)) {
    
        $parameters = array(
        "status"	=> 2,
        "sort" => 'InvoiceCode',
        "order" => 'ASC',
        "searchat" => 'DebtorCode',
        "searchfor" => $bedrijf
        );
        $parameters_partial = array(
        "status"	=> 3,
        "sort" => 'InvoiceCode',
        "order" => 'ASC',
        "searchat" => 'DebtorCode',
        "searchfor" => $bedrijf
        );

        $send_invoices_list = $api->sendRequest('invoice', 'list', $parameters);
        $partial_payed_invoice_list = $api->sendRequest('invoice', 'list', $parameters_partial);

        if ($send_invoices_list['status'] == 'success' && $partial_payed_invoice_list['status'] == 'success') { 
            
            $invoicearray = array();
           
            if ($send_invoices_list['totalresults'] > 0 && $partial_payed_invoice_list['totalresults'] > 0) {
                $invoicearray = array_merge($send_invoices_list['invoices'],$partial_payed_invoice_list['invoices']);
                usort($invoicearray, function($a,$b) {
                return $a['Identifier'] - $b['Identifier'];
                });
            }
            elseif ($send_invoices_list['totalresults'] > 0 && $partial_payed_invoice_list['totalresults'] < 1) {
                $invoicearray = $send_invoices_list['invoices'];
            }
            elseif ($send_invoices_list['totalresults'] < 1 && $partial_payed_invoice_list['totalresults'] > 0) {
                $invoicearray = $partial_payed_invoice_list['invoices'];
            }
            else { //no invoices
                return false;
                exit;
            }
            
            $overtime_array = array();
            $now = time();
            
            foreach($invoicearray as $invoice) {
                $ID = $invoice['InvoiceCode'];
                $bedrijfID = $invoice['DebtorCode'];
                
                $factuurdatum = $invoice['Date'];
                $duedate = strtotime($factuurdatum);
                $diff = abs($now - $duedate);
                $dagen = floor($diff/(60*60*24));
                $dagen_overtijd = $dagen - 30;
                $bedrag = $invoice['AmountIncl'];  
                
                if ($invoice['Status'] == 3) {
                    $invoicedetails = $api->sendRequest('invoice', 'show', array('InvoiceCode' => $invoice['InvoiceCode']));
                    if ($invoicedetails['status']=='success') {
                        $betaald = $invoicedetails['invoice']['AmountPaid'];
                        $bedrag = $bedrag - $betaald;
                    }
                }
                
                if ($dagen_overtijd > 0) {
                    $overtime_array[$ID]['Factuur datum'] = $factuurdatum;
                    $overtime_array[$ID]['Openstaand bedrag'] = $bedrag;
                    $overtime_array[$ID]['Dagen openstaand'] = $dagen;
                    $overtime_array[$ID]['Dagen overtijd'] = $dagen_overtijd;
                }
            }
            if (!empty($overtime_array)) {
                $overtime_array['bedrijfsdata']['contactpersoon']['voornaam'] = $invoicearray[0]['Initials'];
                $overtime_array['bedrijfsdata']['contactpersoon']['achternaam'] = $invoicearray[0]['SurName'];
                $DebtorCode = $invoicearray[0]['DebtorCode'];
                $bedrijfdata = $api->sendRequest('debtor', 'show', array('DebtorCode'	=> $DebtorCode));
                $aanhef = $bedrijfdata['debtor']['InvoiceSex'];
                if (empty($aanhef)) {$aanhef = $bedrijfdata['debtor']['Sex'];}
                $email = $bedrijfdata['debtor']['EmailAddress'];
                $secondEmail = $bedrijfdata['debtor']['SecondEmailAddress'];
                $invoiceEmail = $bedrijfdata['debtor']['InvoiceEmailAddress'];
                $overtime_array['bedrijfsdata']['email'] = $email; 
                $overtime_array['bedrijfsdata']['second_email'] = $secondEmail; 
                $overtime_array['bedrijfsdata']['invoice_email'] = $invoiceEmail; 
                $overtime_array['bedrijfsdata']['contactpersoon']['aanhef'] = $aanhef;
            }
            return $overtime_array;
        }
        else {
            echo 'Er ging iets mis bij het ophalen van de facturen.';
            exit();
        }
    }
    else {
        echo 'Geen bedrijf ingesteld.';
        exit();
    }
}
 
function email_customer_overtime_invoices($bedrijf) {
$overtime_array = get_company_invoices($bedrijf);
$tr_html = '';
if (!empty($overtime_array)) {
    foreach ($overtime_array as $ID => $values) {
        if ($ID != "bedrijfsdata") { 
            $tr_html .= "<tr style=\"font-size:13px;font-family:'Open Sans',sans-serif;\" height=\"35\">";
            $tr_html .= "<td style=\"font-size:13px;font-family:'Open Sans',sans-serif;\" align=\"center\">{$ID}</td>";      
            foreach($values as $key => $value) {
                if ($key == 'Openstaand bedrag'){$value = "&euro; $value";}
                $tr_html .= "<td style=\"font-size:13px;font-family:'Open Sans',sans-serif;\" align=\"center\">{$value}</td>";      
            }   
            $tr_html .= "</tr>";
        }
    }    
}
$email = $overtime_array['bedrijfsdata']['email'];
$secondEmail = $overtime_array['bedrijfsdata']['second_email'];
$invoiceEmail = $overtime_array['bedrijfsdata']['invoice_email'];

$aanhef = $overtime_array['bedrijfsdata']['contactpersoon']['aanhef'];
$voornaam = $overtime_array['bedrijfsdata']['contactpersoon']['voornaam'];
$achternaam = $overtime_array['bedrijfsdata']['contactpersoon']['achternaam'];

if ($aanhef == 'm'){$aanhef  = 'heer';}
elseif ($aanhef == 'f'){$aanhef = 'mevrouw';}
elseif ($aanhef == 'd'){$aanhef = 'heer/mevrouw';} 

if ($aanhef == 'heer' && !empty($achternaam) || $aanhef == 'mevrouw' && !empty($achternaam)) {
    $contactpersoon = $aanhef . ' ' . $achternaam; 
}
elseif ($aanhef == 'heer/mevrouw') {
    $contactpersoon = $aanhef; 
}
$message = 
"
<html>
<head>
</head>
<body style=\"font-size:13px;font-family:'Open Sans',sans-serif;line-height:24px;\">
<p style=\"font-size:13px;font-family:'Open Sans',sans-serif;\">
Geachte {$contactpersoon},<br /><br />
Bij deze willen wij u erop wijzen dat u momenteel achter loopt met het voldoen van een aantal openstaande vorderingen.<br /> Hieronder vind u een actueel overzicht van openstaande vorderingen die een betalingsachterstand hebben.<br /><br /> 
</p>
<table width=\"100%\" align=\"center\" cellspacing=\"5px\" style=\"padding-bottom:20px;font-size:13px;font-family:'Open Sans',sans-serif;\">
    <thead>
        <th height=\"80px\" width=\"20%\" style=\"border-bottom:1px solid #ccc;font-size:13px;font-family:'Open Sans',sans-serif;\">Factuur nummer</th>
        <th height=\"80px\" width=\"20%\" style=\"border-bottom:1px solid #ccc;font-size:13px;font-family:'Open Sans',sans-serif;\">Factuur datum</th>
        <th height=\"80px\" width=\"20%\" style=\"border-bottom:1px solid #ccc;font-size:13px;font-family:'Open Sans',sans-serif;\">Openstaand bedrag</th>
        <th height=\"80px\" width=\"20%\" style=\"border-bottom:1px solid #ccc;font-size:13px;font-family:'Open Sans',sans-serif;\">Leeftijd factuur in dagen</th>
        <th height=\"80px\" width=\"20%\" style=\"border-bottom:1px solid #ccc;font-size:13px;font-family:'Open Sans',sans-serif;\"><strong>Betalingsachterstand in dagen</strong></th>
    </thead>
    <tbody>
       {$tr_html}
    </tbody>
</table>
<br>
<p style=\"font-size:13px;font-family:'Open Sans',sans-serif;\">
    Wij verzoeken u vriendelijk doch dringend deze vorderingen zo spoedig mogelijk te voldoen op IBAN nummer NL93ABNA0521419573 t.n.v. Inuit Internet Diensten o.v.v. het factuurnummer teneinde de achterstand niet verder op te laten lopen.<br /><br /> Onze dank alvast.<br /><br />Met vriendelijke groet,<br /><br />Agnes Geuijen<br />Afd. administratie<br />Inuit Internet Diensten
</p>
</body>
</html>    
";

$to = "administratie@inuit-internet.nl";
if (!empty($invoiceEmail)) {
    $to     .= ",$invoiceEmail";
}
elseif (!empty($email)) {
    $to     .= ",$email";
}
elseif (!empty($secondEmail)) {
    $to     .= ",$secondEmail";
} 

$subject = "Overzicht openstaande vorderingen met een betalingsachterstand";
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: Administratie ~ Inuit Internet Diensten <administratie@inuit-internet.nl>' . "\r\n" .
'Reply-To: administratie@inuit-internet.nl' . "\r\n" .
'X-Mailer: PHP/' . phpversion();
if (mail($to, $subject, $message, $headers)) {echo "E-mails succesvol verzonden naar {$to}";} else {echo "email failed";}

//var_dump($message);
}   
$bedrijf = $_GET['bedrijf'];
$bedrijf = trim($bedrijf);
email_customer_overtime_invoices($bedrijf);