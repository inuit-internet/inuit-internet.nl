<?php
date_default_timezone_set('Europe/Amsterdam');
setlocale(LC_ALL, 'nl_NL');
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gruiters_klanten');

/** MySQL database username */
define('DB_USER', 'gruiters_klanten');

/** MySQL database password */
define('DB_PASSWORD', '7Kg16WEVFp6fdfzjZlWWTdAD3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DISALLOW_FILE_EDIT', TRUE); // Sucuri Security: Thu, 01 Dec 2016 10:35:28 +0000


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3TjlOrkL LfzR`*(/ _Vxo4<z29KO[mO{tGyScc-u~#G}f>EjROT;cf@)|Oe^-c$');
define('SECURE_AUTH_KEY',  ']lpJ -]@=#QA,YcbB,o]E1F+I#CyIS9B.J|,L{-2SoO1L&3S71 ;JM$2-X0fu?&~');
define('LOGGED_IN_KEY',    'z{M$[Cd-+>!WHp~~zWK8U%0x/{?$t<FWIZQE5{;htI#/rORq;P/&=4$|,dQj#:]R');
define('NONCE_KEY',        'vt0:b9yyW<()}bmU`/P3O=q{R_4/T5I&5~%4@K>,TYa[m++-]:QmB?-R %o+k-x6');
define('AUTH_SALT',        'T69T3CAXqzm|3dP>Z%:+c(nWUWJguChG-,zt^pxU ~(|N7CMyD(5uQ$2r2K<%Ft(');
define('SECURE_AUTH_SALT', 'u+c.-A;p-|C >=h1E,Huv2A2&1o:<R~t~U+I6&j9Sc|Y|[ ^L+zfO~!|GD!--,bq');
define('LOGGED_IN_SALT',   '${L;b=16SE}L9*7 SEe(-ov(wy2$o/xYF|%)-CT6=G0W@-aqQqpC,`<mJ*Gx-*id');
define('NONCE_SALT',       '~As9miT p&NXeC|g<`W+9|UXZ@f|%i2Yztpb;HyPs_gmRr/cXy-p-OR=^=Z|%I.}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_SITEURL', 'https://mijn.inuit-internet.nl/');
define('WP_HOME', 'https://mijn.inuit-internet.nl/');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
