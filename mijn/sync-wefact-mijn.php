<?php
require_once('wp-load.php');
require_once(WP_PLUGIN_DIR.'/wefact-api/index.php');

function sync() {
    $api = new WeFactAPI();
    $debtorsresult = $api->sendRequest('debtor', 'list', array());
    $invoiceresult = $api->sendRequest('invoice', 'list', array('limit'=>-1));
	
	if ($invoiceresult['status'] == 'success') {
		$invoicedebtors = array();
		$invoices = $invoiceresult['invoices'];
	    foreach($invoices as $invoice) {
			$invdeb = $invoice['DebtorCode'];
			$invoicedebtors[] = $invdeb;
		}
	}
	
    if($debtorsresult['status'] == 'success') { 
        $debtors = $debtorsresult['debtors'];
        
        foreach ($debtors as $debtor) {
            $debtorCode = $debtor['DebtorCode'];
            $debtorEmail = $debtor['EmailAddress'];
			$voornaam = $debtor['Initials'];
            $achternaam = $debtor['SurName'];
   
			$invoicecheck = 0;
			if (in_array($debtorCode,$invoicedebtors)) {
				$invoicecheck++; //debtor heeft facturen --> is Actief of Slapend
			}
			
			$groupcheck = 0;
			$debtordetails = $api->sendRequest('debtor', 'show', array('DebtorCode' => $debtorCode));
			$groups = $debtordetails['debtor']['Groups'];
			foreach($groups as $array) {
				if (in_array('Actief + Slapend',$array)) {
					$groupcheck++;
					//echo $debtorCode . '<br>';
				}
			}
			
			//continue;
			
			if ($groupcheck > 0) {  
				$usercheck = username_exists( $debtorCode );
				if ($usercheck === false) { //user bestaat nog niet, aanmaken
					//if ($debtorCode == 'DB0008') {
						$errors = register_new_user(  $debtorCode, $debtorEmail );
						if ( !is_wp_error($errors) ) {
							printf(__('Gebruiker %s aangemaakt <br />'),$debtorCode);
							$newuser = get_user_by( 'email',  $debtorEmail );
							$user_id = wp_update_user( array( 'ID' => $newuser->data->ID, 'first_name' => $voornaam, 'last_name' => $achternaam ) );
							
							$message = "Er is een gebruikersaccount aangemaakt voor {$voornaam} {$achternaam}, debtorcode: {$debtorCode}";
							wp_mail(get_option('admin_email'), sprintf(__('Nieuwe gebruiker aangemaakt met debtorcode [%s]'),$debtorCode), $message);
						}
						else {
							$message = "Er ging iets mis bij het aanmaken van gebruiker $debtorCode";
							wp_mail(get_option('admin_email'), sprintf(__('[%s] Er ging iets mis bij het aanmaken van de gebruiker'), $debtorCode), $message);
						}
					//}
				}
			}
        } 
    }
	
	wp_mail(get_option('admin_email'),'wefact mijn sync heeft gedraaid', 'wefact mijn sync heeft gedraaid');
}
sync();
?>