<?php
/**
 * Plugin Name: Custom registration email
 * Plugin URI: http://inuit-internet.nl
 * Description: custom message for new user registration
 * Version: 1.0.0
 * Author: Bas Gruijters
 * Author URI: http://inuit-internet.nl
 * Text Domain: 
 * Domain Path: 
 * Network: 
 * License:GPL2
 */
  
// Redefine user notification function
function set_content_type( $content_type ) {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'set_content_type' );


if ( !function_exists( 'wp_new_user_notification' ) ) :
function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
    //return;
    
    $user = new WP_User($user_id);
    $plaintext_pass = wp_generate_password(16); 
    wp_set_password( $plaintext_pass, $user_id ); 
    
    $user_login = stripslashes($user->user_login);
    $user_email = stripslashes($user->user_email);
    $voornaam = stripslashes($user->first_name);
    $achternaam = stripslashes($user->last_name);

    $message  = sprintf(__('Nieuwe gebruiker aangemaakt in %s:'), get_option('blogname')) . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";

    @wp_mail(get_option('admin_email'), sprintf(__('[%s] Registratie nieuwe gebruiker'), get_option('blogname')), $message);

    if ( empty($plaintext_pass) )
        return;

    $message  = sprintf(__('Geachte relatie, <br /><br />')) . "\r\n\r\n";
    $message .= sprintf(__("Er is een account voor u aangemaakt in %s.<br /><br /> In deze persoonlijke omgeving vindt u onder andere een overzicht van uw facturen en abonnementen.<br /> U kunt hier tevens uw persoonlijke gegevens inzien en indien nodig wijzigen.<br /><br />Hieronder vindt u uw persoonlijke inloggegevens voor %s:"), get_option('blogname'), get_option('blogname')) . "\r\n\r\n";
    $message .= sprintf(__('<br /><br />Gebruikersnaam: %s'), $user_login) . "\r\n";
    $message .= sprintf(__('<br />Wachtwoord: %s'), $plaintext_pass) . "\r\n\r\n";
    $message .= sprintf(__('<br /><br />U kunt hier inloggen: %s'), wp_login_url()) . "\r\n\r\n";
    $message .= sprintf(__('<br /><br />Mocht u vragen hebben, laat het mij dan weten via<br> %s.<br /><br />Met vriendelijke groet,<br /><br />Bas Gruijters<br />Inuit Internet Diensten'), get_option('admin_email')) . "\r\n\r\n";

    wp_mail($user_email, sprintf(__('[%s] Alstublieft, uw gebruikersnaam en wachtwoord voor mijn.inuit-internet.nl'), get_option('blogname')), $message);
    
}
endif;  