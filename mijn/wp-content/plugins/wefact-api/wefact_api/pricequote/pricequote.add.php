<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$priceQuoteParams = array(
				'DebtorCode'		=> 'DB0001',	// Customer information will automatically be fetched

				'PriceQuoteLines'	=> array(
					array(
						'Number'		=> 3,
						'ProductCode'	=> 'P0001'
					),
					array(
						'Description'	=> 'Service contract',
						'PeriodicType'	=> 'period',
						'Periods' 		=> 3,
						'Periodic' 		=> 'm',
						'PriceExcl' 	=> 150,
					)
				)
);

$response = $api->sendRequest('pricequote', 'add', $priceQuoteParams);

print_r_pre($response);

?>