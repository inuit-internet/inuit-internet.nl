<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$creditInvoiceParams = array(
				'CreditInvoiceCode' 	=> 'CF0001',
				
				'InvoiceLines' => array(
					array(
						'Identifier'	=> '3'
					)
				)
);

$response = $api->sendRequest('creditinvoiceline', 'delete', $creditInvoiceParams);

print_r_pre($response);

?>