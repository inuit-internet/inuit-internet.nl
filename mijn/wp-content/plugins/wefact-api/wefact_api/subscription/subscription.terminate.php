<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$subscriptionParams = array(
							'Identifier'		=> '1',
							'Subscription'		=> array(
														'TerminationDate'	=> '2014-06-01'
													)
);

$response = $api->sendRequest('subscription', 'terminate', $subscriptionParams);

print_r_pre($response);

?>