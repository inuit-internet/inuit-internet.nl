<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$attachmentParams = array(
				'CreditInvoiceCode'	=> 'CF0001',
				'Type'				=> 'creditinvoice',
				'Filename'			=> 'sample.pdf'
);

$response = $api->sendRequest('attachment', 'download', $attachmentParams);

print_r_pre($response);

?>