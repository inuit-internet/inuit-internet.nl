<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$attachmentParams = array(
				'ReferenceIdentifier'	=> 1,
				'Type'					=> 'invoice',
				'Filename'				=> 'sample.pdf'
);

$response = $api->sendRequest('attachment', 'delete', $attachmentParams);

print_r_pre($response);

?>