# Change Log

All notable changes to this project will be documented in this file.

This projects adheres to [Semantic Versioning](http://semver.org/) and [Keep a CHANGELOG](http://keepachangelog.com/).

## [Unreleased][unreleased]
-

## [1.0.1] - 2016-10-20
- Added transaction feedback status setting.
- Fixed - Too many arguments for function "__".

## 1.0.0 - 2016-07-06
- First release.

[unreleased]: https://github.com/wp-pay-gateways/ems-e-commerce/compare/1.0.1...HEAD
[1.0.1]: https://github.com/wp-pay-gateways/ems-e-commerce/compare/1.0.0...1.0.1
