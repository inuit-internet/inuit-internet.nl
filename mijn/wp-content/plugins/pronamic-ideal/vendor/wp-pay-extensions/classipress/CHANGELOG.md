# Change Log

All notable changes to this project will be documented in this file.

This projects adheres to [Semantic Versioning](http://semver.org/) and [Keep a CHANGELOG](http://keepachangelog.com/).

## [Unreleased][unreleased]
- 

## [1.0.2] - 2016-04-12
- No longer use camelCase for payment data.

## [1.0.1] - 2016-02-12
- Removed status code from redirect in update_status.

## 1.0.0 - 2015-05-26

### Added
- First release.

[unreleased]: https://github.com/wp-pay-extensions/classipress/compare/1.0.2...HEAD
[1.0.2]: https://github.com/wp-pay-extensions/classipress/compare/1.0.1...1.0.2
[1.0.1]: https://github.com/wp-pay-extensions/classipress/compare/1.0.0...1.0.1
