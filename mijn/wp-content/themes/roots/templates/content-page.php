<div class="container">
    <section class="section compact" role="region">
			<?php get_template_part('templates/parts/sidebar','menu'); ?>
			<div class="content">
				<div class="textwrapper">
                    <h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
                    <?php 
                    get_template_part('templates/content','loop'); 
                    ?>
                </div><!-- /textwrapper -->
			</div><!-- /content -->
			<?php include roots_sidebar_path(); ?>
	</section>
</div><!-- /container -->