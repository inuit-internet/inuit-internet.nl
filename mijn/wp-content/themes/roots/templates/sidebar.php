<?php
$sidebaritems = get_post_meta($post->ID,'sidebar-items',true);
$showdefaultitems = get_post_meta($post->ID,'sidebar-items-default-show',true);
if (!empty($sidebaritems)) :
?>
<aside class="aside">
    <?php
    foreach($sidebaritems as $sidebaritem) :
    $title = $sidebaritem['title'];
    $classes = $sidebaritem['item-classes'];
    $content = $sidebaritem['item-content'];
    $page_links = $sidebaritem['page-links'];
    if (!empty($page_links)) :
    ?>
    <div class="sidebox box-links <?php if (!empty($classes)) {echo $classes;} ?>">
        <?php 
        if (!empty($title)) : 
        ?>
        <h3><strong><?php echo $title; ?></strong></h3>
        <?php 
        endif; 
        ?>
        <ul class="list-links">
            <?php 
            foreach ($page_links as $item) :
            ?>
            <li>
                <a href="<?php echo get_permalink($item); ?>"><?php echo get_the_title($item); ?></a>
            </li>
            <?php
            endforeach;
            ?>
        </ul>
    </div><!-- /box-links -->
    <?php
    elseif (!empty($content)) :
    ?> 
    <div class="sidebox sidebox-content <?php if (!empty($classes)) {echo $classes;} ?>">
        <?php
        if (!empty($title)) : ?>
        <h3><strong><?php echo $title; ?></strong></h3>
        <?php 
        endif;
        $content = apply_filters('the_content', $content);
        echo $content; 
        ?>
    </div>
    <?php
    endif;
    endforeach;
    ?>
</aside>
<?php
endif;
?>