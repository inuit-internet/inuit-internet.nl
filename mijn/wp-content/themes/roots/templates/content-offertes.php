<?php
$contracten = get_current_user_contracts();
?>
<div class="container">
    <section class="section" role="region">
		<?php get_template_part('templates/parts/sidebar','menu'); ?>
		<div class="content">
			<?php
			if ( !empty($contracten) ) :
			?>
			<div id="tabs">
				<div class="tabs-container">
					<div id="tabs-1">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th class="tableheader header">Referentie</th>
                                    <th class="tableheader header headerSortDown">Datum</th>
                                    <th class="tableheader header">Status</th>
                                    <th class="tableheader header">PDF</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $nr = 0; 
                            foreach($contracten as $contract) : $nr++;
                            $status = '';
                            switch ($contract['Status']) {
                                case '3': $status = '<i class="fa fa-check"></i> Akkoord</a>';break;
                                default:  $status = '<i class="fa fa-exclamation"></i> Nog niet geaccordeerd';break;
                            }
                           
                            if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
                            ?>
                                <tr class="<?php echo $class; ?>">
                                    <td><?php echo $contract['PriceQuoteCode']; ?></td>
                                    <td><?php $date = date_create_from_format('Y-m-d', $contract['Date']); echo date_format($date, 'd-m-Y'); ?></td>
                                    <td><?php echo $status; ?></td>
                                   <td><i class="fa fa-file-pdf-o"></i> <a title="Download offerte" href="?download_pricequote=<?php echo $contract['PriceQuoteCode']; ?>"> download</a></td>
                                </tr>
                            <?php
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
			</div>
		</div>
		<?php
		else :
		?>
		<h1>Helaas konden we geen offertes vinden.</h1>
		<?php
		endif;
		?>
		</div><!-- /content -->
	</section>
</div><!-- /container -->