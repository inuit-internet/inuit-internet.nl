<?php
if ($_POST['gform_submit'] == 1 && $_SERVER['REQUEST_METHOD'] == 'POST') { //update gegevens
    unset($_POST['gform_submit']);
    
    if ( !filter_input(INPUT_POST, 'EmailAddress', FILTER_VALIDATE_EMAIL) )  {
        echo '<script>alert(\'Voer alstublieft een correct e-mail adres in.\');</script>';
    }
    else {
        update_current_user_details('',$_POST);
    } 
}
$debtordetails = get_current_user_details();
?> 
<div class="container">
    <section class="section" role="region">
			<?php get_template_part('templates/parts/sidebar','menu'); ?>
			<div class="content">
				<h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
				<?php if (!empty($debtordetails)) : ?>
				<div class="gf_browser_chrome gform_wrapper columns_wrapper" id="gform_wrapper_1">
					<form method="post" enctype="multipart/form-data" id="gegevens" class="columns" action="">
						<div class="gform_heading">
						</div>
						<div class="gform_body">
							<ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below">
								<?php 
								$nr = 0;
								foreach($debtordetails as $key => $value) :  $nr++;
								if ($nr % 2 == 0) {$class = 'floatright';} else {$class = 'floatleft';}
								?>
								<li id="field_1_<?php echo $nr; ?>" class="gfield <?php echo $class; ?> field_sublabel_below field_description_below gfield_visibility_visible">
									<label class="gfield_label" for="input_1_<?php echo $nr; ?>"><?php echo key($value); ?></label>
									<div class="ginput_container ginput_container_text">
									<?php 
									if (key($value) == 'Aanhef') : //var_dump( $value );
									?>
									<select class="medium" name="Sex" tabindex="<?php echo $nr; ?>" aria-required="true">
										<option value="m" <?php if ($value['Aanhef'] == 'Dhr.') {echo "selected=\"selected\"";} ?>>Dhr.</option>
										<option value="f" <?php if ($value['Aanhef'] == 'Mevr.') {echo "selected=\"selected\"";} ?>>Mevr.</option>
									</select>
									<?php
									else :
									?>
									<input name="<?php echo $key; ?>" id="input_1_<?php echo $nr; ?>" type="text" value="<?php echo current($value); ?>" class="medium" tabindex="<?php echo $nr; ?>" aria-required="true" aria-invalid="false" placeholder="<?php echo key($value); ?>"/>
									<?php
									endif;
									?>
									</div>
								</li> 
								<?php 
								endforeach;
								?>
							</ul>
						</div>
						
						<div class="gform_footer cf top_label">  
							<input type="submit" class="btn-1 gform_button floatright" id="gform_submit_button_1" value="Bijwerken"> 
							<input type="hidden" class="gform_hidden" name="gform_submit" value="1">
						</div>
						
					</form>
				</div>
				<?php
				else : 
				?>
				<h1>Helaas hebben we geen gegevens kunnen vinden. Neemt u altublieft contact op. </h1>
				<?php
				endif; 
				?>
			</div><!-- /content -->
			<?php 
            session_destroy();
            ?>
	</section>
</div><!-- /container -->