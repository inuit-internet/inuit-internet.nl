<section class="section" role="region"> 
    <div class="container">
		<?php get_template_part('templates/parts/sidebar','menu'); ?>
		<div class="content">
            <h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
            <?php
            get_template_part('templates/content','loop'); 
			?>
        </div>
    </div>
</section>