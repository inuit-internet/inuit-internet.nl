<footer class="footer" role="contentinfo">
    <div class="container">
        <div class="column">
            <div class="copyright">
                 <?php get_template_part('templates/content','vcard'); ?>
            </div>
        </div><!-- /column -->
    </div>
</footer>