<div class="container">
	<section class="section" role="region">
		<div class="box-services">
			<div class="row">
				 <div class="column full">
			
					<?php
					$img_html = get_post_meta($post->ID,'page_item_img_html',true);
					if (!empty($img_html)) :
					?>
					<?php echo $img_html; ?>
					<?php
					endif;
					?>
			
					<?php
					$intro = get_post_meta($post->ID,'page_intro',true); 
					?>
					<div class="content">
						<h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
						<p>
						<?php
						if (!empty($intro)) {
							echo $intro; 
						}
						else {
							get_template_part('templates/content','loop'); 
						}
						?>
						</p>
					</div><!-- /content -->
				</div>
			</div>
		</div><!-- /box-intro -->
	</section>
</div><!-- /container -->