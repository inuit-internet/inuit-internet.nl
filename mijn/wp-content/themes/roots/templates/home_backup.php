<section class="section" role="region"> 
    <div class="container">
		<?php get_template_part('templates/parts/sidebar','menu'); ?>
		<div class="content">
            <h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
            <?php
            get_template_part('templates/content','loop'); 
            
            $producten = get_current_user_products();
            $contracten = get_current_user_contracts();
            $currdate = date('Y-m-d');
            if (empty($facturen) && empty($producten) && empty($contracten) && empty($domeinen)) :
			?>
				<p>Op dit moment zijn er (nog) geen gegevens voor u beschikbaar.</p>
			<?php
			else :
			?>
            <div id="tabs">
                <div class="tabs-header cf">
                    <ul>
                        <?php 
                        if (!empty($producten)) :
                        ?>
                        <li><a href="#tabs-1"><?php _e('Mijn abonnementen'); ?></a></li>
                        <?php
                        endif;
                        if (!empty($facturen)) :
                        ?>
                        <li><a href="#tabs-2"><?php _e('Mijn facturen'); ?></a></li>
                        <?php 
                        endif;
                        if (!empty($contracten)) :
                        ?>
                        <li><a href="#tabs-3"><?php _e('Mijn contracten'); ?></a></li>
                        <?php
                        endif;
                        if (!empty($domeinen)) :
                        ?>
                        <li><a href="#tabs-4"><?php _e('Mijn domeinen'); ?></a></li>
                        <?php
                        endif;
                        if (is_array($stats) && !empty($stats)) :
                        ?>
                        <li><a href="#tabs-5"><?php _e('Mijn webhosting'); ?></a></li>
                        <?php
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="tabs-container">
                    <?php
                    if (!empty($producten)) :
                    ?>
                    <div id="tabs-1">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th width="50%" class="tableheader header">Product omschrijving</th>
                                    <th class="tableheader header">Status</th>
                                    <th class="tableheader header">Periode</th>
                                    <th class="tableheader header">Bedrag (incl. BTW)</th>
                                    <th class="tableheader header">Verloopdatum</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $nr = 0; 
                            $productdetails = get_product_details();
                            
                            foreach($producten as $product) : $nr++;
                            if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
                            $Periodic = '';
                            switch ($product['Periodic']) {
                                case 'm': $Periodic = 'maand';break;
                                case 'j': $Periodic = 'jaar';break;  
                                case 'd': $Periodic = 'dag';break;  
                                case 'w': $Periodic = 'week';break;  
                                case 'k': $Periodic = 'kwartaal';break;  
                                case 'h': $Periodic = 'halfjaar';break;  
                                case 't': $Periodic = 'twee jaar';break;  
                            }
                            switch ($product['Status']) {
                                case '1': $Status = 'Actief';break;
                                default:  $Status = 'Opgezegd';break; 
                            }
                            
                            $pcode = $product['ProductCode'];
                            $pname = $productdetails[$pcode];
                            
                            if (!empty($pname)) {
                                $pname .= ' - ';
                            }
                            ?>
                                <tr class="<?php echo $class; ?>">
                                    <td><?php echo $pname . $product['Description']; ?></td>
                                    <td><?php echo $Status; ?></td>
                                    <td><?php printf(__('%s %s'),$product['Periods'],$Periodic); ?></td>
                                    <td><?php printf(__('&euro; %s'),$product['AmountIncl']); ?></td>
                                    <td><?php $date = date_create_from_format('Y-m-d', $product['TerminationDate']); if (!empty($date)) { if ($currdate > $product['TerminationDate']) { echo 'Verlopen op ' . date_format($date, 'd-m-Y'); } else {echo date_format($date, 'd-m-Y');} } else {echo '-';} ?></td>
								</tr>
                            <?php
                            endforeach;
                            ?>                          
                            </tbody>
                        </table>
                    </div>
                    <?php
                    endif;
                    if (!empty($facturen)) : 
                    ?>
                    <div id="tabs-2">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th class="tableheader header">Factuurnr</th>
                                    <th class="tableheader header headerSortDown">Factuurdatum</th>
                                    <th class="tableheader header">Bedrag (incl. BTW)</th>
                                    <th class="tableheader header">Status</th>
                                    <th class="tableheader header">       </th>
                                    <th class="tableheader header">PDF</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $nr = 0; 
                            foreach($facturen as $factuur) :
							if ($factuur['Status'] !== '0') :
							$nr++;
                            if ($factuur['Status'] == '2' || $factuur['Status'] == '3') {
                                $paymenturl = get_invoice_payment_url($factuur['InvoiceCode']);
                            }
                            else {
                                $paymenturl = '';
                            }
                            $status = '';
                            switch ($factuur['Status']) {
                                case '2': $status = '<i class="fa fa-exclamation"></i> Nog niet voldaan';break;
                                case '3': $status = '<i class="fa fa-exclamation"></i> Deels voldaan';break;  
                                case '4': $status = '<i class="fa fa-check"></i> Voldaan';break;
                                case '8': $status = '<i class="fa fa-undo"></i> Credit factuur';break;
                                case '9': $status = '<i class="fa fa-ban"></i> Vervallen';break;
                            }
                            
                            if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
                            ?>
                                <tr class="<?php echo $class; ?>">
                                    <td><?php echo $factuur['InvoiceCode']; ?></td>
                                    <td><?php $date = date_create_from_format('Y-m-d', $factuur['Date']); echo date_format($date, 'd-m-Y'); ?></td>
                                    <td><?php printf(__('&euro; %s'),$factuur['AmountIncl']); ?></td>
                                    <td><?php echo $status; ?></td>
                                    <td><?php if (!empty($paymenturl)) : ?><i class="fa fa-credit-card"></i> <a target="_blank" href="<?php echo $paymenturl; ?>">direct online betalen</a><?php endif; ?></td>
                                    <td><i class="fa fa-file-pdf-o"></i> <a title="Download factuur" href="?download_invoice=<?php echo $factuur['InvoiceCode']; ?>">download</a></td>
                                </tr>
                            <?php
							endif;
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    endif;
                    if (!empty($contracten)) :
                    ?>
                    <div id="tabs-3">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th class="tableheader header">Referentie</th>
                                    <th class="tableheader header headerSortDown">Datum</th>
                                    <th class="tableheader header">Status</th>
                                    <th class="tableheader header">PDF</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $nr = 0; 
                            foreach($contracten as $contract) : $nr++;
                            $status = '';
                            switch ($contract['Status']) {
                                case '3': $status = '<i class="fa fa-check"></i> Akkoord</a>';break;
                                default:  $status = '<i class="fa fa-exclamation"></i> Nog niet geaccordeerd';break;
                            }
                           
                            if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
                            ?>
                                <tr class="<?php echo $class; ?>">
                                    <td><?php echo $contract['PriceQuoteCode']; ?></td>
                                    <td><?php $date = date_create_from_format('Y-m-d', $contract['Date']); echo date_format($date, 'd-m-Y'); ?></td>
                                    <td><?php echo $status; ?></td>
                                   <td><i class="fa fa-file-pdf-o"></i> <a title="Download offerte" href="?download_pricequote=<?php echo $contract['PriceQuoteCode']; ?>"> download</a></td>
                                </tr>
                            <?php
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    endif;
                    if (!empty($domeinen)) : 
                    ?>
                    <div id="tabs-4">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th class="tableheader header">Domein</th>
                                    <th class="tableheader header headerSortUp">Bandbreedte gebruik huidige maand</th>
                                    <th class="tableheader header headerSortUp">Schijfruimte gebruik</th>
                                    <th class="tableheader header headerSortUp">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $nr = 0; 
							$domeinen = array_unique($domeinen);
                            foreach($domeinen as $domein) : 
                            if (!empty($domein)) :
                            $nr++; 
                            
                            $status = check_domain($domein);
                            switch ( $status ) {
                                case 0: $status = '<i class="fa fa-exclamation"></i> Niet bereikbaar</a>';break;
                                case 200: $status = '<i class="fa fa-check"></i> Bereikbaar';break;
                                default: $status = '<i class="fa fa-exclamation"></i> Issue</a>';break;
                            }
                            if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
                            
							if ($stats) {
								foreach($stats as $user => $domains) {
									foreach($domains as $domain => $values) {
										if ($domain == $domein) {
											$bandbreedte = $values['bandbreedte'];
											//$bandbreedte = $bandbreedte /1024; //GB
											$schijfruimte = $values['schijfruimte'];
											//$schijfruimte = $schijfruimte /1024; //GB
										}
									}
								}
							}
                            ?>
                                <tr class="<?php echo $class; ?>">
                                    <td><a href="http://<?php echo $domein; ?>" target="_blank"><?php echo $domein ?></a></td>
                                    <td><?php if (isset($bandbreedte) && !empty($bandbreedte)) {printf(__('%s MB'),round($bandbreedte,2,PHP_ROUND_HALF_UP));} else {echo 'Onbekend';} ?></td>
                                    <td><?php if (isset($schijfruimte) && !empty($schijfruimte)) {printf(__('%s MB'),round($schijfruimte,2,PHP_ROUND_HALF_UP));} else {echo 'Onbekend';} ?></td>
                                    <td><?php echo $status; ?></td>
                                </tr>
                            <?php
                            endif;
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    endif;
                    if (is_array($stats) && !empty($stats)) : 
                    ?>
                    <div id="tabs-5">
                        <table class="tablesorter">
                            <thead>
                                <tr>
                                    <th class="tableheader header">Accountnaam</th>
                                    <th class="tableheader header headerSortUp">Bandbreedte gebruik huidige maand</th>
                                    <th class="tableheader header headerSortUp">Schijfruimte gebruik</th>
                                    <th class="tableheader header headerSortUp">Aantal e-mail accounts</th>
                                    <th class="tableheader header headerSortUp">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $nr = 0; 
                            foreach($stats as $user => $domains) : $nr++; 
                            $daUserStats = collect_DA_user_stats($user);
                            switch ( $daUserStats['suspended'] ) {
                                case 'no':  $status = '<i class="fa fa-check"></i> Actief';break;
                                default:    $status = '<i class="fa fa-exclamation"></i> Opgeschord</a>';break;
                            }
                            
                            $bandbreedte = $daUserStats['bandwidthUsage']; //$bandbreedte = $bandbreedte / 1024; //GB
                            $schijfruimte = $daUserStats['diskUsage']; //$schijfruimte = $schijfruimte / 1024; //GB 
                            ?>
                                <tr class="<?php echo $class; ?>">
                                    <td><?php echo $user; ?></a></td>
                                    <td><?php printf(__('%s MB'),round($bandbreedte,2,PHP_ROUND_HALF_UP)); ?></td>
                                    <td><?php printf(__('%s MB'),round($schijfruimte,2,PHP_ROUND_HALF_UP)); ?></td>
                                    <td><?php echo $daUserStats['popAccountsUsage']; ?></td>
                                    <td><?php echo $status; ?></td>
                                </tr>
                            <?php
							endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
			<?php
			endif;
			?>
        </div>
    </div>
</section>