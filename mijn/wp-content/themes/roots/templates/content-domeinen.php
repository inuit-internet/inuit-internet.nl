<?php
$producten = get_current_user_products();
$domeinen = get_current_user_domains($producten);

?>
<div class="container">
    <section class="section" role="region">
		<?php get_template_part('templates/parts/sidebar','menu'); ?>
		<div class="content">
			<?php
			if (!empty($domeinen)) : 
			?>
			<div id="tabs">
				<div class="tabs-container">
					<div id="tabs-1">
						<table class="tablesorter">
							<thead>
								<tr>
									<th class="tableheader header">Domein</th>
									<th class="tableheader header headerSortUp">Status</th>								
								</tr>
							</thead>
							<tbody>
							<?php 
							$nr = 0; 
							$domeinen = array_unique($domeinen);
							foreach($domeinen as $domein) : 
							if (!empty($domein)) :
							$nr++; 
							if ($nr % 2 == 0) {
								$class = 'even';
							} else {
								$class = 'odd';
							}
							?>
								<tr class="<?php echo $class; ?>">
									<td><?php echo $domein; ?></td>
									<td>Actief</td>
								</tr>
							<?php
							endif;
							endforeach;
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>	
			<?php
			else :
			?>
			<h1>Helaas konden we geen domeinen vinden gekoppeld aan uw klant account.</h1>
			<?php
			endif;
			?>
		</div><!-- /content -->
	</section>
</div><!-- /container -->