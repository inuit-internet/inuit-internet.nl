<header class="header" role="banner"> 
    <div class="container">
        <a href="<?php echo home_url(); ?>/" title="Logo van <?php bloginfo('name'); ?>" class="logo">
            <span><?php bloginfo('name'); ?></span>
        </a><!-- /logo -->
        <ul id="skip">
            <li><a href="#nav"><span><?php _e('Direct naar:'); ?></span><?php _e('Navigatie'); ?></a></li>
            <?php
            $sitemapid = get_id_by_template('page-sitemap.php');
            if ($sitemapid) :
            ?>
            <li><a href="<?php echo get_permalink(26); ?>"><span><?php _e('Direct naar:'); ?> </span><?php _e('Sitemap'); ?></a></li>
            <?php
            endif; 
            ?>
        </ul><!-- /skip -->
        <?php 
        if (has_nav_menu('primary_navigation')) : 
        ?>
		<nav id="nav" role="navigation">
            <h2><?php _e('Navigatie'); ?></h2>
			<a class="nav-link" href="#nav">
                <span aria-hidden="true"><i class="fa fa-bars"></i></span>
                <span class="screen-reader-text">Menu</span>
            </a>
            <div class="nav-main" id="nav-main">
                <ul id="menu-hoofd-navigatie" class="menu">
					
                    <li class="switch"> 
                        <span class="inner-switch"><i class="fas fa-sun"></i></span>
                    </li>

                    <?php
					$args = array(
						'theme_location'  => 'primary_navigation',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '%3$s',
						'depth'           => 2,
						'walker'          => new Main_Nav_Walker()
					);
					wp_nav_menu($args);
                    if (is_user_logged_in()) : 
                    $user = wp_get_current_user(); //var_dump($user);
                    $username = $user->user_login;
					$debiteur = get_user_meta($user->ID,'debiteur',true);
                    if (empty($debiteur)) {
                        $api = new WeFactAPI();    
                        $debtorsearch = $api->sendRequest('debtor', 'show', array(
                            'DebtorCode' => $user->user_login
                        ));
                        if ($debtorsearch['status'] == 'success') {
                            $debiteur = $debtorsearch['debtor']['CompanyName'];
                        }
                    }
                    ?>
                    <li <?php if (is_page('mijn-gegevens')) : ?>class="current-menu-item"<?php endif; ?>>
                        <a href="/mijn-gegevens"><i class="fa fa-user"> </i>  <?php echo $debiteur; ?></a>
                        <div class="submenu">
                            <ul>
                                <li>
                                    <a href="/mijn-gegevens"><i class="fa fa-user "></i> Mijn gegevens</a>
                                </li>
                                <li>
                                    <a href="?action=logout"><i class="fa fa-power-off"></i> Uitloggen</a>
                                </li>
                            </ul> 
                        </div>
                    </li>
                    <?php
					if (check_role('administrator') || $_SESSION['is_admin'] == '1') :
					$customers = get_customers(); 
                    ?>
					<li>
						<form method="POST" action="" name="select-customer">&nbsp;
							<select name="customer-select" id="customer-select">
									<option value="">Selecteer klant</option>
									<?php 
									foreach ($customers as $ID => $name) {
										$selected = '';
										if ($user->ID == $ID) {$selected = 'selected="selected"';}
										print '<option '.$selected.' value="'.$ID.'">'.$name.'</option>';
									}
									?>
							</select> 
						</form>
					</li>
					<?php
					endif;
					endif;
                    ?>				
                </ul>	
            </div><!-- /nav main -->
        </nav><!-- /nav -->
		<?php endif; ?>
    </div><!-- /container -->
</header>