<?php
$sidebaritems = ot_get_option('sidebar-menu');
?>
<aside class="aside">
	<?php
	if (!empty($sidebaritems)) :
    ?>
    <div class="sidebox box-links <?php if (!empty($classes)) {echo $classes;} ?>">
        <ul class="list-links">
            <?php 
            foreach($sidebaritems as $sidebaritem) :
			$pageid = $sidebaritem['menu-item'];
			if (!empty($pageid)) :
			$title = $sidebaritem['title'];
			if (empty($title)) {
				$title = get_the_title($pageid);
			}
            $permalink = get_permalink($pageid);
            $icon = $sidebaritem['menu-item-icon-class']; 
            ?>
            <li <?php if($pageid == $post->ID) : ?>class="active"<?php endif; ?>>
                <a href="<?php echo $permalink; ?>"><?php if (!empty($icon)) { echo '<i class="'.$icon.'"></i> '; } ?><?php echo $title; ?></a>
            </li>
            <?php
			endif;
            endforeach;
            ?>
        </ul>
    </div><!-- /box-links -->
	<?php
	endif;
	?>
</aside>

