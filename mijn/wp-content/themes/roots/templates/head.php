<?php
if ($_GET['action']) {
	wp_logout(); 
	auth_redirect();
}

if ( check_role('administrator') == true && isset($_POST['customer-select']) || $_SESSION['is_admin'] == '1' ) {
	$customerID = $_POST['customer-select'];
	$user = get_user_by( 'id', $customerID ); 
	if( $user ) {
		wp_set_current_user( $user_id, $user->user_login );
		session_start(); 
	    $_SESSION['is_admin'] = '1'; 	
	}
}
else {
	session_start(); 
	$_SESSION['is_admin'] = '0'; 		
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="author" content="Inuit Internet Diensten" />
<meta name="google-site-verification" content="veH8K7_TkIDc7kmhGrmDTDQdjE6lKhixweBnL60P0Vs" />
<meta name="viewport" content="width=device-width,initial-scale=1" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<!--[if IEMobile]><meta http-equiv="cleartype" content="on" /><![endif]-->
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#603cba">
<meta name="theme-color" content="#ffffff">

<title><?php wp_title(); ?></title>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/static/fonts/font-awesome/css/all.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/static/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/static/css/style.css" />
<script src="<?php echo get_template_directory_uri(); ?>/static/js/vendor/modernizr-2.5.3.min.js"></script>
<!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/static/js/vendor/respond.min.js"></script>
<![endif]-->
<script src="<?php echo get_template_directory_uri(); ?>/static/js/libs/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/static/js/libs/jqui.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/static/js/libs/jquery.tablesorter.min.js"></script>
<script>
function load() { 
    var storage = window.sessionStorage;
    
    if (storage.getItem("mode") == "dark") {
        jQuery( "html,body" ).addClass( "dark" );
        jQuery( ".inner-switch .fas" ).addClass("fa-moon").removeClass( "fa-sun" );
    }

    // Controleer LocalStorage op bestaande sleutel en detecteer vervolgens Browser "geeft de voorkeur aan kleurenschema" en stel Modus in
    var mq = window.matchMedia( '(prefers-color-scheme: dark)' );
    if (storage.getItem("mode") == "light"){
        jQuery( "html,body" ).removeClass( "dark" );
        jQuery( ".inner-switch .fas" ).addClass("fa-sun").removeClass( "fa-moon" );
    } else if ( mq.matches ){
        jQuery( "html,body" ).addClass( "dark" );
        jQuery( ".inner-switch .fas" ).addClass("fa-moon").removeClass( "fa-sun" );
    }
};
load();
</script>
<?php
wp_head(); 
?>
</head>