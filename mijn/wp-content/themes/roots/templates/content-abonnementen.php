<?php
$producten = get_current_user_products();
?>
<div class="container">
    <section class="section" role="region">
		<?php get_template_part('templates/parts/sidebar','menu'); ?>
		<div class="content">
			<?php
			if ( !empty($producten) ) :
			?>
			<div id="tabs">
				<div class="tabs-container">
					<div id="tabs-1">
						<div style="overflow-x:auto;">
							<table class="tablesorter">
								<thead>
									<tr>
										<th width="50%" class="tableheader header">Product omschrijving</th>
										<th class="tableheader header">Status</th>
										<th class="tableheader header">Periode</th>
										<th class="tableheader header">Bedrag (incl. BTW)</th>
										<th class="tableheader header">Verloopdatum</th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$nr = 0; 
								$productdetails = get_product_details();
								
								foreach($producten as $product) : $nr++;
								if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
								$Periodic = '';
								switch ($product['Periodic']) {
									case 'm': $Periodic = 'maand';break;
									case 'j': $Periodic = 'jaar';break;  
									case 'd': $Periodic = 'dag';break;  
									case 'w': $Periodic = 'week';break;  
									case 'k': $Periodic = 'kwartaal';break;  
									case 'h': $Periodic = 'halfjaar';break;  
									case 't': $Periodic = 'twee jaar';break;  
								}
								switch ($product['Status']) {
									case '1': $Status = 'Actief';break;
									default:  $Status = 'Opgezegd';break; 
								}
								
								$pcode = $product['ProductCode'];
								$pname = $productdetails[$pcode];
								
								if (!empty($pname)) {
									$pname .= ' - ';
								}
								?>
									<tr class="<?php echo $class; ?>">
										<td><?php echo $pname . $product['Description']; ?></td>
										<td><?php echo $Status; ?></td>
										<td><?php printf(__('%s %s'),$product['Periods'],$Periodic); ?></td>
										<td><?php printf(__('&euro; %s'),$product['AmountIncl']); ?></td>
										<td><?php $date = date_create_from_format('Y-m-d', $product['TerminationDate']); if (!empty($date)) { if ($currdate > $product['TerminationDate']) { echo 'Verlopen op ' . date_format($date, 'd-m-Y'); } else {echo date_format($date, 'd-m-Y');} } else {echo '-';} ?></td>
									</tr>
								<?php
								endforeach;
								?>                          
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php
			else :
			?>
			<h1>Helaas konden we geen abonnementen vinden.</h1>
			<?php
			endif;
			?>
		</div><!-- /content -->
	</section>
</div><!-- /container -->