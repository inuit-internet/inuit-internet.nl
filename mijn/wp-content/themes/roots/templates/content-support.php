<div class="container">
    <section class="section" role="region">
			<?php get_template_part('templates/parts/sidebar','menu'); ?>
			<div class="content">
				<h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
				<?php 
				get_template_part('templates/content','loop'); 
				$entries = get_gf_user_entries();
				if (!empty($entries)) :
				?>
				<h2>Overzicht van uw support vragen</h2>
				<table class="tablesorter">
					<thead>
						<tr>
							<th class="tableheader header" width="80%">Support vraag</th>
							<th class="tableheader header headerSortUp" width="20%">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$nr = 0;
						foreach($entries as $entry) : $nr++;
						if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
						$starred = $entry['is_starred'];
						if ($starred == 1) {
							$status = 'Opgelost';
							$icon = 'fa-check';
						}
						else {
							$status = 'In behandeling';
							$icon = 'fa-clock-o';
						}
						?>
						<tr class="<?php echo $class; ?>">
							<td><?php echo $entry[4]; ?></td>
							<td><?php printf(__('<i class="fa %s"></i> %s'), $icon, $status); ?></td>
						</tr>
						<?php
						endforeach;
						?>
					</tbody>
				</table>
				<?php
				endif;
				?>
			</div><!-- /content -->
			<?php include roots_sidebar_path(); ?>
	</section>
</div><!-- /container -->