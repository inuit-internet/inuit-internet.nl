<?php
$producten = get_current_user_products();
$domeinen = get_current_user_domains($producten);

$hostingstats = array();
if ($domeinen && !empty($domeinen)) { //var_dump($domeinen);
	$daUsers = get_current_user_DA_users($domeinen); //var_dump($daUsers);
	$daUsers = array_unique($daUsers);
	if ($daUsers && !empty($daUsers)) {
		foreach($daUsers as $daUser) {
                        $stats = collect_DA_user_stats($daUser); //var_dump($stats);
			$hostingstats[] = $stats;
		}
	}
}
?>
<div class="container">
    <section class="section" role="region">
		<?php get_template_part('templates/parts/sidebar','menu'); ?>
		<div class="content">
			<?php
			if (!empty($domeinen)) : 
			?>
			<div id="tabs">
				<div class="tabs-container">
					<div id="tabs-1">
						<table class="tablesorter">
							<thead>
								<tr>
									<th class="tableheader header">Account ID</th>
									<th class="tableheader header headerSortUp">Actueel bandbreedte gebruik</th>
									<th class="tableheader header headerSortUp">Actueel schijfruimte gebruik</th>
									<th class="tableheader header headerSortUp">Schijfruimte limiet</th>
								</tr>
							</thead>
							<tbody>
							<?php 
							$nr = 0; 
							$domeinen = array_unique($domeinen);
							foreach($hostingstats as $userstats) : 
							if (!empty($userstats)) :
							$nr++; 
							if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
							?>
								<tr class="<?php echo $class; ?>">
									<td><?php echo $userstats['accountname']; ?></td>
									<td><?php printf(__('%s MB'),$userstats['bandwidthUsage']); ?></td>
									<td><?php printf(__('%s MB'),$userstats['diskUsage']); ?></td>
									<td><?php printf(__('%s MB'),$userstats['diskMax']); ?></td>
								</tr>
							<?php
							endif;
							endforeach;
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>	
			<?php
			else :
			?>
			<h1>Helaas konden we geen webhosting accounts vinden gekoppeld aan uw klant account.</h1>
			<?php
			endif;
			?>
		</div><!-- /content -->
	</section>
</div><!-- /container -->