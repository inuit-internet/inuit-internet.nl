<?php
$facturen = get_current_user_invoices();
?>
<div class="container">
    <section class="section" role="region">
		<?php get_template_part('templates/parts/sidebar','menu'); ?>
		<div class="content">
			<?php
			if ( !empty($facturen) ) :
			?>
			<div id="tabs">
				<div class="tabs-container">
				<div id="tabs-1">
					<div style="overflow-x:auto;">
						<table class="tablesorter">
							<thead>
								<tr>
									<th class="tableheader header headerSortUp">Factuurnr</th>
									<th class="tableheader header">Factuurdatum</th>
									<th class="tableheader header">Status</th>
									<th class="tableheader header">Bedrag (incl. BTW)</th>
									<th class="tableheader header">Online voldoen</th>
									<th class="tableheader header">PDF</th>
								</tr>
							</thead>
							<tbody>
							<?php 
                                                         // Sort the multidimensional array
  
   
     function custom_sort($a,$b) {
          return $a['Status']>$b['Status'];
     }
   usort($facturen, "custom_sort");

							$nr = 0; 
							foreach($facturen as $factuur) : //var_dump($factuur);
							$amount = $factuur['AmountIncl'];
							$amountdue = NULL;
							
							if ($factuur['Status'] !== '0') :
								$nr++;
							
								if ($factuur['Status'] == '2' || $factuur['Status'] == '3') {
									$paymenturl = get_invoice_payment_url($factuur['InvoiceCode']);
								}
								else {
									$paymenturl = '';
								}	
								
								if ($factuur['Status'] == '3') {
									$amountdue = get_invoice_amount_due($factuur['InvoiceCode']);
								}
								
								$status = '';
								switch ($factuur['Status']) {
									case '2': $status = '<i class="fa fa-exclamation"></i> Nog niet voldaan';break;
									case '3': $status = '<i class="fa fa-exclamation"></i> Deels voldaan';break;  
									case '4': $status = '<i class="fa fa-check"></i> Voldaan';break;
									case '8': $status = '<i class="fa fa-undo"></i> Credit factuur';break;
									case '9': $status = '<i class="fa fa-ban"></i> Vervallen';break;
								}
								
								if ($nr % 2 == 0) {$class = 'even';} else {$class = 'odd';}
								?>
								<tr class="<?php echo $class; ?>">
									<td><?php echo $factuur['InvoiceCode']; ?></td>
									<td><?php $date = date_create_from_format('Y-m-d', $factuur['Date']); echo date_format($date, 'd-m-Y'); ?></td>
									<td><?php echo $status; ?></td>
									<td><?php printf(__('&euro; %s'),$amount); if (isset($amountdue) && !empty($amountdue)) {printf(__(' (nog te voldoen: &euro; %s)'),$amountdue);} ?></td>
									<td><?php if (!empty($paymenturl)) : ?><i class="fa fa-credit-card"></i> <a target="_blank" href="<?php echo $paymenturl; ?>">direct online betalen</a><?php endif; ?></td>
									<td><i class="fa fa-file-pdf-o"></i> <a title="Download factuur" href="?download_invoice=<?php echo $factuur['InvoiceCode']; ?>">download</a></td>
								</tr>
							<?php
							endif;
							endforeach;
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php
		else :
		?>
		<h1>Helaas konden we geen facturen vinden.</h1>
		<?php
		endif;
		?>
	</div><!-- /content -->
	</section>
</div><!-- /container -->