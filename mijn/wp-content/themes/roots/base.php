<?php
date_default_timezone_set("Europe/Amsterdam");
setlocale(LC_ALL, 'nl_NL');
get_template_part('templates/head'); 
$template = get_page_template_slug($post->ID);
if ($template == 'page-home.php') {
    $extra = 'page-homepage';
}
elseif ($template == 'page.php') {
    $extra = 'page-content';
}
else { 
    $extra = 'page-' . get_post( $post )->post_name;
}
?>
<body <?php body_class($extra); ?>>
    <div class="wrap">
    <?php
    do_action('get_header');
	get_template_part('templates/header');
    ?>

        <div id="main" role="main">
            <?php include roots_template_path(); ?>
        </div><!-- /main -->

        <?php 
        get_template_part('templates/footer');
        ?>
    </div><!-- eo wrap -->
    
    <noscript>
        <p class="msg-warning">
            <strong><?php _e('Je hebt javascript uitgeschakeld'); ?></strong>
            <a href="https://support.google.com/adsense/bin/answer.py?hl=en&amp;answer=12654" title="<?php _e('Lees hier hoe je javascript kunt inschakelen'); ?>"><?php _e('Lees hier hoe je javascript kunt inschakelen'); ?></a>
        </p>
    </noscript>
    
    <script src="<?php get_template_directory_uri(); ?>/static/js/plugins-min.js"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/inuit.js"></script>
    <?php wp_footer(); ?>
    <script>
        load();
    </script>
</body>
</html>