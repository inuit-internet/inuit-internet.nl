<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$creditInvoiceParams = array(
				// Example: search for all invoices on date 2014-05-20
				'searchat'			=> 'Date',
				'searchfor' 		=> '2014-05-20'
);

$response = $api->sendRequest('creditinvoice', 'list', $creditInvoiceParams);

print_r_pre($response);

?>