<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$creditorParams = array(
				'CreditorCode' 		=> 'CD0001',
				'Initials' 			=> 'Gunther',
				'SurName' 			=> 'Meusmann',
);

$response = $api->sendRequest('creditor', 'edit', $creditorParams);

print_r_pre($response);

?>