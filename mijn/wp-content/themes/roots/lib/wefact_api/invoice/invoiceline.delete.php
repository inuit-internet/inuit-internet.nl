<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$invoiceLineParams = array(
						'InvoiceCode'	=> 'F0001',
						
						'InvoiceLines'	=> array(
							array(
								'Identifier'	=> 3
							)
						)
);

$response = $api->sendRequest('invoiceline', 'delete', $invoiceLineParams);

print_r_pre($response);

?>