<?php

require_once("../wefact_api.php");

$api = new WeFactAPI();

$invoiceParams = array(
				'DebtorCode'	=> 'DB0001',	// Customer information will automatically be fetched

				'InvoiceLines'	=> array(
					array(
						'Number'		=> 3,
						'ProductCode'	=> 'P0001'
					),
					array(
						'Description'	=> 'Service contract',
						'PeriodicType'	=> 'period',
						'Periods' 		=> 3,
						'Periodic' 		=> 'm',
						'PriceExcl' 	=> 150,
					)
				)
);

$response = $api->sendRequest('invoice', 'add', $invoiceParams);

print_r_pre($response);

?>