<?php

// Custom metaboxes

/**
 * Initialize the meta boxes before anything else.
 */
add_action( 'admin_init', 'specific_register_meta_boxes' );

/**
 * Builds the Meta Boxes.
 */
 //get custom post types
function specific_get_id() {
  return isset( $_GET['post'] ) ? $_GET['post'] : ( isset( $_POST['post_ID'] ) ? $_POST['post_ID'] : false );
}
 
function specific_register_meta_boxes() { 
  $post_id = specific_get_id();

  if ( function_exists( 'ot_register_meta_box' ) && $post_id ) {
    $post_template = get_page_template_slug($post_id);
    
    if ($post_template == 'page-home.php') {
        homepage_template_meta_boxes();
    }
    if ($post_template == 'page.php') {
        page_template_meta_boxes();
    }
    if ($post_template == 'page-full.php') {
        full_width_template_meta_boxes();
    }
  }
  return false;
} 
 
function homepage_template_meta_boxes() {
  $meta_args_array = array(
        array(
		   'id'          => 'homepage_settings',
		   'title'       => 'Homepage instellingen',
		   'pages' => array('page'),
		   'context'     => 'normal',
		   'priority'    => 'high',
		   'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           )
           )
        ),
    ); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}
 
function page_template_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'page_settings',
		  'title'       => 'Pagina settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
           'label' => 'Pagina introductie',
           'id'  => 'intro',
           'type' => 'wysiwyg',
           'desc' => 'Introductie content voor deze pagina'
           )
		)
        ),
        array(
		  'id'          => 'page_sidebar_settings',
		  'title'       => 'Pagina sidebar instellingen',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array(
            'id'          => 'sidebar-items',
            'label'       => 'Sidebar items',
            'desc'        => '',
            'std'         => '',
            'type'        => 'list-item',
            'rows'        => '',
            'taxonomy'    => '',
            'class'       => '',
            'settings'    => array (
                array (
                    'id' => 'item-content',
                    'label' => 'Content',
                    'type' => 'wysiwyg',
                    'desc' => ''
                ),
                array (
                   'id' => 'page-links',
                   'label' => 'Pagina linkbox',
                   'type' => 'custom-post-type-checkbox',
                   'post_type' => 'any',
                   'desc' => ''
                ),
                array (
                    'id' => 'item-classes',
                    'label' => 'Classes',
                    'type' => 'text',
                    'desc' => ''
                )
            )
            )
            )
            )
        );
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}

function full_width_template_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'page_settings',
		  'title'       => 'Pagina settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
           'label' => 'Pagina introductie',
           'id'  => 'intro',
           'type' => 'wysiwyg',
           'desc' => 'Introductie content voor deze pagina'
           )
		)
        ),
        array(
		  'id'          => 'full_width_settings',
		  'title'       => 'Full width settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
           array(
           'label' => 'Full width content',
           'id'  => 'full-width-content',
           'type' => 'wysiwyg',
           'desc' => ''
           ),
          
		)
        ),
        array(
		  'id'          => 'page_sidebar_settings',
		  'title'       => 'Pagina sidebar instellingen',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array(
            'id'          => 'sidebar-items',
            'label'       => 'Sidebar items',
            'desc'        => '',
            'std'         => '',
            'type'        => 'list-item',
            'rows'        => '',
            'taxonomy'    => '',
            'class'       => '',
            'settings'    => array (
                array (
                    'id' => 'item-content',
                    'label' => 'Content',
                    'type' => 'wysiwyg',
                    'desc' => ''
                ),
                array (
                   'id' => 'page-links',
                   'label' => 'Pagina linkbox',
                   'type' => 'custom-post-type-checkbox',
                   'post_type' => 'any',
                   'desc' => ''
                ),
                array (
                    'id' => 'item-classes',
                    'label' => 'Classes',
                    'type' => 'text',
                    'desc' => ''
                )
        )
        )
        )
        ) 
	); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}