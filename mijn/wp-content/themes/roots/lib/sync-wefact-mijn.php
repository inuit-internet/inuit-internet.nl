<?php
setlocale(LC_ALL, 'nl_NL');
date_default_timezone_set('europe/amsterdam');
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . '/wp-load.php' );

$api = new WeFactAPI();
$debtorsresult = $api->sendRequest('debtor', 'list', array());
if($debtorsresult['status'] == 'success') {
    foreach($debtorsresult['debtors'] as $debtor) {
        $debtordetails = $api->sendRequest('debtor', 'show', array(
            'DebtorCode'	=> $debtor['DebtorCode'];
        ));
        if ($debtordetails['status'] == 'success') {
            var_dump($debtordetails['debtor']);
        }
    }
}
?>
