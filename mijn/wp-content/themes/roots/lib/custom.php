<?php
/**
* Custom functions
*/

/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter( 'ot_show_pages', '__return_false' );
/**
 * Optional: set 'ot_show_new_layout' filter to false.
 * This will hide the "New Layout" section on the Theme Options page.
 */
add_filter( 'ot_show_new_layout', '__return_false' );

/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );

/**
 * Required: include OptionTree.
 */
load_template( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );


remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
add_filter( 'wp_mail_content_type', function( $content_type ) {
	return 'text/html';
});

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

function get_ID_by_slug($slug) {
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE post_name='$slug' AND post_type='page' AND post_status='publish'";
    $id = $wpdb->get_var($query);
    return $id;
}
function get_attachment_id_from_src ($image_src) {
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    if (empty($id)) {
        $urlparts = explode('/',$image_src);
        $lastpart = end($urlparts);
        $query = "SELECT ID FROM {$wpdb->posts} WHERE guid LIKE '%$lastpart%'";
        $id = $wpdb->get_var($query);
    }
    return $id;
}
function return_slug($ID) {
    if (empty($ID)) {
        $ID = $post->ID;
    }
    $post_data = get_post($ID, ARRAY_A);
    $slug = $post_data['post_name'];
    return $slug; 
}
add_filter('show_admin_bar', '__return_false');
add_filter("gform_init_scripts_footer", "init_scripts_footer");
function init_scripts_footer() {
    return true;
}
add_action("gform_enqueue_scripts", "deregister_scripts");

function custom_gform_submit_button( $button, $form ) {
    $button = sprintf(
    ' <input type="submit" class="btn-1 gform_button" id="gform_submit_button_%d" value="%s">',
    absint( $form['id'] ),
        esc_attr(  $form['button']['text'] )
    );    
return $button;
}
add_filter( 'gform_submit_button', 'custom_gform_submit_button', 10, 2 ); 


function human_filesize($bytes, $decimals = 1) {
  $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}

function deregister_scripts(){
    wp_deregister_script("jquery"); 
}
add_filter('tiny_mce_before_init', 'override_mce_options'); 
function override_mce_options($initArray) {
        $opts = '*[*]';
        $initArray['valid_elements'] = $opts;
        $initArray['extended_valid_elements'] = $opts;
        return $initArray;
}

add_action( 'admin_init', 'update_post_comment_status' ); 
function update_post_comment_status() {
	global $wpdb;
	$wpdb->query("UPDATE $wpdb->posts SET comment_status = 'closed'");
}

function get_first_child($ID) {
    $type = get_post_type($ID);
    $children = get_posts('post_parent='.$ID.'&post_type='.$type.'&posts_per_page=-1&orderby=menu_order&order=ASC&offset=0');
    $first = $children[0];
    if (!empty($first)) {
        $firsturl = get_permalink($first->ID);
        return $firsturl;
    }
} 

function fetchData($url){
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_TIMEOUT, 20);
     $result = curl_exec($ch);
     curl_close($ch);
     return $result;
}

function get_id_by_template($template) {
    $pages = get_posts('post_type=page&posts_per_page=-1');
    foreach($pages as $page) {
        $ptemplate = get_page_template_slug( $page->ID );
        if ($ptemplate == $template) {
            $ID = $page->ID;
        }
    }
    return $ID;
}

add_action('init', 'create_nieuws_type');
function create_nieuws_type() {
	register_post_type( 'nieuws',
		array(
			'labels' => array(
				'name' => __( 'Nieuws' ),
				'singular_name' => __( 'nieuws' ),
				'new_item' => __( 'Voeg nieuws item toe' ),
				'add_new_item' => __( 'Voeg nieuws item toe' ),
				'add_new' => __( 'Voeg nieuws item toe' ),
				'all_items' => __( 'Alle nieuws items' )
			),
		'description' => 'Nieuws overzicht',
		'menu_position' => 7,
		'menu_icon' => 'dashicons-megaphone',
		'public' => true,
		'has_archive' => 'nieuws',
		'show_ui' => true,
		'exclude_from_search' => false,
		'supports' => array('title','editor','custom-fields','thumbnail'),
		'taxonomies' => array('category'),
        'query_var' => 'nieuws'
		)
	);
}

/**** LOGIN ****/
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/static/css/style-login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

//* Change the message/body of the email
add_filter( 'retrieve_password_message', 'new_retrieve_password_message', 10, 2 );
function new_retrieve_password_message( $message, $key ){
	// Bail if username or email is not entered
	if ( ! isset( $_POST['user_login'] ) )
		return;
	// Get user's data
	if ( strpos( $_POST['user_login'], '@' ) ) { # by email
		$user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
	} else { # by username
		$login = trim( $_POST['user_login'] );
		$user_data = get_user_by( 'login', $login );
	}
	// Store some info about the user
	$user_fname = $user_data->user_firstname;
    $user_lname = $user_data->user_lastname;
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;
    $blogname = get_bloginfo('name');
	// Assembled the URL for resetting the password
    if (!empty($user_fname)) {
        $aanhef = sprintf(__('%s %s'),$user_fname,$user_lname);
    } else {
        $aanhef = $user_login;
    }
    
	$reset_url = add_query_arg(array(
		'action' => 'rp',
		'key' => $key,
		'login' => rawurlencode( $user_login )
	), wp_login_url() /*$lostpwurl*/ );
	
    // Create and return the message
	$message = sprintf( '<p>%s</p>', __( 'Beste ' ) . $aanhef . ',');
	$message .= sprintf( '<p>%s</p>', __( "Iemand heeft een aanvraag gedaan om je wachtwoord voor $blogname opnieuw in te stellen.<br /><br />Als dit correct is, klik dan op onderstaande link. Anders kun je deze e-mail negeren." ) );
	$message .= sprintf( '<p><a href="%s">%s</a></p>', $reset_url, __( 'Wachtwoord opnieuw instellen' ) );
	return $message;
}

function resetpass_message($message) {
 $action = $_REQUEST['action'];
 if( $action == 'resetpass'|| $action == 'rp' ) {
    $message = '<p class="message">Voer hieronder je nieuwe wachtwoord in.<br>Het wachtwoord moet bestaan uit minimaal 12 tekens, waarvan minimaal 1 hoofdletter, 1 kleine letter en 1 cijfer.</p>';
    return $message;
 }
 else {
     return $message;
 }
}
//add_filter('login_message', 'resetpass_message');

add_action( 'validate_password_reset', 'force_strong_passwords', 10, 2 );
function force_strong_passwords($errors, $user_data) {
    $password = ( isset( $_POST[ 'pass1' ] ) && trim( $_POST[ 'pass1' ] ) ) ? sanitize_text_field( $_POST[ 'pass1' ] ) : false;
    $user_id = isset( $user_data->ID ) ? sanitize_text_field( $user_data->ID ) : false;
	$username = isset( $_POST["user_login"] ) ? sanitize_text_field( $_POST["user_login"] ) : $user_data->user_login ;
    
    // No password set?
	// Already got a password error?
	if ( ( false === $password ) || ( $errors->get_error_data("pass") ) ) {
		return $errors;
	}

    if (strlen($password) < 12) {
        $error[] = __('Het wachtwoord moet bestaan uit minimaal 12 tekens', 'profile');
    }
    elseif (!preg_match("#[0-9]+#",$password)) {
        $error[] = __('Het wachtwoord moet minimaal 1 cijfer bevatten', 'profile');
    }
    elseif (!preg_match("#[A-Z]+#",$password)) {
        $error[] = __('Het wachtwoord moet minimaal 1 hoofdletter bevatten', 'profile');
    } 
    elseif (!preg_match("#[a-z]+#",$password)) {
        $error[] = __('Het wachtwoord moet minimaal 1 kleine letter bevatten', 'profile');
    }
    
    if (!empty($error)) {
        $error = current($error);
        $errors->add( 'pass', '<strong>ERROR</strong>: '.$error.' ');
    } 
    
    return $errors;
}

function my_login_redirect( $redirect_to, $request, $user ) {
    $redirect_to = get_bloginfo('url') . '/wp-admin';
    
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'administrator', $user->roles ) ) {
			return $redirect_to;
		} else {
			return home_url();
		}
	} else {
		return home_url();
	}
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

function redirect_to_login() {
    $url = get_bloginfo('url');
    
    if ( ! is_user_logged_in() && strpos($_SERVER['REQUEST_URI'], '/login') === false && $_SERVER['REMOTE_ADDR'] !== '178.251.26.102' ) {
        $loginURL = $url . '/login';
        wp_safe_redirect( $loginURL );
        exit();
    } 

    if ( is_user_logged_in() && is_login_page() ) {
        wp_safe_redirect( $url );
        exit();
    }   
}
add_action( 'init', 'redirect_to_login', 10, 2 );

if(!function_exists('is_login_page')){
    function is_login_page() {
        return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
    }
}
/**** EO LOGIN ****/

function get_customers() {
	$klantenarr = array();
	$args = array(
		'offset' => 0,
		'number' => -1,
		'role__in' => array('klant')
	);
	$klanten = get_users($args);
	foreach($klanten as $klant) {
		$ID = $klant->data->ID;
		$displayname = $klant->data->user_login; 
		$displayname .= ' - ' . $klant->user_firstname . ' ' . $klant->user_lastname;
		$klantenarr[$ID] = $displayname;
	}
	if (!empty($klantenarr)) {
		return $klantenarr;
	}
}

function get_current_user_products() {
    $api = new WeFactAPI();
   
    $current_user = wp_get_current_user();
    $username = $current_user->data->user_login;
    
    $user_products_active = $api->sendRequest('subscription', 'list', array(
        'searchfor'	=> "$username",
        'searchat' => 'DebtorCode',
        'status' => 'active',
    ));
    $user_products_terminated = $api->sendRequest('subscription', 'list', array(
        'searchfor'	=> "$username",
        'searchat' => 'DebtorCode',
        'status' => 'terminated',
    ));
    
    if( !empty($user_products_active['subscriptions']) && !empty($user_products_terminated['subscriptions']) ) {
        $merged = array_merge($user_products_active['subscriptions'],$user_products_terminated['subscriptions']);
        return $merged;
    }
    elseif ( !empty($user_products_active['subscriptions']) && empty($user_products_terminated['subscriptions']) ) {
        $merged = $user_products_active['subscriptions'];
        return $merged;
    }
    else {
        $merged = $user_products_terminated['subscriptions'];
        return $merged;
    }
}

function get_current_user_invoices() {
    $api = new WeFactAPI();
    
    $current_user = wp_get_current_user();
    $username = $current_user->data->user_login;
    
    $user_invoices = $api->sendRequest('invoice', 'list', array(
        'searchat' => 'DebtorCode',
        'searchfor'	=> "$username",
		'order' => 'ASC'
    ));
    if ($user_invoices['status'] == 'success') {
		$invoices = $user_invoices['invoices'];
		if (!empty($invoices)) {
			return $invoices;
		}
    }
}

function get_invoice_amount_due($invoiceCode) {
	$api = new WeFactAPI();
	$invoicedetails = $api->sendRequest('invoice', 'show', array('InvoiceCode' => $invoiceCode));
	if ($invoicedetails['status']=='success') {
		$totaal = $invoicedetails['invoice']['AmountIncl'];
		$voldaan = $invoicedetails['invoice']['AmountPaid'];
		$amount = $totaal - $voldaan; 
		if (!empty($amount)) {
			return $amount;
		}
	}
}

function get_invoice_payment_url($invoiceCode) {
    $api = new WeFactAPI();
    $details = $api->sendRequest('invoice', 'show', array(
        'InvoiceCode'	=> $invoiceCode
    ));
    if ($details['status'] == 'success') {
        $paymenturl = $details['invoice']['PaymentURL'];
    }
    if (!empty($paymenturl)) {
        return $paymenturl;
    }
}

function get_product_details() {
    $api = new WeFactAPI();
    $productsArray = array();
    $wfproducts = $api->sendRequest('product', 'list', array(
    ));
    
    $wftproducts = $wfproducts['products'];
    foreach($wftproducts as $wfproduct) {
        $pcode = $wfproduct['ProductCode'];
        $pname = $wfproduct['ProductName'];
        $productsArray[$pcode] = $pname;
    }
    return $productsArray;
}

function get_current_user_details() {
    $api = new WeFactAPI();

    //var_dump($api);
    
    $current_user = wp_get_current_user();  
    $username = $current_user->data->user_login;
    
    $requestresults = $api->sendRequest('debtor', 'show', array(
        'DebtorCode' => "$username"
    ));

   //var_dump($requestresults);

    
    if ($requestresults['status'] == 'success') {
        
        $debtordetails = array();
        
        $sex = $requestresults['debtor']['Sex'];
        if ($sex == 'm') {
            $debtordetails['Sex'] = array('Aanhef' => 'Dhr.');
        }
        else {
             $debtordetails['Sex'] = array('Aanhef' => 'Mevr.');
        }
       
        $debtordetails['Initials'] = array( 'Initialen' => $requestresults['debtor']['Initials']); 
        $debtordetails['SurName'] = array( 'Achternaam' => $requestresults['debtor']['SurName'] ); 
        $debtordetails['Address'] = array( 'Adres' => $requestresults['debtor']['Address'] ); 
        $debtordetails['ZipCode'] = array( 'Postcode' => $requestresults['debtor']['ZipCode'] ); 
        $debtordetails['City'] = array( 'Plaats' => $requestresults['debtor']['City'] ); 
        $debtordetails['Country'] = array( 'Land' => $requestresults['debtor']['Country'] ); 
        $debtordetails['EmailAddress'] = array( 'E-mailadres' => $requestresults['debtor']['EmailAddress']); 
        $debtordetails['InvoiceEmailAddress'] = array( 'Facturatie e-mailadres' => $requestresults['debtor']['InvoiceEmailAddress']);   
        $debtordetails['PhoneNumber'] = array( 'Telefoonnummer' => $requestresults['debtor']['PhoneNumber']); 
        $debtordetails['MobileNumber'] = array( 'Mobielnummer' => $requestresults['debtor']['MobileNumber']); 
        
        return $debtordetails;
    }
}

function update_current_user_details($data) {
    $api = new WeFactAPI();
    
    $current_user = wp_get_current_user();
    $username = $current_user->data->user_login;
      
    $bedrijfsnaam = $data['CompanyName'];
    $aanhef = $data['Sex'];
    $initialen = $data['Initials'];
    $achternaam = $data['SurName'];
    $adres = $data['Address'];
    $postcode = $data['ZipCode'];
    $stad = $data['City'];
    $land = $data['Country'];
    $email = $data['EmailAddress'];
    $invoiceEmail = $data['InvoiceEmailAddress'];
    $tel = $data['PhoneNumber'];
    $mob = $data['MobileNumber'];
    
    $api->sendRequest('debtor', 'edit', array(
        'DebtorCode' => "$username", 
        'Sex'        => "$aanhef",
        'Initials'   => "$initialen",
        'SurName'    => "$achternaam",
        'ZipCode'    => "$postcode",
        'Address'    => "$adres",
        'City'       => "$stad",
        'Country'    => "$land",
        'EmailAddress' => "$email",
        'InvoiceEmailAddress' => "$invoiceEmail",
        'PhoneNumber' => "$tel",
        'MobileNumber' => "$mob"
    ));
}

function get_current_user_contracts() {
    $api = new WeFactAPI();
    
    $current_user = wp_get_current_user();
    $username = $current_user->data->user_login;
    
    $requestresults = $api->sendRequest('pricequote', 'list', array(
        'searchat'	=> 'DebtorCode',
        'searchfor' => "$username",
        'status' 	=> '3'
    ));
    
    if ($requestresults['status'] == 'success') {
        return $requestresults['pricequotes'];
    }
}

function get_current_user_domains(&$producten) {
    $domains = array();
    $domainproductcodes = array('P0001','P0002','P0003','P0004','P0005','P0007','P0015','P0024','P0029');
    if (!empty($producten)) {
        foreach($producten as $product) { //var_dump($product);
			$productcode = $product['ProductCode']; 
			$status = $product['Status']; 
			if (in_array($productcode,$domainproductcodes) && $status == '1') { //uitgaan van niet-opgezegde domein abonnementen in wefact
				$description = $product['Description'];
				$descArray = explode(" ",$description); //var_dump($descArray);
				$domain = end($descArray); 
				if (preg_match('/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/', $domain) !== false) {
					$domains[] = trim($domain);
				}
			}
        }
    }
    return $domains;
}

function check_domain($domain) { 
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL,$domain);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
    curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,0);
    $rawdata=curl_exec($ch); 
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $alldata = '<br />response: ' . $rawdata . '<br /> httpCode: ' . $httpCode;
    //print curl_error($ch);
    curl_close($ch); 
    return $httpCode;
}   

function return_domain_whois_status($domain) {
	require_once 'unirest-php-master/src/Unirest.php';
	$response = Unirest\Request::get("https://whois-v0.p.mashape.com/check?domain=$domain",
	  array(
		"X-Mashape-Key" => "5zEklX151LmshMiOIgFYlN00KDW5p1Z5K0qjsnwSHKroxgoPGj",
		"Accept" => "application/json"
	  )
	);
	return $response;
}

function collect_users_domains_stats(&$domeinen) {
    include_once('httpsocket.php');
    $sock = new HTTPSocket;
    $sock->connect('ssl://server1.inuit-internet.nl',2222);
    $sock->set_login('adminuser','fpuRM6FnWpfSQw63');
    $sock->query('/CMD_API_SHOW_ALL_USERS');
    $user_array_raw = explode("&", $sock->fetch_body());
    $waardes = array();
    
    foreach ($user_array_raw as $user) { 
        $user = str_replace('list[]=','',$user);
        
        $sock->query('/CMD_API_SHOW_USER_DOMAINS?user='.$user.'');
        $domains = explode("\n\r", $sock->fetch_body()); //var_dump($domains);
        
        foreach($domains as $key => $value) {
            $value = urldecode($value);
            if (strpos($value,'&') !== false) { //er zijn meerdere domeinen in deze string
                
                $domainsarray = explode("&",$value); 
                foreach($domainsarray as $domain) {
                   
                    $domain_values = explode("=",$domain);
                    $domain = trim($domain_values[0]);
                    
                    if (!empty($domain) && in_array($domain,$domeinen)) {
                        $values = $domain_values[1];
                        $valuesarr = explode(":",$values);
                        $waardes[$user][$domain]['bandbreedte'] = $valuesarr[0];
                        $waardes[$user][$domain]['schijfruimte'] = $valuesarr[2];
                    }
                }
            }    
            else {
                $domain_values = explode("=",$value);
                $domain = trim($domain_values[0]);
                $values = $domain_values[1];
                
                if (!empty($domain) && in_array($domain,$domeinen)) {
                    $valuesarr = explode(":",$values);
                    $waardes[$user][$domain]['bandbreedte'] = $valuesarr[0];
                    $waardes[$user][$domain]['schijfruimte'] = $valuesarr[2];
                }
            }   
        }
    }
    if (!empty($waardes)) {
        return $waardes; 
    }
}

function get_current_user_DA_users(&$domeinen) {
    include_once('httpsocket.php');
    $sock = new HTTPSocket;
    $sock->connect('ssl://server1.inuit-internet.nl',2222);
    $sock->set_login('adminuser','fpuRM6FnWpfSQw63');
    $sock->query('/CMD_API_SHOW_ALL_USERS'); 
    //var_dump($sock);
    $user_array_raw = explode("&", $sock->fetch_body()); //var_dump($user_array_raw);
    $daUsers = array();
	
    foreach ($user_array_raw as $user) { 
        $user = str_replace('list[]=','',$user);
        $sock->query('/CMD_API_SHOW_USER_DOMAINS?user='.$user.'');
        $domains = explode("\n\r", $sock->fetch_body()); 
		
        foreach($domains as $key => $value) {
            
			$value = urldecode($value);
			
			if (strpos($value,'&') !== false) { //er zijn meerdere domeinen in deze string
                $domainsarray = explode("&",$value); 
                foreach($domainsarray as $domain) { 
				    $domain_values = explode("=",$domain);
				    $domain = trim($domain_values[0]);
				    if (!empty($domain) && in_array($domain,$domeinen)) {
						$daUsers[] = $user;
					}
				}
			}
			else {
				$domain_values = explode("=",$value);
				$domain = trim($domain_values[0]);
				if (!empty($domain) && in_array($domain,$domeinen)) {
					$daUsers[] = $user;
				}
			}
		}
	}
	if (!empty($daUsers)) {
		return $daUsers;
	}
	return false;
}

function collect_DA_user_stats($daUser) { //gebruiken: CMD_API_SHOW_USER_USAGE + CMD_API_SHOW_USER_CONFIG
    if (!empty($daUser)) {
        $daUserStats = array(); 
        include_once('httpsocket.php');
        $sock = new HTTPSocket;
        $sock->connect('ssl://server1.inuit-internet.nl',2222);
        $sock->set_login('adminuser','fpuRM6FnWpfSQw63');
        $sock->query('/CMD_API_SHOW_USER_USAGE?user='.$daUser.'');
        //$usage = $sock->fetch_body();
        $usage = explode("&", $sock->fetch_body());
         
        //var_dump($usage);
	$daUserStats['accountname'] = $daUser;
        foreach($usage as $value) {
					
            $value = urldecode($value); 
            $valueArr = explode("=",$value);
            
            if ($valueArr[0] == 'bandwidth') {
                $daUserStats['bandwidthUsage'] = $valueArr[1];
            }
            if ($valueArr[0] == 'quota') {
                $daUserStats['diskUsage'] = $valueArr[1];
            }
            if ($valueArr[0] == 'nemails') {
                $daUserStats['popAccountsUsage'] = $valueArr[1];
            }
            if ($valueArr[0] == 'mysql') {
                $daUserStats['mysqlUsage'] = $valueArr[1];
            }
            
        }
        $sock->query('/CMD_API_SHOW_USER_CONFIG?user='.$daUser.'');
        $config = explode("&", $sock->fetch_body());
        foreach($config as $value) {
            $value = urldecode($value);
            $valueArr = explode("=",$value);
 
            if ($valueArr[0] == 'bandwidth') {
                $daUserStats['bandwidthMax'] = $valueArr[1];
            }
            if ($valueArr[0] == 'quota') {
                $daUserStats['diskMax'] = $valueArr[1];
            }
            if ($valueArr[0] == 'nemails') {
                $daUserStats['popAccountsMax'] = $valueArr[1];
            }
            if ($valueArr[0] == 'mysql') {
                $daUserStats['mysqlMax'] = $valueArr[1];
            }
            if ($valueArr[0] == 'suspended') {
                $daUserStats['suspended'] = $valueArr[1];
            }
            if ($valueArr[0] == 'ssl') {
                $daUserStats['ssl'] = $valueArr[1];
            }
            
        }
        if (!empty($daUserStats)) {
            return $daUserStats;
        }
    }
}

// Download factuur

$invoiceID = $_REQUEST['download_invoice'];
$pricequoteID = $_REQUEST['download_pricequote'];

function download_invoice($invoiceID) {
    if (!empty($invoiceID)) {

        $current_user = wp_get_current_user();
        $username = $current_user->data->user_login;
          
        $api = new WeFactAPI(); 
        
        $invoiceParams = array(
                    'InvoiceCode'	=> $invoiceID
        );
        
        $getinvoicedebtor = $api->sendRequest('invoice', 'show', $invoiceParams);
        if ( $getinvoicedebtor['status'] == 'success' ) {
            $invoicedebtor = $getinvoicedebtor['invoice']['DebtorCode'];
        }
        
        if ($invoicedebtor == $username) { 

            $response = $api->sendRequest('invoice', 'download', $invoiceParams);
            $filespath = WP_PLUGIN_DIR . '/wefact-api/files';        
            $filename = $response['invoice']['Filename'];
            $pdf_base64 = $response['invoice']['Base64'];
            
            $file = fopen("$filespath/$filename", "w+"); 
            
            $pdf_decoded = base64_decode($pdf_base64);
            fwrite($file, $pdf_decoded);
            
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');	}
            
            if(is_file("$filespath/$filename")) {
                header('Pragma: public'); 	// required
                header('Expires: 0');		// no cache
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: private',false);
                header('Content-Type:application/pdf');
                header('Content-Disposition: attachment; filename="'.basename($filename).'"');
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: '.filesize("$filespath/$filename"));	// provide file size
                header('Connection: close');
                readfile("$filespath/$filename");
                exit;
            }
        }
        else {
            wp_safe_redirect(get_bloginfo('url'));
        }
    }
}

function download_pricequote($pricequoteID) {
    if (!empty($pricequoteID)) {
        $api = new WeFactAPI();

        $current_user = wp_get_current_user();
        $username = $current_user->data->user_login;   
        
        $pricequoteParams = array(
                    'PriceQuoteCode'	=> $pricequoteID
        );
        
        $getpricequotedebtor = $api->sendRequest('pricequote', 'show', $pricequoteParams);
        if ( $getpricequotedebtor['status'] == 'success' ) {
            $pricequotedebtor = $getpricequotedebtor['pricequote']['DebtorCode'];
        }
        
        if ($pricequotedebtor == $username) {
        
            $filespath = WP_PLUGIN_DIR . '/wefact-api/files';   
            $response = $api->sendRequest('pricequote', 'download', $pricequoteParams);
            $filename = $response['pricequote']['Filename'];
            $pdf_base64 = $response['pricequote']['Base64'];
            
            $file = fopen("$filespath/$filename", "w+"); 
            
            $pdf_decoded = base64_decode($pdf_base64);
            fwrite($file, $pdf_decoded);
            
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');	}
            
            if(is_file("$filespath/$filename")) {
                header('Pragma: public'); 	// required
                header('Expires: 0');		// no cache
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: private',false);
                header('Content-Type:application/pdf');
                header('Content-Disposition: attachment; filename="'.basename($filename).'"');
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: '.filesize("$filespath/$filename"));	// provide file size
                header('Connection: close');
                readfile("$filespath/$filename");
                exit;
            }
        }
        else {
            wp_safe_redirect(get_bloginfo('url'));
        }
    }
}

if (isset($_REQUEST['download_invoice'])) {
    download_invoice($invoiceID);
}
if (isset($_REQUEST['download_pricequote'])) {
    download_pricequote($pricequoteID);
}

add_filter( 'gform_field_value_fullname', 'my_custom_population_function_name' );
add_filter( 'gform_field_value_email', 'my_custom_population_function_email' );

function my_custom_population_function_name( $value ) {
    $current_user = wp_get_current_user();
    $username = $current_user->data->user_login; 
    return "$username";
}
function my_custom_population_function_email( $value ) {
    $current_user = wp_get_current_user();
    $email = $current_user->user_email; 
    return "$email";
}

function get_gf_user_entries() {
    $current_user = wp_get_current_user(); 
    $search_criteria['field_filters'][] = array( 'key' => 'created_by', 'value' => $current_user->ID );
    $search_criteria['status'] = 'active';
    $sorting = array( 'key' => 'is_starred', 'direction' => 'ASC', 'is_numeric' => true );
    $entries = GFAPI::get_entries( 1, $search_criteria, $sorting );
    return $entries;
}

function check_role($role) {
    if (is_user_logged_in()) {
        $user_ID = get_current_user_id(); 
        $userdata = get_userdata( $user_ID );
        $roles = $userdata->roles;
        if (in_array($role, $roles)) {
            return true;
        }  
    }
    return false;
}

/** AUTO_TLS TEST **/
function wp_include_dir(){
    $wp_include_dir = preg_replace('/wp-content$/', 'wp-includes', WP_CONTENT_DIR);
    return $wp_include_dir;
}
function check_autoTLS() {
    $include = wp_include_dir();
    include_once $include . '/class-phpmailer.php';
       
    $phpmailerobject = new PHPMailer();
    $SMTPAutoTLS = $phpmailerobject->SMTPAutoTLS; 
    
    if ($SMTPAutoTLS == true) {
        $host = $_SERVER['HTTP_HOST'];
        $curl = curl_init('https://api.pushbullet.com/v2/pushes');
        $email = 'sebradius@gmail.com';
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Authorization: Bearer o.2SKcRnRsjaQNvd4lNYIBvBpdFyrdnrYU']);
        curl_setopt($curl, CURLOPT_POSTFIELDS, ["email" => $email, "type" => "link", "title" => "AUTOTLS > TRUE op $host", "body" => "", "url" => "$host"]);
        // UN-COMMENT TO BYPASS THE SSL VERIFICATION IF YOU DON'T HAVE THE CERT BUNDLE (NOT RECOMMENDED).
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        $headers = "From: website@$host" . "\r\n" .
        "Reply-To: website@$host" . "\r\n" .
        "X-Mailer: PHP/" . phpversion();
        mail("sebradius@gmail.com","AUTOTLS > TRUE op $host","AUTOTLS > TRUE op $host",$headers);
    }
} 
add_action('init','check_autoTLS'); 
if ( ! wp_next_scheduled( 'check_autotls' ) ) {
  wp_schedule_event( time(), 'hourly', 'check_autotls' );
}
add_action( 'check_autotls', 'check_autoTLS' );


function check_notify_user_diskspace_overusage() {
    if (isset($_GET['run_diskspace_check']) && (int)$_GET['run_diskspace_check'] == 1) {

    } 


}
add_action('init',)
?>