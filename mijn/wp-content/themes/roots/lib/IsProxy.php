<?php

/**
 * Realtime Register is proxy
 * 
 * <code>
 * require_once 'IsProxy.php';
 * 
 * $ip = new IsProxy("customer/user", "password");
 * $ip->check('domainname', 'com');
 * 
 * $result = $ip->result();
 * echo $result['domain'].' '.$result['result'];
 *
 * $ip->close();
 * </code>
 */

class IsProxy { 
    /**
     * Connection
     *
     * @var     object 
     */
    var $_fp; 
     
    /**
     * Construct
     *
     * @param   string  The username in the form "customer/user"
     * @param   string  The password
     * @param   string  The IsProxy host, defaults to "is.yoursrs.com"
     * @param   int     The IsProxy port, defaults to 2001
     * @return  void
     */
    function IsProxy($username, $password, $host = "is.yoursrs.com", $port = 2001) {
        $this->username = $username;
        $this->password = $password; 
        $this->host = $host;
        $this->port = $port; 
    }
    
    /**
     * Close
     * 
     * @return  void
     */
    function close() {
        $this->write('CLOSE');
        
        @fclose($this->_fp);
    }
    
    /**
     * Connect
     *
     * @return  bool    Connection established?
     */
    function connect() {
        $this->_fp = @fsockopen($this->host, $this->port, $errno, $errstr, 10);
        if(!is_resource($this->_fp)) {
            return false;
        }
        
        /**
         * Login
         */
        return $this->login();
    }
    
    /**
     * Check
     *
     * @param   string  Domainname
     * @param   mixed   TLD(s)
     * @return  void
     */
    function check($domainname, $tlds) {
        if(!is_array($tlds)) {
            $tlds = (array) $tlds;
        }
        
        foreach($tlds as $tld) {
            $this->write('IS '.$domainname.'.'.$tld);
        }
    }
    
    /**
     * Result
     * 
     * @return  array   [ domain, result ]
     */
    function result() {
        $response = $this->read();
        if(preg_match('#^([\-\w.]+)\s(available|not\savailable|invalid\sdomain|error)#', $response, $match)) {
            return array('domain' => $match[1], 'result' => $match[2]);
        }
        
        return array('domain' => '-', 'result' => 'error');
    }
    
    /**
     * Is connected?
     *
     * @return  bool    Connection?
     */
    function is_connected() {
        return is_resource($this->_fp);
    }
    
    /**
     * Login
     *
     * @return  bool    Login successfull?
     */
    function login() {
        if(!$this->write('LOGIN '.$this->username.' '.$this->password)) {
            return false;
        }
        
        $response = $this->read();
        if(preg_match('#^400\sLogin\sfailed#', $response)) {
            return false;
        }
        
        return preg_match('#^100\sLogin\sok#', $response);
    }
    
    /**
     * Read
     *
     * @return  string  Response
     */
    function read() {
        if(!$this->is_connected()) {
            $this->connect();
        }
        
        if(!$response = fgets($this->_fp, 1024)) {
            return false;
        }
        
        return trim($response);
    }
    
    /**
     * Write
     *
     * @param   string  Message
     * @return  bool    Writing successfull?
     */
    function write($message) {
        if(!$this->is_connected()) {
            $this->connect();
        }
        
        return @fputs($this->_fp, $message."\r\n");
    }
}

?>
