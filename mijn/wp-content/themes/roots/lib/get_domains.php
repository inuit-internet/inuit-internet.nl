<?php

class RestClient {
    var $conn_handle;

    private function do_request($method, $uri, $json=NULL) {
        if (!isset($conn_handle)) {
            $this->conn_handle = curl_init();
        }

        $options = array(
            CURLOPT_URL => "https://httpapi.yoursrs-ote.com/v1/" . $uri,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => "bas@inuit-internet.nl2/admin:realtimedemo",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json)
            )
        );

        curl_setopt_array($this->conn_handle, $options);

        return json_decode(curl_exec($this->conn_handle), 1);
    }

    public function get_domains() {
        return $this->do_request("GET", "domains/");
    }

    public function register_domain($domain_name, $registrant, $admin) {
        $body = array(
            'customer' => 'test',
            'period' => 12,
            'registrant' => $registrant,
            'contacts' => array(
               array('handle' => $admin, 'role' => 'ADMIN'),
               array('handle' => 'test', 'role' => 'BILLING'),
               array('handle' => 'test', 'role' => 'TECH'),
            )
        );
        return $this->do_request("POST", "domains/" . $domain_name, json_encode($body));
    }
}

$client = new RestClient;
var_dump($client);
//print_r($client->register_domain("myrefsdtestdomain23.com", "registrant", "registrant"));
var_dump($client->get_domains());
?>