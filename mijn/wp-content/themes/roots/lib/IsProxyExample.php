<?php
/**
 * Realtime Register is proxy example usage
 */

/**
 * Realtime Register is proxy username
 */
$username = 'bas@inuit-internet.nl2/admin';

/**
 * Realtime Register is proxy password
 */
$password = 'realtimedemo';

/**
 * TLDs
 */
$tlds = array('ae', 'am', 'at', 'be', 'biz', 'cc', 'ch', 'club.tw', 'co.at', 'co.gg', 'co.in', 'co.je', 'com', 'com.es', 'com.tw', 'co.uk', 'de', 'dk', 'ebiz.tw', 'es', 'eu', 'firm.in', 'fm', 'fr', 'fr', 'game.tw', 'gen.in', 'gg', 'idv.tw', 'in', 'ind.in', 'info', 'it', 'je', 'li', 'ltd.uk', 'me', 'me.uk', 'mobi', 'name', 'net', 'net.gg', 'net.in', 'net.je', 'net.uk', 'nl', 'nom.es', 'nu', 'or.at', 'org', 'org.es', 'org.gg', 'org.in', 'org.tw', 'org.uk', 'plc.uk', 'pm', 're', 'ru', 'tel', 'tf', 'to', 'tv', 'tw', 'wf', 'xxx', 'yt');

/**
 * Text
 */
$text = array(
    'error'             => 'Error',
    'available'         => 'Available',
    'not available'     => 'Not available',
    'invalid domain'    => 'Invalid domain',
);

$domainname = isset($_REQUEST['domainname']) ? $_REQUEST['domainname'] : null;
$tld = isset($_REQUEST['tld']) ? $_REQUEST['tld'] : null;

echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">';
echo '  <table>';
echo '    <tr>';
echo '      <td><input type="text" name="domainname" value="'.$domainname.'" /></td>';
echo '      <td>.</td>';
echo '      <td><select name="tld"><option value="all" '.($tld == 'all' ? 'selected="selected"' : null).'>(all)</option>';

foreach($tlds as $opt) {
    echo '        <option value="'.$opt.'"'.($tld == $opt ? 'selected="selected"' : null).'>'.$opt.'</option>';
}

echo '      </select></td>';
echo '      <td><input type="submit" value="Check" /></td>';
echo '    </tr>';
echo '  </table>';
echo '</form>';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(is_null($domainname) || is_null($tld)) {
        echo '<p>Ongeldige domeinnaam</p>';
    }
    else {
        /**
         * Initiate Realtime Register whois proxy
         */
        require_once 'IsProxy.php';
        
        $ip = new IsProxy($username, $password);
        
        $tld_check = $tld == 'all' ? $tlds : (array) $tld;
        
        $tld_count = sizeof($tld_check);
        $a = 0;
        
        echo '<table>';
        
        $ip->check($domainname, $tld_check);
        while($result = $ip->result()) {
            $a++;
            
            echo '<tr><td>'.$result['domain'].'</td><td>'.$text[$result['result']].'</td></tr>';
            flush();
            
            if($a == $tld_count) {
                $ip->close();
                
                break;
            }
        }
        
        echo '</table>';
    }
}

?>
