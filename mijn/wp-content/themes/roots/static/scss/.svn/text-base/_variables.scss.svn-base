//
//  Variables.scss
//

// Settings
// --------------------------------------------------
$static-path:           "../../static";                 // URL to the root of the static folder

// Global values
// --------------------------------------------------

// Custom colors
// -------------------------
$c1:                    #4d4d4d !default;               // grey for text
$c2:                    #f96236 !default;               // orange
$c3:                    #D1D1D1 !default;               // grey text

// Accent colors
// -------------------------
$black:                 #000 !default;
$grayDarker:            #222 !default;
$grayDark:              #333 !default;
$gray:                  #555 !default;
$grayLight:             #999 !default;
$grayLighter:           #eee !default;
$white:                 #fff !default;

// Scaffolding
// -------------------------
$bodyBackground:        $white !default;
$textColor:             $c1 !default;

// Links
// -------------------------
$linkColor:             $c1 !default;
$linkColorHover:        darken($linkColor, 15%) !default;

// Typography
// -------------------------
$baseFontFamily:        Arial, Helvetica, sans-serif;
$baseFontSize:          14px !default;
$baseLineHeight:        20px !default;

$headingsFontFamily:    inherit !default; // empty to use BS default, $baseFontFamily
$headingsFontWeight:    bold !default;    // instead of browser default, bold
$headingsColor:         inherit !default; // empty to use BS default, $textColor

// Component sizing
// -------------------------
$fontSizeParagraph:     $baseFontSize;

// Headings
$headingsFontFamily:    $baseFontFamily;
$headingsFontWeight:    normal;
$headingsColor:         $c1;

$fontSizeh1:            2.5em;
$fontSizeh2:            1.8em;
$fontSizeh3:            1em;
$fontSizeh4:            1em;