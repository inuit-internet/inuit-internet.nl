/*
    Compat functions for the String object

*/

if (!String.prototype.trimLeft) {
    String.prototype.trimLeft = function() {
        return this.replace(/^[ \t\r\n]*/g, '');
    };
}

if (!String.prototype.trimRight) {
    String.prototype.trimRight = function() {
        return this.replace(/[ \t\r\n]*$/g, '');
    };
}

if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.trimLeft(this.trimRight());
    };
}
