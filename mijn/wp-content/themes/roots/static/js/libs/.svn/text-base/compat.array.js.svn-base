/*
    Compat functions for the Array object


*/

var global = this;
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(searchElement, fromIndex) {
        if (fromIndex < 0) {
            return -1;
        }
        if (fromIndex === undefined) {
            fromIndex = 0;
        }
        for (var i=fromIndex; i < this.length; i++) {
            if (this[i] === searchElement) {
                return i;
            }
        }
        return -1;
    };
}

if (!Array.prototype.lastIndexOf) {
    Array.prototype.lastIndexOf = function(searchElement, fromIndex) {
        if (fromIndex === undefined) {
            fromIndex = this.length - 1;
        } else {
            while (fromIndex < 0) {
                fromIndex += this.length;
            }
        }
        for (var i=fromIndex; i >= 0; i--) {
            if (this[i] === searchElement) {
                return i;
            }
        }
        return -1;
    };
}

if (!Array.prototype.filter) {
    Array.prototype.filter = function(callback, thisObject) {
        var ret = [];
        if (!thisObject) {
            thisObject = global;
        }
        for (var i=0; i < this.length; i++) {
            if (callback.call(thisObject, this[i])) {
                ret.push(this[i]);
            }
        }
        return ret;
    };
}

if (!Array.prototype.forEach) {
    Array.prototype.forEach = function(callback, thisObject) {
        if (!thisObject) {
            thisObject = global;
        }
        for (var i=0; i < this.length; i++) {
            callback.call(thisObject, this[i]);
        }
    };
}

if (!Array.prototype.every) {
    Array.prototype.every = function(callback, thisObject) {
        if (!thisObject) {
            thisObject = global;
        }
        for (var i=0; i < this.length; i++) {
            if (!callback.call(thisObject, this[i])) {
                return false;
            }
        }
        return true;
    };
}

if (!Array.prototype.map) {
    Array.prototype.map = function(callback, thisObject) {
        if (!thisObject) {
            thisObject = global;
        }
        var ret = [];
        for (var i=0; i < this.length; i++) {
            ret.push(callback.call(thisObject, this[i]));
        }
        return ret;
    };
}

if (!Array.prototype.some) {
    Array.prototype.some = function(callback, thisObject) {
        if (!thisObject) {
            thisObject = global;
        }
        for (var i=0; i < this.length; i++) {
            if (callback.call(thisObject, this[i])) {
                return true;
            }
        }
        return false;
    };
}

if (!Array.prototype.reduce) {
    Array.prototype.reduce = function(callback, initialValue) {
        var i, ret;
        if (initialValue !== undefined) {
            i = 0;
            ret = initialValue;
        } else {
            i = 1;
            ret = this[0];
        }
        for (; i < this.length; i++) {
            if (this[i] === null) {
                continue;
            }
            ret = callback(ret, this[i], i, this);
        }
        return ret;
    };
}

if (!Array.prototype.reduceRight) {
    Array.prototype.reduceRight = function(callback, initialValue) {
        var i, ret;
        if (initialValue !== undefined) {
            ret = initialValue;
            i = this.length - 1;
        } else {
            ret = this[this.length - 1];
            i = this.length - 2;
        }
        for (; i >= 0; i--) {
            if (this[i] === null) {
                continue;
            }
            ret = callback(ret, this[i], i, this);
        }
        return ret;
    };
}


/**
 * Duck punch jQuery's $.unique method, (that can only filter an array of DOMElements)
 * so it can filter every type of item in an array
 */
(function($) {

    var _oldUnique = $.unique;

    $.unique = function(arr) {
        // do the default behavior only if we got an array of elements
        if (!!arr[0].nodeType) {
            return _oldUnique.apply(this, arguments);
        } else {
            // reduce the array to contain no dupes via grep/inArray
            return $.grep(arr, function(v, k) {
                return $.inArray(v, arr) === k;
            });
        }
    };
})(jQuery);