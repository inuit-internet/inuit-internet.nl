window.INUIT = new function(e) {
    "use scrict";
    var t = this,
        i = t.IN = function() { /*this.initGoTop()*/
            this.initBoxTrigger(), this.initNavigation(), this.initEqualheights(), this.initCarousel(), this.initScrollHeader(), this.initGoogleMaps(), this.initPlaceholders(), this.initRefslider(), this.initLazyLoader()
        };
    i.prototype.initPlaceholders = function() {
        var t = "placeholder" in document.createElement("input");
        t || (e("[placeholder]").focus(function() {
            var t = e(this);
            t.val() == t.attr("placeholder") && (t.val(""), t.removeClass("placeholder"))
        }).blur(function() {
            var t = e(this);
            ("" == t.val() || t.val() == t.attr("placeholder")) && (t.addClass("placeholder"), t.val(t.attr("placeholder")))
        }).blur(), e("[placeholder]").parents("form").submit(function() {
            e(this).find("[placeholder]").each(function() {
                var t = e(this);
                t.val() == t.attr("placeholder") && t.val("")
            })
        })), e(".gfield").each(function() {
            var t = e(this).find("label").text().toLowerCase().replace("*", "");
            t = "Uw " + t, e(this).find("input[type=text],input[type=email],input[type=password],textarea,input[type=tel]").attr("placeholder", t)
        })
    }, equalheight = function(t) {
        var i, o = 0,
            n = 0,
            a = new Array;
        e(t).each(function() {
            if (i = e(this), e(i).height("auto"), topPostion = i.position().top, n != topPostion) {
                for (currentDiv = 0; currentDiv < a.length; currentDiv++) a[currentDiv].height(o);
                a.length = 0, n = topPostion, o = i.height(), a.push(i)
            } else a.push(i), o = o < i.height() ? i.height() : o;
            for (currentDiv = 0; currentDiv < a.length; currentDiv++) a[currentDiv].height(o)
        })
    }, i.prototype.initEqualheights = function() {
        equalheight(".homepage-intro-list li p"), e(window).resize(function() {
            equalheight(".homepage-intro-list li p")
        })
    }, i.prototype.initLazyLoader = function() {
        var t = e(window).height(),
            i = t / 2,
            o = e(window).scrollTop(),
            n = e(".box-items.portfolio");
        if (n.length) {
            var a = e(".loader"),
                l = e(".loading-bar"),
                r = a.attr("data-grid-type"),
                s = a.attr("data-grid-url");
            s && e(document).bind("scroll", function() {
                if (_cur_top = e(window).scrollTop(), window.scrollY > i && o < _cur_top && !e(".isloading").length && !e(".stoploading").length) {
                    e(".loader").addClass("loading"), l.addClass("isloading");
                    var t = l.find(".loader").attr("data-grid-start");
                    e.ajaxSetup({
                        cache: !1
                    }), e.ajax({
                        data: {
                            start: t,
                            type: r
                        },
                        url: s,
                        success: function(t) {
                            n.append(t);
                            var i = n.find(".newloader").html();
                            n.find(".newloader").remove(), l.html(i), e(".loader").removeClass("loading"), l.removeClass("isloading"), e(".box-items.portfolio .row:last").fadeIn("slow").removeClass("hide")
                        }
                    })
                }
                o = _cur_top
            })
        }
    }, i.prototype.initNavigation = function() {
        var t = (e(".nav-main"), e(".nav-link")),
            i = e(".wrap");
        t.click(function(o) {
            return o.preventDefault(), e("body").scrollTo("0", 0), t.toggleClass("active"), i.toggleClass("active"), e("html").toggleClass("active"), !1
        }), e(".nav-main > .menu > li").each(function() {
            var t = e(this).find(".submenu"),
                i = e(t).width(),
                o = e(t).height();
            e(this).hover(function() {
                e(t).length > 0 && !e(".nav-link").is(":visible") && (e(".link-border").css("visibility", "hidden"), t.css({
                    width: "0px",
                    height: "0px",
                    left: i / 2 + "px"
                }).show(0).animate({
                    width: i + "px",
                    height: o + "px",
                    left: -i / 4
                }, {
                    duration: 175,
                    complete: function() {
                        t.css({
                            background: "#efefef"
                        }).delay(250).find(">ul").show(0)
                    }
                }))
            }, function() {
                e(t).length > 0 && !e(".nav-link").is(":visible") && (t.hide(0).css({
                    background: "transparent"
                }).find(">ul").hide(0), e(".link-border").css("visibility", "visible"))
            })
        })
    }, i.prototype.initGoTop = function() {
        e("body").append('<a href="#header" title="Ga terug naar boven">T</a>');
        var t = e('a[title*="terug naar boven"]'),
            i = 0;
        e(document).bind("scroll", function() {
            window.scrollY > 20 && 0 == i && (i = 1, t.animate({
                opacity: 1
            })), window.scrollY < 20 && 1 == i && (i = 0, t.animate({
                opacity: 0
            }))
        }), t.click(function(t) {
            t.preventDefault(), e("body").scrollTo("0", 600)
        })
    }, i.prototype.initScrollHeader = function() {
        var t = 0,
            i = e(".header");
        e(document).bind("scroll", function() {
            window.scrollY > 0 && 0 == t && (t = 1, e(i).addClass("scrolling").find(".link-border").css("visibility", "hidden")), 0 == window.scrollY && 1 == t && (t = 0, e(i).removeClass("scrolling").find(".link-border").css("visibility", "visible"))
        })
    }, i.prototype.initBoxTrigger = function() {
        e(".homepage-intro-list li").each(function() {
            var t = e(this).find(".btn-1"),
                i = e(this),
                o = "0",
                n = t.outerHeight(!0);
            t.css("bottom", -n + "px"), e(i).find("a").length > 0 && e(this).hover(function() {
                e(t).animate({
                    bottom: o
                }, 1, "easeInExpo", function() {})
            }, function() {
                e(t).animate({
                    bottom: -n + "px"
                }, 50, "easeOutExpo", function() {})
            })
        })
    }, i.prototype.initCarousel = function() {
        e(".carousel").flexslider({
            namespace: "carousel-",
            animation: "fade",
            selector: ".carousel-images > li",
            slideshowSpeed: 7e3,
            animationSpeed: 600,
            useCSS: !0,
            pauseOnHover: !0,
            touch: !0,
            controlNav: !1,
            directionNav: !0
        }), e("a").each(function() {
            var t = new RegExp("/" + window.location.host + "/");
            t.test(this.href) || e(this).click(function(e) {
                e.preventDefault(), e.stopPropagation(), window.open(this.href, "_blank")
            })
        }), e("#content a[href^='http://']").attr("target", "_blank")
    }, i.prototype.initRefslider = function() {
        e(".referenties").flexslider({
            namespace: "referentie-",
            animation: "fade",
            selector: ".referentie-items > li",
            slideshowSpeed: 7e3,
            animationSpeed: 600,
            useCSS: !0,
            pauseOnHover: !0,
            touch: !1,
            controlNav: !1,
            directionNav: !1
        })
    }, i.prototype.initAccordion = function() {
        var t = e(".accordion .accordion-content").css({
            height: 0
        });
        e(".accordion > .accordion-heading > a").click(function() {
            if ($this = e(this), $target = $this.parent().next(), $target.hasClass("active")) e(this).removeClass("active"), t.removeClass("active").transition({
                height: 0
            }, 450, "snap");
            else {
                e(".accordion-heading a").removeClass("active");
                var i = e(".content", $target).height();
                t.removeClass("active").animate({
                    height: 0
                }, 400), $target.addClass("active").transition({
                    height: i
                }, 450, "snap"), e(this).addClass("active")
            }
            return !1
        })
    }, i.prototype.initTooltips = function() {
        e(".list-logos, .list-clients").tooltip({
            selector: "a",
            placement: "top"
        }), e(".carousel .carousel-images li").tooltip({
            selector: "span",
            placement: "right"
        })
    }, i.prototype.initGoogleMaps = function() {
        function t() {
            var e = new google.maps.LatLng(51.647491, 5.630102),
                t = new google.maps.LatLng(51.65869, 5.626878),
                i = {
                    zoom: 12,
                    center: t,
                    disableDefaultUI: !0,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                },
                o = new google.maps.Map(document.getElementById("map"), i),
                n = ['<form action="http://maps.google.com/maps" method="get" target="_blank">', '<input type="hidden" class="hidden" name="daddr" value="Muntmeester 477, Uden" /> ', '<input type="submit" class="btn-1 routebtn" value="Plan mijn route" />', "</form>"].join(""),
                a = new google.maps.InfoWindow({
                    content: n
                }),
                l = new google.maps.Marker({
                    position: e,
                    map: o,
                    icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
                    title: "Inuit Internet Diensten"
                });
            google.maps.event.addListener(l, "click", function() {
                a.open(o, l)
            });
            var r = [{
                stylers: [{
                    hue: "#36A6DA"
                }]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [{
                    lightness: 100
                }, {
                    visibility: "simplified"
                }]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [{
                    visibility: "on"
                }]
            }];
            o.setOptions({
                styles: r
            })
        }
        e("body").hasClass("contact") && (window.onload = t)
    }
}(jQuery), $(document).ready(function() {
    new INUIT.IN
});