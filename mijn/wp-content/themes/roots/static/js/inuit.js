window.INUIT = new function(a) {
    "use scrict";
    var b = this,
    c = b.IN = function() {
        this.initBoxTrigger(), 
        this.initNavigation(), 
        this.initScrollHeader(), 
        this.initTabs(),
        this.initTableSorter(),
        this.initPlaceholders(),
        this.initSelectCustomer(),
        this.initDarkmode()
    };
    
    c.prototype.initPlaceholders = function() {
        var b = "placeholder" in document.createElement("input");
        b || (a("[placeholder]").focus(function() {
            var b = a(this);
            b.val() == b.attr("placeholder") && (b.val(""), b.removeClass("placeholder"))
        }).blur(function() {
            var b = a(this);
            ("" == b.val() || b.val() == b.attr("placeholder")) && (b.addClass("placeholder"), b.val(b.attr("placeholder")))
        }).blur(), a("[placeholder]").parents("form").submit(function() {
            a(this).find("[placeholder]").each(function() {
                var b = a(this);
                b.val() == b.attr("placeholder") && b.val("")
            })
        })), a(".gfield").each(function() {
            var b = a(this).find("label").text().toLowerCase().replace("*", "");
            b = "Uw " + b, a(this).find("input[type=text],input[type=email],input[type=password],textarea,input[type=tel]").attr("placeholder", b)
        })
    };
  
    c.prototype.initNavigation = function() {
        var b = (a(".nav-main"), a(".nav-link")),
            c = a(".wrap");
        b.click(function(d) {
            return d.preventDefault(), a("body").scrollTo("0", 0), b.toggleClass("active"), c.toggleClass("active"), a("html").toggleClass("active"), !1
        }), a(".nav-main > .menu > li").each(function() {
            var b = a(this).find(".submenu"),
                c = a(b).width(),
                d = a(b).height();
            a(this).hover(function() {
                a(b).length > 0 && !a(".nav-link").is(":visible") && (a(".link-border").css("visibility", "hidden"), b.css({
                    width: "0px",
                    height: "0px",
                    left: c / 2 + "px"
                }).show(0).animate({
                    width: c + "px",
                    height: d + "px",
                    left: -c / 4
                }, {
                    duration: 175,
                    complete: function() {
                        b.css({
                            background: "#edf8fc"
                        }).delay(250).find(">ul").show(0)
                    }
                }))
            }, function() {
                a(b).length > 0 && !a(".nav-link").is(":visible") && (b.hide(0).css({
                    background: "transparent"
                }).find(">ul").hide(0), a(".link-border").css("visibility", "visible"))
            })
        })
    };
    
    c.prototype.initGoTop = function() {
        a("body").append('<a href="#header" title="Ga terug naar boven">T</a>');
        var b = a('a[title*="terug naar boven"]'),
            c = 0;
        a(document).bind("scroll", function() {
            window.scrollY > 20 && 0 == c && (c = 1, b.animate({
                opacity: 1
            })), window.scrollY < 20 && 1 == c && (c = 0, b.animate({
                opacity: 0
            }))
        }), b.click(function(b) {
            b.preventDefault(), a("body").scrollTo("0", 600)
        })
    };
    
    c.prototype.initScrollHeader = function() {
        var b = 0,
            c = a(".header");
        a(document).bind("scroll", function() {
            window.scrollY > 0 && 0 == b && (b = 1, a(c).addClass("scrolling").find(".link-border").css("visibility", "hidden")), 0 == window.scrollY && 1 == b && (b = 0, a(c).removeClass("scrolling").find(".link-border").css("visibility", "visible"))
        })
    };
    
    c.prototype.initBoxTrigger = function() {
        a(".homepage-intro-list li").each(function() {
            var b = a(this).find(".btn-1"),
                c = a(this),
                d = "0",
                e = b.outerHeight(!0);
            b.css("bottom", -e + "px"), a(c).find("a").length > 0 && a(this).hover(function() {
                a(b).animate({
                    bottom: d
                }, 1, "easeInExpo", function() {})
            }, function() {
                a(b).animate({
                    bottom: -e + "px"
                }, 50, "easeOutExpo", function() {})
            })
        })
    };

    c.prototype.initAccordion = function() {
        var b = a(".accordion .accordion-content").css({
            height: 0
        });
        a(".accordion > .accordion-heading > a").click(function() {
            if ($this = a(this), $target = $this.parent().next(), $target.hasClass("active")) a(this).removeClass("active"), b.removeClass("active").transition({
                height: 0
            }, 450, "snap");
            else {
                a(".accordion-heading a").removeClass("active");
                var c = a(".content", $target).height();
                b.removeClass("active").animate({
                    height: 0
                }, 400), $target.addClass("active").transition({
                    height: c
                }, 450, "snap"), a(this).addClass("active")
            }
            return !1
        })
    };
    
    c.prototype.initTooltips = function() {
        a(".list-logos, .list-clients").tooltip({
            selector: "a",
            placement: "top"
        }), a(".carousel .carousel-images li").tooltip({
            selector: "span",
            placement: "right"
        })
    };
    
    c.prototype.initTabs = function() {
        if (a("#tabs").length > 0) {
            a("#tabs").tabs();
        }
    };
    
    c.prototype.initTableSorter = function() {
        if( (a("body.mijn-facturen").length > 0) && typeof tablesorter === "function"  || (a("body.mijn-offertes").length > 0) && typeof tablesorter === "function" ) {
			a("table").tablesorter( {sortList: [[0,1]]} );
		}
		else if (typeof tablesorter === "function") {
			a("table").tablesorter( {sortList: [[0,0], [1,0]]} );	
		}
	};
	
	c.prototype.initSelectCustomer = function() {
		a("#customer-select").change(function(e) {
			a(this).closest('form').submit();
			e.preventDefault; 
		});
	};

	c.prototype.initDarkmode = function() {
            
            var storage = window.sessionStorage;
            //Toggle Mode en stel LocalStorage Key in
            a( ".inner-switch > .fas" ).on("click", function() { 
               if( a( "html,body" ).hasClass( "dark" )) {
                 a( "html,body" ).removeClass( "dark" );
                 a( ".inner-switch .fas" ).addClass("fa-sun").removeClass( "fa-moon" );
                 storage.setItem("mode","light");
               } else {
                 a( "html,body" ).addClass( "dark" );
                 a( ".inner-switch .fas" ).addClass("fa-moon").removeClass( "fa-sun" );
                 storage.setItem("mode","dark");
               }
           });
        
    };
}
(jQuery), $(document).ready(function() {
    new INUIT.IN;
});