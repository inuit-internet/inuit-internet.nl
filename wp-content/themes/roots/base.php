<?php
date_default_timezone_set("Europe/Amsterdam");
setlocale(LC_ALL, 'nl_NL');
get_template_part('templates/head'); 
$template = get_page_template_slug($post->ID);
if ($template == 'page-home.php') {
    $extra = 'page-homepage';
}
elseif ($template == 'page.php') {
    $extra = 'page-content';
}
else { 
    $extra = 'page-' . get_post( $post )->post_name;
}
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TSK8HVV');</script>
<!-- End Google Tag Manager -->

<body <?php body_class($extra); ?>>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSK8HVV"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
     <!-- End Google Tag Manager (noscript) -->

    <div class="wrap">
    <?php
    do_action('get_header');
	if (is_front_page()) {
		get_template_part('templates/header');
	}
	else {
		get_template_part('templates/header-regular');
	}
    ?>

        <div id="main" role="main">
            
            <?php include roots_template_path(); ?>
            
        </div><!-- /main -->

        <?php get_template_part('templates/footer');  ?>

    </div><!-- eo wrap -->
    
    <noscript>
        <p class="msg-warning">
            <strong><?php _e('Je hebt javascript uitgeschakeld'); ?></strong>
            <a href="https://support.google.com/adsense/bin/answer.py?hl=en&amp;answer=12654" title="<?php _e('Lees hier hoe je javascript kunt inschakelen'); ?>"><?php _e('Lees hier hoe je javascript kunt inschakelen'); ?></a>
        </p>
    </noscript>

    <script src="<?php get_template_directory_uri(); ?>/static/js/libs/jquery.easing.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/libs/components.carousel.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/libs/components.shadowbox.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/plugins-min.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/inuit.js" defer="defer"></script>
	
    <?php wp_footer(); ?>
    

</body>
</html>