<div class="container">
    <section class="section supercompact">
        <?php
        $args = array(
        'type'                     => 'post',
        'child_of'                 => 0,
        'parent'                   => 8,
        'orderby'                  => 'name',
        'order'                    => 'ASC',
        'hide_empty'               => 1,
        'hierarchical'             => 1,
        'taxonomy'                 => 'category',
        'pad_counts'               => false 
        ); 
        $portcats = get_categories($args); 
        if (!empty($portcats)) :
        ?>
            <div class="portfolio-subnav">
                <ul>
                    <?php
                    if ( (isset($_SESSION['selected_categories']) && !empty($_SESSION['selected_categories'])) || (empty($_SESSION['selected_categories'])) || (!isset($_SESSION['selected_categories'])) ) :
                    if (empty($_SESSION['selected_categories'])) {$class = 'selected';}
                    ?>
                    <li <?php if (!empty($class)) : ?>class="<?php echo $class; ?>"<?php endif; ?>><a href="<?php printf(__('%s?category_name=%s'),get_permalink($post->ID),'all'); ?>"><i class="fa fa-cog"></i> <?php _e('Alle'); ?></a></li>
                    <?php
                    endif;
                    foreach ($portcats as $cat) :
                    $catSlug = $cat->slug;
                    if (in_array($catSlug,$_SESSION['selected_categories'])) {
                        $class = 'selected';
                    }
                    else {
                        $class = '';
                    }
                    ?>
                    <li <?php if (!empty($class)) : ?>class="<?php echo $class; ?>"<?php endif; ?>><a href="<?php printf(__('%s?category_name=%s'),get_permalink($post->ID),$cat->slug); ?>"><i class="fa fa-cog"></i> <?php echo $cat->cat_name; ?></a></li>
                    <?php
                    endforeach;
                    ?>
                </ul>
            </div>
        <?php
        endif;
        ?>
    </section>
    <section class="section box-items portfolio" role="region">
            <?php
            $nr = 0;  
            $totalnr = 0;
            if (!empty($_SESSION['selected_categories'])) {
                $catarray = array(); 
                foreach ($_SESSION['selected_categories'] as $key => $value) {
                    $catarray[] = $key;
                }
                $args = array(
                    'post_type' => 'project',
                    'posts_per_page' => 9,
                    'orderby' => 'menu_order', 
                    'post_status' => 'publish',
                    'sort_order' => 'desc',
                    'category__in' => $catarray
                );
            }
            else {
                $args = array(
                    'post_type' => 'project',
                    'posts_per_page' => 9,
                    'orderby' => 'menu_order', 
                    'post_status' => 'publish',
                    'sort_order' => 'desc',
                );
            }
            $projectenQuery = new WP_Query( $args );
            while ($projectenQuery->have_posts()) :  $nr++; $totalnr++;
            $projectenQuery->the_post(); 
            if ($nr == 1) : 
            ?>
			<div class="row">
            <?php 
            endif; 
            ?>
                <div class="column portfolio-item">
                    <article class="article">
                        <?php
                        if (has_post_thumbnail()) : 
                        $projectlink = get_post_meta($post->ID,'project_link_url_external',true);
                        $projecthasdetails = get_post_meta($post->ID,'project_has_details',true);
                        $target = '_blank';
                        
                        if ($projecthasdetails[0] == 'ja') {
                            $projectlink = get_permalink($post->ID);
                            $target = '_self'; 
                        }
                     
                        if (!empty($projectlink)) :
                        $projectlinktitle = get_post_meta($post->ID,'project_link_text',true);
                        ?>
                        <a rel="nofollow" href="<?php echo $projectlink; ?>" target="<?php echo $target; ?>" title="<?php if (!empty($projectlinktitle)) { echo $projectlinktitle; } else { _e('Project live bekijken'); } ?>">
                        <?php
                        endif;
                        $thumb_id = get_post_thumbnail_id($post->ID);
                        $thumb_url = wp_get_attachment_image_src( $thumb_id, 'list-box-img', false );
                        $alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
                        ?>
                            <figure>
                                <img src="<?php echo $thumb_url[0]; ?>" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
                                <?php 
                                if (!empty($projectlink)) : 
                                ?>
                                <figcaption><?php the_title(); ?></figcaption>
                                <i class="fa fa-arrows-alt"></i>
                                <?php 
                                endif; 
                                ?>
                            </figure>
                        <?php
                        if (!empty($projectlink)) :
                        ?>
                        </a>
                        <?php
                        endif;
                        endif;
                        ?>
                        <div class="content">
                            <h4><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h4>
                            <?php
                            $intro = get_post_meta($post->ID,'intro',true);
                            if (!empty($intro)) {
                            $firsttag = substr($intro, 0, 3);  
                            if ($firsttag != '<p>') {
                                $intro = '<p>' .$intro. '</p>';
                            }
                            echo $intro;
                            }
                            ?>
                        </div><!-- /content -->
                    </article>
                </div><!-- /column -->
			<?php 
            if ($nr == 3 || $projectenQuery->found_posts == 9 || $projectenQuery->found_posts == $totalnr) : 
            ?>
			</div><!-- /row -->
        <?php 
        endif;        
        if ($nr == 3) {
            $nr = 0; 
        }
        endwhile;
        wp_reset_query();
        ?>
    </section>
    <?php
    $siteurl = get_option('siteurl');
    if ($totalnr < $projectenQuery->found_posts) :
    ?>
    <section class="section supercompact">
        <div class="loading-bar">
            <i class="fa fa-circle-o-notch loader" data-grid-url="<?php echo $siteurl; ?>/wp-content/themes/roots/lib/load-more-items.php" data-grid-type="project" data-grid-start="10"></i>
        </div>
    </section>
    <?php
    endif;
    ?>
    <section class="section compact">
        <?php get_template_part('templates/content','cta'); ?>
    </section>
</div><!-- /container -->	