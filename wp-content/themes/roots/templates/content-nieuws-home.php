<?php
wp_reset_query();
$args = array(
    'post_type' => 'nieuws',
    'posts_per_page' => 3,
    'post_status' => 'publish',
    'meta_key' => 'date',
    'orderby' => 'meta_value',
    'sort_order' => 'desc'	
);
$nieuwsQuery = new WP_Query( $args );
if ($nieuwsQuery->found_posts > 0) :
?>
<div class="row">
	<?php
	while ($nieuwsQuery->have_posts()) : 
	$nieuwsQuery->the_post(); 
	
    $linked_page = get_post_meta($post->ID,'page_link_url',true);
    $link_text = get_post_meta($post->ID,'page_link_text',true);
    if (!empty($linked_page)) :
    ?>
    <a href="<?php echo get_permalink($linked_page); ?>" title="<?php echo $link_text; ?>">
    <?php
    endif;
    ?>
	<div class="column">
		<?php 
		if (has_post_thumbnail()) : 
		$thumb_id = get_post_thumbnail_id($post->ID);
		$thumb_url = wp_get_attachment_image_src( $thumb_id, 'list-box-img', false );
		$meta = get_post($thumb_id);
		$alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
		$caption = $meta->post_excerpt;
		?>
		<figure>
            <img src="<?php echo $thumb_url[0]; ?>" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
			<figcaption><?php echo $caption; ?></figcaption>
		</figure>
		<?php
		endif;
		?>
		<div class="content">
			<h2><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h2>
			<?php
            $date = get_post_meta($post->ID,'date',true);
            if (!empty($date)) :
			?>
			<h3><?php echo $date; ?></h3>
			<?php 
            endif; 
            $intro = get_post_meta($post->ID,'intro',true);
            if (!empty($intro)) {
            $firsttag = substr($intro, 0, 3);  
            if ($firsttag != '<p>') {
                $intro = '<p>' .$intro. '</p>';
            }
            echo $intro;
            }
            ?>
		</div><!-- /content -->
        <?php
        if (!empty($linked_page)) :
        ?>
        </a>
        <?php
        endif;
        ?>
	</div><!-- /column -->
	<?php
	endwhile;
	wp_reset_query();
	?>
	<a href="<?php echo get_permalink(14); ?>" class="btn-1"><?php _e('Lees meer artikelen'); ?></a>
</div>
<?php
endif;
?>