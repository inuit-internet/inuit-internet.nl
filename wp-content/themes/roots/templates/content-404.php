<div class="container">
    <section class="section breadcrumb compact" role="region">
        <div class="content">
            <?php echo simple_breadcrumb(); ?>
        </div>
    </section>
    <section class="section compact" role="region">
		<div class="box-intro">
			<div class="content">
				<div class="textwrapper">
                    <?php
                    $title = ot_get_option('404_title');
                    $content = ot_get_option('404_content');
                    if (!empty($title)) : 
                    ?>
                    <h1><?php echo $title; ?></h1>
                    <?php 
                    endif; 
                    if (!empty( $content)) {
                    $firsttag = substr($content, 0, 2);  
                    if ($firsttag != '<p') {
                        $content = '<p class="intro">' .$content. '</p>';
                    }
                    elseif (strpos($content,'content') == false) {
                        $content = str_replace("<p","<p class=\"intro\"",$content);
                    }
                    echo $content;
                    }
                    ?>
                </div><!-- /textwrapper -->
                <?php
                //get_template_part('templates/content','cta');  
                ?>
			</div><!-- /content -->
			<?php include roots_sidebar_path(); ?>
		</div><!-- /box-intro -->
	</section>
</div><!-- /container -->