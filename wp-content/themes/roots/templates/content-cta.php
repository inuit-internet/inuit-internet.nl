<?php
$hide = get_post_meta($post->ID,'cta_hide',true);
if ($hide[0] != 'ja') :
$ctaTitle = get_post_meta($post->ID,'cta_title',true);
if (empty($ctaTitle)) {$ctaTitle = 'Vrijblijvend een offerte aanvragen?';}
$ctaContent = get_post_meta($post->ID,'cta_content',true);
$ctaBtntext = get_post_meta($post->ID,'cta_button_text',true);
$ctaUrl = get_post_meta($post->ID,'cta_button_url',true);
?>
<div class="box-cta">
    <?php
    if (!empty($ctaTitle)) : 
    ?>
    <h3><?php echo $ctaTitle; ?></h3>
    <?php
    endif;
    if (!empty($ctaContent)) {
        $firsttag = substr($ctaContent, 0, 3);  
        if ($firsttag != '<p>') {
            $ctaContent = '<p>' .$ctaContent. '</p>';
        }
        echo $ctaContent;
    }
    $contactpage = get_id_by_template('page-contact.php');
    ?>
    <a href="<?php if (!empty($ctaUrl)) { echo get_permalink($ctaUrl); } else { echo get_permalink($contactpage); } ?>" class="btn-1"><?php if (!empty($ctaBtntext)) {echo $ctaBtntext;} else { _e('Neem contact op'); } ?> <i class="fa fa-chevron-circle-right"></i></a>
</div>
<?php
endif;
?>