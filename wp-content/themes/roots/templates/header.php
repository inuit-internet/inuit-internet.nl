<header class="header" role="banner">
    <div class="container">
        <a href="<?php echo home_url(); ?>/" title="Logo van <?php bloginfo('name'); ?>" class="logo">
            <span><?php bloginfo('name'); ?></span>
        </a><!-- /logo -->
        <ul id="skip">
            <li><a href="#nav"><span><?php _e('Direct naar:'); ?></span><?php _e('Navigatie'); ?></a></li>
            <?php
            $sitemapid = get_id_by_template('page-sitemap.php');
            if ($sitemapid) :
            ?>
            <li><a href="<?php echo get_permalink(26); ?>"><span><?php _e('Direct naar:'); ?> </span><?php _e('Sitemap'); ?></a></li>
            <?php
            endif; 
            ?>
        </ul><!-- /skip -->
        <?php if (has_nav_menu('primary_navigation')) : ?>
		<nav id="nav" role="navigation">
            <h2><?php _e('Navigatie'); ?></h2>
			<a class="nav-link" href="#nav">
                <span aria-hidden="true"><i class="fa fa-bars"></i></span>
                <span class="screen-reader-text">Menu</span>
            </a>
            <div class="nav-main" id="nav-main">
                <ul id="menu-hoofd-navigatie" class="menu">
					<?php
					$args = array(
						'theme_location'  => 'primary_navigation',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '%3$s',
						'depth'           => 2,
						'walker'          => new Main_Nav_Walker()
					);
					wp_nav_menu($args);
					?>
                </ul>
            </div><!-- /nav main -->
        </nav><!-- /nav -->
		<?php endif; ?>
    </div><!-- /container -->
</header>
<?php get_template_part('templates/content','carousel'); ?>