<div class="container">
	<section class="section" role="region">
		<div class="box-intro">
			<?php
			$intro = get_post_meta($post->ID,'page_intro',true); 
			?>
			<div class="content">
				<h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
				<p>
				<?php
				if (!empty($intro)) {
					echo $intro; 
				}
				else {
					get_template_part('templates/content','loop'); 
				}
				?>
				</p>
			</div><!-- /content -->
		</div><!-- /box-intro -->
		<div class="box-items">
			<?php
			$nr = 0;
			$args = array(
				'post_type' => 'nieuws',
				'posts_per_page' => -1,
				'meta_key' => 'page_date',
				'orderby' => 'meta_value',
				'sort_order' => 'desc'
			);
			$nieuwsQuery = new WP_Query( $args );
			while ($nieuwsQuery->have_posts()) : 
			$nieuwsQuery->the_post(); 
			$nr++;
			?>
			
			<?php if ($nr == 1) : ?>
			<div class="row">
			<?php endif; ?>
			
				<div class="column">
					
					<?php 
					if (has_post_thumbnail()) : 
					$size = 'full';
					$thumb_id = get_post_thumbnail_id($post->ID);
					$thumb_url = wp_get_attachment_image_src( $thumb_id, 'full', false );
					$meta = get_post($thumb_id);
					$alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
					$caption = $meta->post_excerpt;
					?>
					<figure>
						<img src="<?php echo $thumb_url[0]; ?>" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
						<figcaption><?php echo $caption; ?></figcaption>
					</figure>
					<?php
					endif;
					?>
					<div class="content">
						<h2><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h2>
						<?php
						$date = get_post_meta($post->ID,'page_date',true);
						if (!empty($date)) :
						//var_dump($date);
						$parts = explode("-", $date);
						$day = $parts[2];
						$month = $parts[1];
						switch ($month) {
							case "01":
								$month = 'januari';
								break;
							case "02":
								$month = 'februari';
								break;
							case "03":
								$month = 'maart';
								break;
							case "04":
								$month = 'april';
								break;
							case "05":
								$month = 'mei';
								break;
							case "06":
								$month = 'juni';
								break;
							case "07":
								$month = 'juli';
								break;
							case "08":
								$month = 'augustus';
								break;
							case "09":
								$month = 'september';
								break;
							case "10":
								$month = 'oktober';
								break;
							case "11":
								$month = 'november';
								break;
							case "12":
								$month = 'december';
								break;
						}
						$year = $parts[0];
						?>
						<h3><?php echo $day . ' ' . $month . ' ' . $year; ?></h3>
						<?php endif; ?>
						<p>
						<?php
						$intro = get_post_meta($post->ID,'page_intro',true); 
						if (empty($intro)) {
							get_template_part('templates/content','loop');
						}
						else {
							echo $intro;
						}
						?>
						</p>
						<?php
						$linked_page = get_post_meta($post->ID,'page_link_url',true);
						$link_text = get_post_meta($post->ID,'page_link_text',true);
						if (!empty($linked_page) && !empty($link_text)) :
						?>
						<p>
							<a href="<?php echo $linked_page; ?>" class="external"><?php echo $link_text; ?></a>
						</p>
						<?php
						endif;
						if ((empty($link_text) && empty($linked_page)) && (!empty($intro))) :
						?>
						<p>
							<a href="<?php echo get_permalink($post->ID); ?>" class="external"><?php echo ot_get_option('readmore_text'); ?></a>
						</p>
						<?php
						endif;
						?>
					</div><!-- /content -->
				</div><!-- /column -->
			<?php if ($nr == 3) : ?>	
			</div><!-- /row -->
			<?php 
			$nr = 0;
			endif; 
			endwhile;
			wp_reset_query();
			?>
		</div><!-- /box-items -->
	</section>
</div><!-- /container -->