<footer class="footer" role="contentinfo">
    <h2><?php _e('Content informatie'); ?></h2>
	<section class="section light footer-top" role="region">
        <div class="container">
			<div class="column">
                <?php get_template_part('templates/content','vcard'); ?>
            </div><!-- /column -->
            <div class="column">
                <?php
                $referenties = ot_get_option('referenties');
                if (!empty($referenties)) :
                ?>
                <h3><span><strong><?php _e('Wat klanten zeggen'); ?></strong></span></h3>
                <div class="referenties">
                    <ul class="referentie-items">
                        <?php   
                        foreach($referenties as $referentie) :
                        $title = $referentie['title'];
                        $content = $referentie['referentie'];
                        $firsttag = substr($content, 0, 3);  
                        if ($firsttag != '<p>') {
                            $content = '<p>' .$content. '</p>';
                        }
                        ?>
                        <li>
                            <?php echo $content; ?>
                            <span>
                                <?php echo $title; ?>
                            </span>
                        </li>
                        <?php
                        endforeach; 
                        ?>
                    </ul><!-- /referenties -->
                </div>
                <?php
                endif;
                ?>
            </div><!-- /column --> 
			<?php 
            $socialmedia = ot_get_option('social_media'); 
            if (!empty($socialmedia)) :
			?>
            <div class="column">
                <h3 class="hidden"><?php _e('Volg ons'); ?></h3>
                <ul class="list-socialmedia">
                    <?php 
					foreach ($socialmedia as $medium) : 
					?>
					<li>
                        <a href="<?php echo $medium['url']; ?>" title="<?php echo $medium['link_title']; ?>" target="_blank">
                            <i class="fa fa-<?php echo $medium['title']; ?>-square"></i>
                            <span class="screen-reader-text"><?php echo $medium['screen_reader_text']; ?></span>
                        </a>
                    </li>
					<?php endforeach; ?>
                </ul>
            </div><!-- /column -->
			<?php endif; ?>
        </div>
    </section><!-- /section-->
    <section class="section compact footer-bottom" role="region">
        <div class="container">
			<?php 
			$footer_logos = ot_get_option('footer_logos'); 
			if (!empty($footer_logos)) :
			?>
             <div class="column">
                <ul class="list-logos">
					<?php 
					foreach ($footer_logos as $logo) :
					$titel = $logo['title'];
					$img = $logo['logo_img'];
					$alt = $logo['logo_alt'];
					$url = $logo['logo_link_url'];
					$linktext = $logo['logo_link_text'];
					$linkattr = $logo['logo_link_attr'];
 					?>
                    <li>
						<?php 
						if (!empty($img) && !empty($url)) : 
						list($width, $height, $type, $attr) = getimagesize($img);
						?>
                        <a href="<?php echo $url; ?>" title="<?php echo $titel; ?>" <?php if (!empty($linkattr)) { echo $linkattr; } ?>>
							<img src="<?php echo $img; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" alt="<?php echo $alt; ?>">
						</a>
						<?php 
						endif; 
						if (!empty($img) && empty($url)) :
						list($width, $height, $type, $attr) = getimagesize($img);						
						?>
							<img src="<?php echo $img; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" alt="<?php echo $alt; ?>">
						<?php
						endif;
						if (empty($img) && !empty($url) && !empty($linktext)) :
						?>
						<a href="<?php echo $url; ?>" title="<?php echo $titel; ?>" target="_blank" <?php if (!empty($linkattr)) { echo $linkattr; } ?>><?php echo $linktext; ?></a>
						<?php
						endif;
						?>
					</li>
                   <?php endforeach; ?>
                </ul>
            </div>
			<?php endif; ?>
            <div class="column">
                <div class="copyright">
                    <h2><?php _e('Copyright en technische informatie'); ?></h2>
                    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); _e('. Alle rechten voorbehouden.'); ?></p>
                </div>
            </div><!-- /column -->
            <?php
            if (has_nav_menu('footer_navigation')) :
            ?>
            <div class="column">
                <ul class="footer-nav">
                    <?php 
                    $args = array(
                    'theme_location'  => 'footer_navigation',
                    'menu'            => '',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => '',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '%3$s',
                    'depth'           => 0,
                    'walker'          => ''
                    );
                    wp_nav_menu($args); 
                    ?>
                </ul>
            </div>
            <?php 
            endif; 
            ?> 
        </div><!-- /container -->
    </section><!-- /section -->
    <?php 
	//phpinfo(); 
	//$accepted = cn_cookies_accepted();
	//var_dump($accepted);
	?>

</footer>