<div class="container">
	<section class="section" role="region">
		<div class="box-intro">
			<div class="content">
				<h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
				<?php 
                $intro = get_post_meta($post->ID,'intro',true);
                if (!empty($intro)) {  
                    echo $intro;  
                }
				get_template_part('templates/content','loop'); 
				?>
			</div><!-- /content -->
			<?php
			$page_menu = get_post_meta($post->ID,'page_menu_items',true); 
			$custom_post_menu = get_post_meta($post->ID,'custom_post_menu_items',true); 
			
			if (!empty($page_menu) && !empty($custom_post_menu)) {
                $merged = array_merge($page_menu,$custom_post_menu);
                $merged = array_unique($merged);
			}
			elseif (empty($page_menu) && !empty($custom_post_menu)) {
                $merged = $custom_post_menu;
			}
			elseif (!empty($page_menu) && empty($custom_post_menu)) {
                $merged = $page_menu;
			}
			if (!empty($merged)) :
			?>
			<div class="box-links">
				<h2><?php _e('Zie ook:'); ?></h2>
				<ul class="list-links">
					<?php 
					foreach ($merged as $item) :
					?>
					<li>
						<a href="<?php echo get_permalink($item); ?>"><?php echo get_the_title($item); ?></a>
					</li>
					<?php
					endforeach;
					?>
				</ul>
			</div><!-- /box-links -->
			<?php
			endif; 
			?>
		</div><!-- /box-intro -->
		<div class="box-services">
			<?php
			//wp_reset_query();
			$page_box_items = array();
			$page_box_items = get_post_meta($post->ID,'page_box_items',true); 
			$custom_post_box_items = get_post_meta($post->ID,'custom_post_box_items',true); 
			
			if (!empty($page_box_items) && !empty($custom_post_box_items)) {
                $merged = array_merge($page_box_items,$custom_post_box_items);
                $merged = array_unique($merged);
				$args = array(
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'post_type' => 'any',
					'post__in' => $merged,
					'page__in' => $merged,//array(51,52),
					'sort_order' => 'asc'
				);
			}
			elseif (!empty($page_box_items) && empty($custom_post_box_items)) {
				$args = array(
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'post_type' => 'page',
					'page__in' => $page_box_items,//array(51,52),
					'sort_order' => 'asc'
				);
			}
			elseif (empty($page_box_items) && !empty($custom_post_box_items)) {
				$args = array(
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'post_type' => 'any',
					'post__in' => $custom_post_box_items,
					'sort_order' => 'asc'
				);
			}
			
			$pageitemQuery = new WP_Query( $args );
			while ($pageitemQuery->have_posts()) : 
			$pageitemQuery->the_post(); 
			?>
			<div class="row">
				<div class="column full">
					<div class="content">
						<h2><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h2>
						<?php
                        get_template_part('templates/content','loop');
						$accordeon = get_post_meta($post->ID,'dienst_accordeon',true);
						if (!empty($accordeon)) :
						foreach ($accordeon as $accitem) :
						//var_dump($accitem);
						?>
							<div class="accordion">
								<div class="accordion-heading">
									<a href="#"><h3><?php echo $accitem['title']; ?></h3></a>
								</div><!-- /accordion-heading -->
								<div class="accordion-content">
									<div class="content">
										<p><?php echo $accitem['accordeon_item_content']; ?></p>
									</div>
								</div>
							</div><!-- /accordion -->
						<?php
						endforeach;
						endif;
						?>
						<?php
						$linked_page = get_post_meta($post->ID,'page_link_url',true);
						$link_text = get_post_meta($post->ID,'page_link_text',true);
						if (!empty($linked_page) && !empty($link_text)) :
						?>
						<p>
							<a href="<?php echo $linked_page; ?>" class="external"><?php echo $link_text; ?></a>
						</p>
						<?php
						endif;
						if ((empty($link_text) && empty($linked_page)) && (!empty($intro))) :
						?>
						<p>
							<a href="<?php echo get_permalink($post->ID); ?>" class="external"><?php echo ot_get_option('readmore_text'); ?></a>
						</p>
						<?php
						endif;
						?>
					</div><!-- /content -->
					<?php 
					if (has_post_thumbnail()) :
 					$size = 'thumbnail';
					$thumb_id = get_post_thumbnail_id($post->ID);
					$thumb_url = wp_get_attachment_image_src( $thumb_id, 'full', false );
					$meta = get_post($thumb_id);
					$alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
					$caption = $meta->post_excerpt;
					?>
					<div class="figure">
						<img src="<?php echo $thumb_url[0]; ?>" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
					</div><!-- /figure -->
					<?php
					endif; 
					?>
				</div><!-- /column -->
			</div>
			<?php
			endwhile;
			wp_reset_query();
			?>
		</div><!-- /box-services -->
	</section>
</div><!-- /container -->