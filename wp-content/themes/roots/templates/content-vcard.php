<div class="vcard" itemscope itemtype="http://schema.org/Organization">
    <?php
    $naam = ot_get_option('bedrijfsnaam');
    if (!empty($naam)) :
    ?>
    <h3><span itemprop="name" class="name"><strong><?php echo $naam; ?></strong></span></h3>
    <?php
    endif;
    ?>
    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <?php
        $adres = ot_get_option('adres');
        if (!empty($adres)) :
        ?>
        <span itemprop="streetAddress"><?php echo $adres; ?></span><br />
        <?php
        endif;
        $postcode = ot_get_option('postcode');
        if (!empty($postcode)) :
        ?>
        <span itemprop="postalCode"><?php echo $postcode; ?></span>
        <?php
        endif;
        $stad = ot_get_option('stad');
        if ($stad) :
        ?>
        <span itemprop="addressLocality"><?php echo $stad; ?></span><br />
        <?php
        endif;
        ?>
    </div>
    <?php
    $tel = ot_get_option('tel');
    $tel_stripped = preg_replace('/[^0-9]/', '', $tel);
    if (!empty($tel_stripped)) :
    ?>
    T:
    <span itemprop="telephone"><a href="tel:<?php echo $tel_stripped; ?>"><?php echo $tel; ?></a></span><br />
    <?php
    endif;
    $email = ot_get_option('email');
    if (!empty($email)) :
    ?>
    E:
    <span itemprop="email"><a href="mailto:<?php echo $email; ?>" title="Stuur een email naar <?php bloginfo('name'); ?>"><?php echo $email; ?></a></span><br />
    <br />
    <div>
    <h3><span><strong><?php _e('Overige informatie'); ?></strong></span></h3>
    <?php
    endif;
    $kvk = ot_get_option('kvk');
    if (!empty($kvk)) :
    ?>
    <span><?php printf(__('KvK nummer: %s'),$kvk); ?></span><br />
    <?php
    endif;
    $IBAN = ot_get_option('iban');
    if (!empty($IBAN)) :
    ?>
    <span><?php printf(__('ASN Bank: %s'),$IBAN); ?></span><br />
    <?php
    endif;
    $BTW = ot_get_option('btw');
    if (!empty($BTW)) :
    ?> 
    <span><?php printf(__('BTW nummer: %s'),$BTW); ?></span>
    <?php
    endif;
    ?>
    </div>
</div><!-- /vcard -->