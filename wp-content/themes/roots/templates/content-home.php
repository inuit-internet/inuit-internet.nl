<section class="section" role="region"> 
    <div class="container homepage-intro">
        <div class="homepage-intro-content"> 
        <?php
        $title = get_post_meta($post->ID,'what_we_do_title',true);
        if (!empty($title)) :
        ?>
        <h1><?php echo $title; ?></h1>
        <?php 
        endif; 
        $content = get_post_meta($post->ID,'what_we_do_intro',true); 
        if (!empty($content)) {
        $firsttag = substr($content , 0, 3);  
        if ($firsttag != '<p>') {
           $content  = '<p>' .$content. '</p>';
        }
        echo $content;
        }
        ?>
        </div> 
    </div>
</section>
<section class="section section-grey" role="region"> 
    <div class="container homepage-intro">
        <?php
        $boxes = get_post_meta($post->ID,'what_we_do_boxes',true);
        if (!empty($boxes)) :
        $nr = 0;
        foreach ($boxes as $box) : $nr++;
        $boxrel = $box['wwd_box_url'];
        $boxbtntxt = $box['wwd_box_btn_text'];
        if ($nr == 1 || ($nr % 4) == 0) :
        ?>
        <ul class="homepage-intro-list">
        <?php 
        endif;
        ?>
            <li>
                <?php
                if (!empty($boxrel)) : ?>
                <a href="<?php echo get_permalink($boxrel); ?>" title="<?php if (!empty($boxbtntxt)) {echo $boxbtntxt;} else { printf(__('Meer informatie over %s'),$box['title']); } ?>">
                <?php
                endif;
                $title = strtolower($box['title']);
                if (!empty($title)) :
                $titlearr = explode(' ',$title);
                if (count($titlearr) <= 2) {
                $firstpart = $titlearr[0];
                $lastpart = $titlearr[1];
                }
                else {
                $lastpart = $firstpart = false;
                }
                ?>
                <h3><?php if (!empty($lastpart) && !empty($firstpart)) { echo $firstpart. ' ' ."<span>$lastpart</span>"; } else { echo $title; } ?></h3>
                <?php
                endif;
                $img = $box['wwd_box_img'];
                if (!empty($img)) :
                $imgId = get_attachment_id_from_src($img);
                if (!empty($imgId)) {
                    $img = wp_get_attachment_image_src($imgId,'list-box-img');
                    $src = $img[0];
                    $width = $img[1];
                    $height = $img[2];
                    $alt = get_post_meta( $imgId, '_wp_attachment_image_alt', true);
                } else {
                    $src = $img;
                    $alt = $box['title'];
                    list($width, $height) = getimagesize($img);
                }
                ?>
                <img src="<?php echo $src; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" alt="<?php echo $alt; ?>">
                <?php
                endif; 
                $content = $box['wwd_box_content'];
                if (!empty($content)) {
                $firsttag = substr($content , 0, 3);  
                if ($firsttag != '<p>') {
                   $content  = '<p>' .$content. '</p>';
                }
                echo $content; 
                }
                if (!empty($boxrel)) : 
                ?>
                    <span class="btn-1"> <i class="fa fa-chevron-circle-right"></i></span>
                </a>
                <?php
                endif;
                ?>
            </li>
        <?php 
        if (($nr % 3) == 0) :
        ?>
        </ul>
        <?php
        $nr = 0;
        endif;
        endforeach;
        endif;
      ?>
    </div><!-- /homepage-intro -->
</section>
<section class="section" role="region">
    <div class="container box-items homebox-items">  
        <?php get_template_part('templates/content','projecten-home'); $projectpage = get_id_by_template('page-portfolio.php');?>	
        <div class="center">
            <a href="<?php echo get_permalink($projectpage); ?>" class="btn-1"><?php _e('Bekijk ons portfolio'); ?> <i class="fa fa-chevron-circle-right"></i></a>	
        </div>
    </div><!-- /container -->
</section>	 