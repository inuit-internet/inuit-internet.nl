<div class="container">
    <section class="section breadcrumb compact" role="region">
        <div class="content">
         
            <?php 
            if ( function_exists('yoast_breadcrumb') ) 
            {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} 
            ?>
      
        </div>
    </section>
    <section class="section compact" role="region">
		<div class="box-intro">
			<div class="content">
				<div class="textwrapper">
                    <h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
                    <?php 
                    $intro = get_post_meta($post->ID,'intro',true);
                    if (!empty($intro)) {
                    $firsttag = substr($intro, 0, 2);  
                    if ($firsttag != '<p') {
                        $intro = '<p class="intro">' .$intro. '</p>';
                    }
                    elseif (strpos($intro,'intro') == false) {
                        $intro = str_replace("<p","<p class=\"intro\"",$intro);
                    }
                    echo $intro;
                    }
                    if (has_post_thumbnail()) :
                    $showlink = 0;
                    $thumb_id = get_post_thumbnail_id($post->ID);
                    $thumb_url = wp_get_attachment_image_src( $thumb_id, 'page-thumb-large', false );
                    $large_url = wp_get_attachment_image_src( $thumb_id, 'large', false );
                    $alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
                    if (!empty($large_url) && $showlink == 1) :
                    ?>
                    <a href="<?php echo $large_url[0]; ?>">
                    <?php endif; ?>
                    <img src="<?php echo $thumb_url[0]; ?>" class="floatleft" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
                    <?php if (!empty($large_url) && $showlink == 1) : ?>
                    </a>
                    <?php
                    endif;
                    endif;
                    get_template_part('templates/content','loop'); 
                    get_template_part('templates/content','cta');  
                    ?>
                </div><!-- /textwrapper -->
			</div><!-- /content -->
			<?php include roots_sidebar_path(); ?>
		</div><!-- /box-intro -->
	</section>
</div><!-- /container -->