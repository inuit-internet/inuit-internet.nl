<?php
wp_reset_query();
$args = array(
'post_type' => 'project',
'posts_per_page' => -1,
'orderby' => 'date',
'order' => 'desc'
);
$projectenQuery = new WP_Query( $args );
if ($projectenQuery->found_posts > 0) :
?>
<div class="row">
    <?php
    $nr = 0;
	while ($projectenQuery->have_posts()) :  
	$projectenQuery->the_post(); 
    $featured = get_post_meta($post->ID,'featured_project',true);
    if ($featured[0] == 'ja'){$nr++;}
    if ($nr <= 3 && $featured[0] == 'ja') :
    ?>
        <div class="column portfolio-item">
        <article class="article">
        <?php
        $projectlink = get_post_meta($post->ID,'project_link_url_external',true);
        $projecthasdetails = get_post_meta($post->ID,'project_has_details',true);
        $target = '_blank';
        
        if ($projecthasdetails[0] == 'ja') {
            $projectlink = get_permalink($post->ID);
            $target = '_self'; 
        }
     
        if (!empty($projectlink)) :
        $projectlinktitle = get_post_meta($post->ID,'project_link_text',true);
        ?>
        <a href="<?php echo $projectlink; ?>" target="<?php echo $target; ?>" title="<?php if (!empty($projectlinktitle)) { echo $projectlinktitle; } else { _e('Bekijk dit project'); } ?>">
        <?php
        endif;
        if (has_post_thumbnail()) : 
        $thumb_id = get_post_thumbnail_id($post->ID);
        $thumb_url = wp_get_attachment_image_src( $thumb_id, 'list-box-img', false );
        $alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
        ?>
        <figure>
            <img src="<?php echo $thumb_url[0]; ?>" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
            <?php 
            if (!empty($projectlink)) : 
            ?>
            <figcaption></figcaption>
            <i class="fa fa-arrows-alt"></i>
            <?php 
            endif; 
            ?>
        </figure>
        <?php
        endif;
        ?>
        <div class="content">
            <h2><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h2>
            <?php
            $intro = get_post_meta($post->ID,'intro',true);
            if (!empty($intro)) {
            $firsttag = substr($intro, 0, 3);  
            if ($firsttag != '<p>') {
                $intro = '<p>' .$intro. '</p>';
            }
            echo $intro;
            }
            ?>
        </div><!-- /content -->
        <?php
        if (!empty($projectlink)) :
        ?>
        </a>
        <?php
        endif;
        ?>
        </article>
    </div><!-- /column -->
	<?php
    endif;
    endwhile;
	wp_reset_query();
	
    ?>
</div>
<?php
endif;
?>