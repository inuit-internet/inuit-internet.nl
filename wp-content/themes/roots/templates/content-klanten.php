<div class="box-clients">
	<?php
	$klantentitel = ot_get_option('klanten_box_title');
	$klantencontent = ot_get_option('klanten_box_content');
	if (!empty($klantentitel)) :
	?>
	<h2><?php echo $klantentitel; ?></h2>
	<?php
	endif;
	if (!empty($klantencontent)) :
	?>
	
	<p>
		<?php echo $klantencontent; ?>
	</p>
	
	<ul class="list-clients">
		<?php
		endif;
		$args = array(
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'post_type' => 'klant',
			'sort_order' => 'asc'
		);
		
		$klantenQuery = new WP_Query( $args );
		while ($klantenQuery->have_posts()) : 
		$klantenQuery->the_post(); 
		
		if (has_post_thumbnail()) : 
		$size = 'full';
		$thumb_id = get_post_thumbnail_id($post->ID);
		$thumb_url = wp_get_attachment_image_src( $thumb_id, 'full', false );
		$meta = get_post($thumb_id);
		$externallink = get_post_meta($post->ID,'page_link_url',true);
		$alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
		$caption = $meta->post_excerpt;
		?>
		<li>
			<?php if (!empty($externallink)) : ?>
			<a href="<?php echo $externallink; ?>" title="<?php echo $caption; ?>" class="external">
			<?php endif; ?>
				<img src="<?php echo $thumb_url[0]; ?>" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
			<?php if (!empty($externallink)) : ?>
			</a>
			<?php endif; ?>
		</li>
		<?php
		endif;
		endwhile;
		wp_reset_query();
		?>
	</ul>
</div><!-- /box-clients -->