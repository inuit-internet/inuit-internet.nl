<br />
<div class="container">
	<section class="section compact" role="region">
		<div class="box-intro">
			<div class="content">
				<?php 
                if (isset($_GET['i'])) {
                switch ($_GET['i']) { 
                case 'e-commerce': 
                $ctastring = 'Weten hoe WordPress e-commerce uw onderneming verder kan helpen? ';  
                break;
                case 'seoscan' :
                $ctastring = 'Weten hoe uw website ervoor staat in Google? Vraag een <span class="cta-text">gratis seoscan</span> aan. ';
                break;
                }
                } 
                ?>
                <h1><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h1>
				<?php 
               
                $intro = get_post_meta($post->ID,'intro',true);
                if (!empty($ctastring)) {$intro = $ctastring . $intro;}
                //echo $intro;
                
                
                if (!empty($intro)) {
                $firsttag = substr($intro, 0, 2);  
                if ($firsttag != '<p') {
                    $intro = '<p class="intro">' .$intro. '</p>';
                }
                elseif (strpos($intro,'intro') == false) {
                    $intro = str_replace("<p","<p class=\"intro\"",$intro);
                }
                echo $intro;
                }
                
				get_template_part('templates/content','loop'); 
				?>
			</div><!-- /content -->
			<?php 
			$socialmedia = ot_get_option('social_media'); 
			if (!empty($socialmedia)) :
			?>
            <div class="column">
                <ul class="list-socialmedia">
                    <?php 
					foreach ($socialmedia as $medium) : 
					?>
					<li>
                        <a href="<?php echo $medium['url']; ?>" title="<?php echo $medium['link_title']; ?>" target="_blank">
                            <i class="fa fa-<?php echo $medium['title']; ?>-square"></i>
                            <span class="screen-reader-text"><?php echo $medium['screen_reader_text']; ?></span>
                        </a>
                    </li>
					<?php 
                    endforeach; 
                    ?>
                </ul>
            </div><!-- /column -->
			<?php endif; ?>
		</div><!-- /box-intro -->
		<div class="box-contact">
			<div class="row">
				<div class="column">		
					 <?php get_template_part('templates/content','vcard'); ?>
				</div>
				<div class="column">
					<?php 
                    if (!isset($_GET['f'])) :
                        gravity_form(1, true, true, false, '', false); 
                    else :
                    ?>
                    <p class="intro">    
                    <?php _e('Dank. Wij hebben uw bericht ontvangen en zullen zo spoedig mogelijk contact met u opnemen.'); ?>
                    </p>
                    <?php 
                    endif;
                    ?> 
                </div>
			</div>
            <?php
            $contactbox1 = get_post_meta($post->ID,'contact-box-1',true);
            $contactbox2 = get_post_meta($post->ID,'contact-box-2',true);
            if (!empty($contactbox1) || !empty($contactbox2)) :
			?>
            <div class="row">
                <?php if (!empty($contactbox1)) : ?>
                <div class="column full">
					<?php 
                    $firsttag = substr($contactbox1 , 0, 3);  
                    if ($firsttag != '<p>') {
                       $contactbox1  = '<p>' .$contactbox1. '</p>';
                    }
                    echo $contactbox1; 
                    ?>
				</div>
				<?php
				endif;
				if (!empty($contactbox2)) :
				?>
				<div class="column">
					<?php 
                    $firsttag = substr($contactbox2 , 0, 3);  
                    if ($firsttag != '<p>') {
                       $contactbox2  = '<p>' .$contactbox2. '</p>';
                    }
                    echo $contactbox2; 
                    ?>
				</div>
				<?php
				endif;
				?>
			</div>
            <?php
            endif;
            ?>
		</div><!-- /box-contact -->
	</section>
</div><!-- /container -->