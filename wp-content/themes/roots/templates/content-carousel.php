<?php
$carousel = get_post_meta($post->ID,'carousel',true);
if (!empty($carousel)) :
?>
<div class="carousel">
    <ul class="carousel-images">
        <?php
        foreach($carousel as $car) :
        $bgimg = $car['bg-img'];
        $img = $car['front-img'];
        $title = $car['title'];
        $content = $car['content'];
        $url = $car['link_url'];
        $page = $car['link_rel_page'];
        $linktitle = $car['link_title'];
        $linktext = $car['link_text']; 
        $classes = $car['classes']; 
        $imgpos = $car['img_pos'];
        ?>
        <li style="background-image: url('<?php echo $bgimg; ?>')" <?php if (!empty($classes)) : ?>class="<?php echo $classes; ?>"<?php endif; ?>>
            <div class="container">
                <?php
                if ($imgpos == 'over-content' || empty($imgpos)) :
                $thumbnail_id = get_attachment_id_from_src($img);
                $img = wp_get_attachment_image_src($thumbnail_id,'full');
                $src = $img[0];
                $width = $img[1];
                $height = $img[2];
                $alt = get_post_meta( $thumbnail_id , '_wp_attachment_image_alt', true);
                if (empty($src)) {
                    $src = $img;
                }
                if (!empty($src)) :
                ?>
                <div class="carousel-img-wrapper">
                    <img src="<?php echo $src; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" alt="<?php echo $alt; ?>" />
                </div>
                <?php
                endif;
                endif;
                ?>
                <div class="carousel-content">
                    <div class="carousel-content-inner">
                    <?php 
                    if (!empty($title)) :
                    ?>
                    <strong><?php echo $title; ?></strong>
                    <?php
                    endif;
                    if (!empty($content)) {
                        $firsttag = substr( $content, 0, 3);
                        if ($firsttag != '<p>') {
                          $content = '<p>' . $content . '</p>';
                        }
                        echo $content;
                    }
                    ?>
                    </div><!-- eo carousel-content-inner -->
                    <?php
                    if ( (!empty($linktext) && !empty($url)) || (!empty($linktext) && !empty($page)) ) :
                    ?>
                    <a href="<?php if (!empty($page) && !empty($url)) {echo get_permalink($page) . $url;} elseif(!empty($page)) {echo get_permalink($page);} else {echo $url;} ?>"<?php if (!empty($linktext)) : ?>title="<?php echo $linktext; ?>"<?php endif; ?> class="btn-1"><?php echo $linktext; ?> <i class="fa fa-chevron-circle-right"></i></a>
                    <?php
                    endif;
                    ?>
                </div><!-- /carousel-content -->
                <?php
                if ($imgpos == 'below-content') :
                $thumbnail_id = get_attachment_id_from_src($img);
                $img = wp_get_attachment_image_src($thumbnail_id,'full');
                $src = $img[0];
                $width = $img[1];
                $height = $img[2];
                $alt = get_post_meta( $thumbnail_id , '_wp_attachment_image_alt', true);
                if (empty($src)) {
                    $src = $img;
                }
                if (!empty($src)) :
                ?>
                <img src="<?php echo $src; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" alt="<?php echo $alt; ?>" />
                <?php
                endif;
                endif;
                ?>
            </div><!-- /container -->
        </li>
        <?php endforeach; ?>
    </ul>
</div><!-- /carousel -->
<?php
endif;
?>