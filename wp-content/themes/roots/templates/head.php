<?php include_once locate_template('/lib/pre_template_logic.php'); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="author" content="Inuit Internet Diensten" />
<meta name="viewport" content="width=device-width,initial-scale=1" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<!--[if IEMobile]><meta http-equiv="cleartype" content="on" /><![endif]-->
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="google-site-verification" content="veH8K7_TkIDc7kmhGrmDTDQdjE6lKhixweBnL60P0Vs" />
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#603cba">
<meta name="theme-color" content="#ffffff">
<title><?php wp_title(); ?></title>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/static/css/style.css" />
<script src="<?php echo get_template_directory_uri(); ?>/static/js/vendor/modernizr-2.5.3.min.js" defer="defer"></script>
<!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/static/js/vendor/respond.min.js" defer="defer"></script>
<![endif]-->
<script src="<?php echo get_template_directory_uri(); ?>/static/js/libs/jquery.min.js"></script>

  <script src="<?php get_template_directory_uri(); ?>/static/js/libs/jquery.easing.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/libs/components.carousel.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/libs/components.shadowbox.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/plugins-min.js" defer="defer"></script>
    <script src="<?php get_template_directory_uri(); ?>/static/js/inuit.js" defer="defer"></script>
<?php
if (is_page('contact')) : 
?>
<script defer="defer" src='https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&key=AIzaSyC7EZrAkcOaa-Mr6uI92GFiWQ7PpfdDpHc'></script>
<?php 
endif; 
wp_head(); 
?>
<style type="text/css" media="screen">
	html { margin-top:0px!important; }
	* html body { margin-top:0!important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top:0!important; }
		* html body { margin-top:0!important; }
	}
</style>
</head>