<?php
error_reporting(E_ALL);
date_default_timezone_set('Europe/Amsterdam');
require_once 'GoogleAPI/autoload.php';  
session_start();

function create_gcal_event() {
    $client_id = '234168905645-csdt0vv53e79sltvilg7cpjuat0sir9j.apps.googleusercontent.com';
    $Email_address = '234168905645-csdt0vv53e79sltvilg7cpjuat0sir9j@developer.gserviceaccount.com';	 
    $key_file_location = 'My Project-47438ff35d50.p12';	 	
    $key = file_get_contents($key_file_location);	 
    $scopes = array('https://www.googleapis.com/auth/calendar');

    if (strpos($client_id, "googleusercontent") == false
        || !strlen($Email_address)
        || !strlen($key_file_location)) {
      echo 'missing service account details';
      exit;
    }

    $client = new Google_Client();
    $client->setApplicationName("Samantha Weel");

    if (isset($_SESSION['service_token'])) {
        $client->setAccessToken($_SESSION['service_token']);
    }

    $cred = new Google_Auth_AssertionCredentials(
        $Email_address,
        $scopes,
        $key
    );
    $client->setAssertionCredentials($cred);

    if ($client->getAuth()->isAccessTokenExpired()) {
        $client->getAuth()->refreshTokenWithAssertion($cred);
    }
    $_SESSION['service_token'] = $client->getAccessToken();
    $service = new Google_Service_Calendar($client);

    $eventStart = '2015-10-04T12:00:00+02:00'; //$_GET van ajax get ophalen
    $eventEnd = '2015-10-04T13:15:00+02:00'; //$_GET van ajax get ophalen
    
    $event = new Google_Service_Calendar_Event(array(
      'summary' => 'Afspraak naam', //hier naam van formulier invullen
      'location' => 'De Glazen Boerderij 20, Schijndel',
      'description' => 'Behandeling',
      'start' => array(
        'dateTime' => $eventStart,
        'timeZone' => 'Europe/Amsterdam',
      ),
      'end' => array(
        'dateTime' => $eventEnd,
        'timeZone' => 'Europe/Amsterdam',
      ),
      'attendees' => array(
        array('email' => 'info@huidenfruit.nl'),
        array('email' => 'sebradius@gmail.com') //hier email adres van formulier invullen
      ),
      'reminders' => array(
        'useDefault' => FALSE,
        'overrides' => array(
          array('method' => 'email', 'minutes' => 24 * 60),
        ),
      ),
    ));

    $calendarId = 'huidenfruit@gmail.com';
    $event = $service->events->insert($calendarId, $event);
    var_dump($event);
}
create_gcal_event();
?>