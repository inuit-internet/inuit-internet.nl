<?php
error_reporting(E_ALL);
date_default_timezone_set('Europe/Amsterdam');
require_once 'GoogleAPI/autoload.php';  
session_start();

function get_date_free_timespans() {
    $client_id = '234168905645-csdt0vv53e79sltvilg7cpjuat0sir9j.apps.googleusercontent.com';
    $Email_address = '234168905645-csdt0vv53e79sltvilg7cpjuat0sir9j@developer.gserviceaccount.com';	 
    $key_file_location = 'My Project-47438ff35d50.p12';	 	
    $key = file_get_contents($key_file_location);	 
    $scopes = array('https://www.googleapis.com/auth/calendar');

    if (strpos($client_id, "googleusercontent") == false
        || !strlen($Email_address)
        || !strlen($key_file_location)) {
      echo 'missing service account details';
      exit;
    }

    $client = new Google_Client();
    $client->setApplicationName("Samantha Weel");

    if (isset($_SESSION['service_token'])) {
        $client->setAccessToken($_SESSION['service_token']);
    }

    $cred = new Google_Auth_AssertionCredentials(
        $Email_address,
        $scopes,
        $key
    );
    $client->setAssertionCredentials($cred);

    if ($client->getAuth()->isAccessTokenExpired()) {
        $client->getAuth()->refreshTokenWithAssertion($cred);
    }
    $_SESSION['service_token'] = $client->getAccessToken();
    $service = new Google_Service_Calendar($client);

    $mycalendars = array( array("id"=>"huidenfruit@gmail.com"));
      
    $selectedDate = '04-10-2015'; //$_GET van ajax get ophalen
    $dateStartTime = '09:00'; //$_GET van ajax get ophalen
    $dateEndTime = '22:00'; //$_GET van ajax get ophalen
    $totalminuten = 60;  //$_GET['totalminuten'] van ajax get ophalen
    $totalminuten = $totalminuten + 15; //15 min erbij optellen
    $interval = 15; //10 minuten check interval
    $availstartTimes = array();
     
    $startpointmomentstring = "$selectedDate $dateStartTime";
    $startpointmomentObj = new DateTime($startpointmomentstring);

    $endpointmomentstring = "$selectedDate $dateEndTime";
    $endpointmomentObj = new DateTime($endpointmomentstring);

    $momentpointer = $startpointmomentObj;
    $internalmax = date_sub($endpointmomentObj, date_interval_create_from_date_string("$totalminuten minutes"));

    while($momentpointer < $internalmax) {
        
        $timeMin = clone($momentpointer);
        $temp = clone($timeMin);
        $timeMax = date_add($temp, date_interval_create_from_date_string("$totalminuten minutes"));
        
        $timeMin = $timeMin->format("c");
        $timeMax = $timeMax->format("c");
        
        $freebusy = new Google_Service_Calendar_FreeBusyRequest();
        $freebusy->setTimeMin($timeMin);
        $freebusy->setTimeMax($timeMax);
        $freebusy->setTimeZone('Europe/Amsterdam');
        $freebusy->setItems( $mycalendars  );
        $createdReq = $service->freebusy->query($freebusy);

        $result = $createdReq->calendars['huidenfruit@gmail.com']->busy[0]->end;
        if ($result == NULL || $result == false) {//free startpoint
          $availstartTimes[][$timeMin] = [$timeMax];
        }
        
        $momentpointer = date_add($momentpointer, date_interval_create_from_date_string("$interval minutes"));
    }
      
    if (!empty($availstartTimes)) {
      $availstartTimes = json_encode($availstartTimes);
      echo $availstartTimes;
      var_dump($availstartTimes); 
      //return to ajax call
    }
}
get_date_free_timespans();

/*
$calendarId = 'huidenfruit@gmail.com';
$optParams = array(
  'maxResults' => 10,
  'orderBy' => 'startTime',
  'singleEvents' => TRUE,
  'timeMin' => date('c'),
);
$results = $service->events->listEvents($calendarId, $optParams);

if (count($results->getItems()) == 0) {
  print "No upcoming events found.\n";
} else {
  print "Upcoming events:\n";
  foreach ($results->getItems() as $event) {
    $start = $event->start->dateTime;
    if (empty($start)) {
      $start = $event->start->date;
    }
    printf("%s (%s)\n", $event->getSummary(), $start);
  }
}
*/
?>