<?php
require_once("/home/gruiters/domains/inuit-internet.nl/public_html/wp-load.php");

function fire_backup($url) {

$ch=curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,false);
//curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
$rawdata=curl_exec($ch);
curl_close($ch);

}

function email_notification($to,$subject,$message,$label) {
    $headers = 'From: website-monitor@inuit-internet.nl' . "\r\n" .
    'MIME-Version: 1.0' . "\r\n" .
    'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
    'Reply-To: noreply@inuit-internet.nl' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
    mail($to, $subject, $message, $headers);
}

function process_backups() {
    $ot_array = get_option('option_tree');
    $subscriptions = $ot_array['backup-subscriptions'];

    foreach ($subscriptions as $subscription) {
        $label = $subscription['title'];
        $url = $subscription['url'];
        $notify = $subscription['notify']; 
        if (!empty($url)) {
            //echo $url;
            fire_backup($url);
            if ( $notify[0] == 'yes' ) {
                $to = $subscription['notify-email-adres'];
                $subject = "Website back-up voltooid: {$label}";
                $message = "Hallo, <br><br> Er heeft een cloud back-up plaatsgevonden van {$label} <br><br>Vriendelijke groet, <br /><br />Inuit Internet Diensten<br>http://www.inuit-internet.nl";
                email_warning($to,$subject,$message,$label);
            }
        } 
        else {
            continue;
        }
    }
}

process_backups();
exit();
?>