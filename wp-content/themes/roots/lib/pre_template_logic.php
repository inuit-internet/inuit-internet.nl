<?php 
/*
if (is_404()) {
    $home = get_bloginfo('url');
    header("Status: 301 Moved Permanently");
    header("Location:$home");
    exit();
}
*/

if (session_status() == PHP_SESSION_NONE) {
    session_start(); 
}
$firstchild = get_first_child($post->ID);
if (!empty($firstchild) && empty($post->post_parent) && !is_404()) {
    wp_safe_redirect($firstchild);
    exit();
}
$template = get_page_template_slug($post->ID);
if ($template == 'page-portfolio.php') { 
    if (!empty($_GET['category_name'])) {
        
        $catname = $_GET['category_name'];
        $category = get_category_by_slug($catname);
        $catID = $category->term_id;
        
        if ($catname !== 'all') {
            
            unset($_SESSION['selected_categories']);
            
            if (isset($_SESSION['selected_categories'][$catID])) {
                unset($_SESSION['selected_categories'][$catID]);
            }
            else {
                $_SESSION['selected_categories'][$catID] = $catname;
            }    
        }
        else {
            unset($_SESSION['selected_categories']);
            wp_safe_redirect(get_permalink($post->ID));
            exit();
        }
    }
}
else {
    unset($_SESSION['selected_categories']);
}

if (isset($_GET['download'])) {
   if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off');	}
   $file = get_post($_GET['download']);
   $file_url = $file->guid;
   $file_title = $file->post_title;
   $file_mime_type = $file->post_mime_type;
   $filearray = explode('/',$file_url);
   $filename = end($filearray);
   $upload_dir_array = wp_upload_dir();
   $upload_path = $upload_dir_array['path'];
   $complete_path = $upload_path.'/'.$filename;
   if(file_exists("$complete_path")) {
        header('Pragma: public'); 	// required
        header('Expires: 0');		// no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private',false);
        header("Content-disposition: attachment; filename=$filename");
        header("Content-Type:$file_mime_type");
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize("$complete_path"));	// provide file size
        header('Connection: close');
        readfile($complete_path);
        exit();
    }
}
?>