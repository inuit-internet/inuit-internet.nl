<?php

// Custom metaboxes

/**
 * Initialize the meta boxes before anything else.
 */
add_action( 'admin_init', 'specific_register_meta_boxes' );

/**
 * Builds the Meta Boxes.
 */

 //get custom post types
function specific_get_id() {
  return isset( $_GET['post'] ) ? $_GET['post'] : ( isset( $_POST['post_ID'] ) ? $_POST['post_ID'] : false );
}
 
function specific_register_meta_boxes() { 
  $post_id = specific_get_id();

  if ( function_exists( 'ot_register_meta_box' ) && $post_id ) {
    $post_template = get_page_template_slug($post_id);
    $postobj = get_post($post_id);
    $type = get_post_type($post_id);
    
    if ($type == 'nieuws') {
        nieuws_cpt_meta_boxes();
    }
    if ($type == 'project') {
        project_cpt_meta_boxes();
    }
    if ($post_template == 'page-home.php') {
        homepage_template_meta_boxes();
    }
    if ($post_template == 'page.php') {
        page_template_meta_boxes();
    }
    if ($post_template == 'page-portfolio.php') {
        portfolio_template_meta_boxes();
    }
    if ($post_template == 'page-contact.php') {
        contact_template_meta_boxes();
    }
    if ($post_template == 'page-full.php') {
        full_width_template_meta_boxes();
    }
    if ($post_template == 'archive-nieuws.php') {    
        nieuws_template_meta_boxes(); 
    }
  }
  return false;
} 
 
function homepage_template_meta_boxes() {
  $meta_args_array = array(
        array(
		   'id'          => 'homepage_settings',
		   'title'       => 'Homepage instellingen',
		   'pages' => array('page'),
		   'context'     => 'normal',
		   'priority'    => 'high',
		   'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           )
           )
        ),
        array(
          'id'          => 'homepage_whatwedo',
          'title'       => 'What we do instellingen',
          'pages' => array('page'),
          'context'     => 'normal',
          'priority'    => 'high',
          'fields'      => array( 
            array(
            'id'          => 'what_we_do_title',
            'label'       => 'What we do title',
            'desc'        => '',
            'std'         => '',
            'type'        => 'text',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'class'       => ''
          ),
          array(
            'id'          => 'what_we_do_intro',
            'label'       => 'What we do intro',
            'desc'        => '',
            'std'         => '',
            'type'        => 'wysiwyg',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'class'       => ''
          ),
          array(
            'id'          => 'what_we_do_img',
            'label'       => 'What we do image',
            'desc'        => '',
            'std'         => '',
            'type'        => 'upload',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'class'       => ''
          ),
          array(
          'id'          => 'what_we_do_boxes',
          'label'       => 'What we do boxes',
          'desc'        => '',
          'std'         => '',
          'type'        => 'list-item',
          'rows'        => '',
          'post_type'   => '',
          'taxonomy'    => '',
          'class'       => '',
		  'settings'    => array (
            array (
            'id' => 'wwd_box_content',
            'label' => 'Box content',
            'type' => 'wysiwyg'
            ),
            array (
            'id' => 'wwd_box_img',
            'label' => 'Box image',
            'type' => 'upload'
            ),
            array (
            'id' => 'wwd_box_url',
            'label' => 'Box URL',
            'type' => 'custom-post-type-select',
            'post_type' => 'page,post,klant,product,dienst,project,nieuws'
            ),
            array (
            'id' => 'wwd_box_btn_text',
            'label' => 'Box btn text',
            'type' => 'text'
            )
            )
            )
            
            )
        ),
        array(
		   'id'          => 'homepage_carousel_settings',
		   'title'       => 'Carousel instellingen',
		   'pages' => array('page'),
		   'context'     => 'normal',
		   'priority'    => 'high',
		   'fields'      => array( 
                array(
                'id'          => 'carousel',
                'label'       => 'Carousel',
                'desc'        => '',
                'std'         => '',
                'type'        => 'list-item',
                'rows'        => '',
                'post_type'   => '',
                'taxonomy'    => '',
                'class'       => '',
                'settings'    => array (
                    array (
                    'id' => 'bg-img',
                    'label' => 'Achtergrond afbeelding',
                    'type' => 'upload'
                    ),
                    array (
                    'id' => 'front-img',
                    'label' => 'Voorgrond afbeelding',
                    'type' => 'upload'
                    ),
                    array (
                    'id' => 'img_pos',
                    'label' => 'Voorgrond afbeelding positie',
                    'type' => 'select',
                    'choices' => array (
                        array (
                        'label' => 'Boven content',
                        'value' => 'over-content'
                        ),
                        array (
                        'label' => 'Onder content',
                        'value' => 'below-content'
                        )
                    )
                    ),
                    array (
                    'id' => 'content',
                    'label' => 'Content',
                    'type' => 'textarea'
                    ),
                    array (
                    'id' => 'link_rel_page',
                    'label' => 'Button related page',
                    'type' => 'custom-post-type-select',
                    'post_type' => 'page,post,project,klant,nieuws,dienst,product'
                    ),
                    array (
                    'id' => 'link_url',
                    'label' => 'Button URL',
                    'type' => 'text',
                    'post_type' => ''
                    ),
                    array (
                    'id' => 'link_title',
                    'label' => 'Button title',
                    'type' => 'text'
                    ),
                    array (
                    'id' => 'link_text',
                    'label' => 'Button tekst',
                    'type' => 'text'
                    ),
                    array (
                    'id' => 'classes',
                    'label' => 'Classes',
                    'type' => 'text'
                    )
                )
                )
           )
        )
    ); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}
 
function page_template_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'page_settings',
		  'title'       => 'Pagina settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
           'label' => 'Pagina introductie',
           'id'  => 'intro',
           'type' => 'wysiwyg',
           'desc' => 'Introductie content voor deze pagina'
           )
		)
        ),
        array(
		  'id'          => 'page_sidebar_settings',
		  'title'       => 'Pagina sidebar instellingen',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array(
            'id'          => 'sidebar-items',
            'label'       => 'Sidebar items',
            'desc'        => '',
            'std'         => '',
            'type'        => 'list-item',
            'rows'        => '',
            'taxonomy'    => '',
            'class'       => '',
            'settings'    => array (
                array (
                    'id' => 'item-content',
                    'label' => 'Content',
                    'type' => 'wysiwyg',
                    'desc' => ''
                ),
                array (
                   'id' => 'page-links',
                   'label' => 'Pagina linkbox',
                   'type' => 'custom-post-type-checkbox',
                   'post_type' => 'any',
                   'desc' => ''
                ),
                array (
                    'id' => 'item-classes',
                    'label' => 'Classes',
                    'type' => 'text',
                    'desc' => ''
                )
            )
            )
            )
            ),
            array(
            'id'          => 'cta_settings',
            'title'       => 'CTA settings',
            'pages' => array('page'),
            'context'     => 'normal',
            'priority'    => 'high',
            'fields'      => array( 
            array (
            'id' => 'cta_title',
            'label' => 'CTA titel',
            'type' => 'text',
            'desc' => ''
            ),
            array (
            'id' => 'cta_content',
            'label' => 'CTA Content',
            'type' => 'wysiwyg',
            'desc' => ''
            ),
            array (
            'id' => 'cta_button_url',
            'label' => 'CTA button url',
            'type' => 'custom-post-type-select',
            'post_type' => 'page,post,project,klant,nieuws,dienst,product',
            'desc' => ''
            ),
            array (
            'id' => 'cta_button_text',
            'label' => 'CTA button tekst',
            'type' => 'text',
            'desc' => ''
            ),
            array (
            'id' => 'cta_hide',
            'label' => 'CTA verbergen',
            'type' => 'checkbox',
            'choices' => array (
                array (
                    'label' => 'Ja',
                    'value' => 'ja'
                )
            ),
            'desc' => ''
            )
        )
        )  
        );
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}

function portfolio_template_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'portfolio_settings',
		  'title'       => 'Portfolio settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
           'label' => 'Pagina introductie',
           'id'  => 'intro',
           'type' => 'wysiwyg',
           'desc' => 'Introductie content voor deze pagina'
           )
		)
        ),
        array(
        'id'          => 'cta_settings',
        'title'       => 'CTA settings',
        'pages' => array('page'),
        'context'     => 'normal',
        'priority'    => 'high',
        'fields'      => array( 
        array (
        'id' => 'cta_title',
        'label' => 'CTA titel',
        'type' => 'text',
        'desc' => ''
        ),
        array (
        'id' => 'cta_content',
        'label' => 'CTA Content',
        'type' => 'wysiwyg',
        'desc' => ''
        ),
        array (
        'id' => 'cta_button_url',
        'label' => 'CTA button url',
        'type' => 'custom-post-type-select',
        'post_type' => 'page,post,project,klant,nieuws,dienst,product',
        'desc' => ''
        ),
        array (
        'id' => 'cta_button_text',
        'label' => 'CTA button tekst',
        'type' => 'text',
        'desc' => ''
        ),
        array (
        'id' => 'cta_hide',
        'label' => 'CTA verbergen',
        'type' => 'checkbox',
        'choices' => array (
            array (
                'label' => 'Ja',
                'value' => 'ja'
            )
        ),
        'desc' => ''
        )
    )
    )
        
	); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}

function contact_template_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'contact_settings',
		  'title'       => 'Contact settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
           'label' => 'Pagina introductie',
           'id'  => 'intro',
           'type' => 'wysiwyg',
           'desc' => 'Introductie content voor deze pagina'
           ),
           array(
           'label' => 'Contact extra box 1 content',
           'id'  => 'contact-box-1',
           'type' => 'wysiwyg',
           'desc' => ''
           ),
           array(
           'label' => 'Contact extra box 2 content',
           'id'  => 'contact-box-2',
           'type' => 'wysiwyg',
           'desc' => ''
           )
		)
        ) 
	); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}

function full_width_template_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'page_settings',
		  'title'       => 'Pagina settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
           'label' => 'Pagina introductie',
           'id'  => 'intro',
           'type' => 'wysiwyg',
           'desc' => 'Introductie content voor deze pagina'
           )
		)
        ),
        array(
		  'id'          => 'full_width_settings',
		  'title'       => 'Full width settings',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
           array(
           'label' => 'Full width content',
           'id'  => 'full-width-content',
           'type' => 'wysiwyg',
           'desc' => ''
           ),
          
		)
        ),
        array(
		  'id'          => 'page_sidebar_settings',
		  'title'       => 'Pagina sidebar instellingen',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array(
            'id'          => 'sidebar-items',
            'label'       => 'Sidebar items',
            'desc'        => '',
            'std'         => '',
            'type'        => 'list-item',
            'rows'        => '',
            'taxonomy'    => '',
            'class'       => '',
            'settings'    => array (
                array (
                    'id' => 'item-content',
                    'label' => 'Content',
                    'type' => 'wysiwyg',
                    'desc' => ''
                ),
                array (
                   'id' => 'page-links',
                   'label' => 'Pagina linkbox',
                   'type' => 'custom-post-type-checkbox',
                   'post_type' => 'any',
                   'desc' => ''
                ),
                array (
                    'id' => 'item-classes',
                    'label' => 'Classes',
                    'type' => 'text',
                    'desc' => ''
                )
        )
        )
        )
        ),
        array(
        'id'          => 'cta_settings',
        'title'       => 'CTA settings',
        'pages' => array('page'),
        'context'     => 'normal',
        'priority'    => 'high',
        'fields'      => array( 
        array (
        'id' => 'cta_title',
        'label' => 'CTA titel',
        'type' => 'text',
        'desc' => ''
        ),
        array (
        'id' => 'cta_content',
        'label' => 'CTA Content',
        'type' => 'wysiwyg',
        'desc' => ''
        ),
        array (
        'id' => 'cta_button_url',
        'label' => 'CTA button url',
        'type' => 'custom-post-type-select',
        'post_type' => 'page,post,project,klant,nieuws,dienst,product',
        'desc' => ''
        ),
        array (
        'id' => 'cta_button_text',
        'label' => 'CTA button tekst',
        'type' => 'text',
        'desc' => ''
        ),
        array (
        'id' => 'cta_hide',
        'label' => 'CTA verbergen',
        'type' => 'checkbox',
        'choices' => array (
            array (
                'label' => 'Ja',
                'value' => 'ja'
            )
        ),
        'desc' => ''
        )
    )
    )
	); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
}

function nieuws_cpt_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'nieuws_settings',
		  'title'       => 'Nieuws settings',
		  'pages' => array('nieuws'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
            'id'          => 'sidebar-items',
            'label'       => 'test',
            'desc'        => '',
            'std'         => '',
            'type'        => 'list-item',
            'rows'        => '',
            'taxonomy'    => '',
            'class'       => '',
            'settings'    => array (
                array (
                    'id' => 'item-content',
                    'label' => 'Content',
                    'type' => 'text',
                    'desc' => ''
                )
            )
            )
		)
        ),
        array(
		  'id'          => 'page_banner_settings',
		  'title'       => 'Pagina banner instellingen',
		  'pages' => array('page'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'banner_background',
           'label' => 'Banner achtergrond afbeelding',
           'type' => 'upload',
           'desc' => ''
           ),
           array (
           'id' => 'banner_content',
           'label' => 'Banner titel',
           'type' => 'text',
           'desc' => ''
           )
           )
        ),
	); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
} 

function project_cpt_meta_boxes() {
  $meta_args_array = array(
		array(
		  'id'          => 'project_settings',
		  'title'       => 'Project settings',
		  'pages' => array('project'),
		  'context'     => 'normal',
		  'priority'    => 'high',
		  'fields'      => array( 
		   array (
           'id' => 'alt_page_title',
           'label' => 'Alternatieve pagina titel',
           'type' => 'text',
           'desc' => 'voeg een alternatieve pagina titel toe'
           ),
           array(
           'label' => 'Introductie',
           'id'  => 'intro',
           'type' => 'wysiwyg',
           'desc' => 'Introductie content'
           ),
            array(
            'label'       => 'Featured project',
            'id'          => 'featured_project',
            'type'        => 'checkbox',
            'choices'     => array (
                array (
                'label' => 'Ja',
                'value' => 'ja'
                )
            ),
            'desc'        => 'Featured project? (weergave buiten portfolio)'
            ),
            array(
            'label'       => 'Has details',
            'id'          => 'project_has_details',
            'type'        => 'checkbox',
            'choices'     => array (
                array (
                'label' => 'Ja',
                'value' => 'ja'
                )
            ),
            'desc'        => 'Heeft dit project uitleg? (anders alleen link)'
            ), 
            array(
            'label'       => 'Datum',
            'id'          => 'date',
            'type'        => 'date-picker'
            ),
            array(
            'label'       => 'Externe URL',
            'id'          => 'project_link_url_external',
            'type'        => 'text'
            ),
            
            array(
            'label'       => 'Link tekst',
            'id'          => 'project_link_text',
            'type'        => 'text'
            )
            )
            ),
            array(
        'id'          => 'cta_settings',
        'title'       => 'CTA settings',
        'pages' => array('page'),
        'context'     => 'normal',
        'priority'    => 'high',
        'fields'      => array( 
        array (
        'id' => 'cta_title',
        'label' => 'CTA titel',
        'type' => 'text',
        'desc' => ''
        ),
        array (
        'id' => 'cta_content',
        'label' => 'CTA Content',
        'type' => 'wysiwyg',
        'desc' => ''
        ),
        array (
        'id' => 'cta_button_url',
        'label' => 'CTA button url',
        'type' => 'custom-post-type-select',
        'post_type' => 'page,post,project,klant,nieuws,dienst,product',
        'desc' => ''
        ),
        array (
        'id' => 'cta_button_text',
        'label' => 'CTA button tekst',
        'type' => 'text',
        'desc' => ''
        ),
        array (
        'id' => 'cta_hide',
        'label' => 'CTA verbergen',
        'type' => 'checkbox',
        'choices' => array (
            array (
                'label' => 'Ja',
                'value' => 'ja'
            )
        ),
        'desc' => ''
        )
    )
    )
	); 
  /* load each metabox */
  foreach( $meta_args_array as $meta_args ) {
    ot_register_meta_box( $meta_args );
  }
} 