<?php
function filter_xml($matches) { 
    return trim(htmlspecialchars($matches[1])); 
} 

$url = 'https://www.connexys.nl/enexispublic/run/wp_xml_feed.startup?p_pub_id=1&p_taal=1&p_key=776866E94653D370FF3447DB2E19562F';

if (($response_xml_data = file_get_contents($url)) === false){
    echo "Error fetching XML\n";
} else { 
   libxml_use_internal_errors(true);
   $response_xml_data = preg_replace_callback('/<!\[CDATA\[(.*)\]\]>/', 'filter_xml', $response_xml_data); 
   $data = simplexml_load_string($response_xml_data);
   if (!$data) { 
       echo "Error loading XML\n";
       foreach(libxml_get_errors() as $error) {
           echo "\t", $error->message;
       }
   } else {
      //print_r($data);
      foreach($data as $key=>$job) {
          var_dump($job);
      }
      
   }
}
?>