<?php
date_default_timezone_set('europe/amsterdam');
$url = "https://www.osintelink.nl";
$orignal_parse = parse_url($url, PHP_URL_HOST);
$get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
$read = stream_socket_client("ssl://".$orignal_parse.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
$cert = stream_context_get_params($read);
$certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);

//print_r($certinfo);
$validFrom = date('Y-m-d', $certinfo['validTo_time_t']);
echo $validFrom . '<br>';

$currentDate = date('Y-m-d');

$diff = abs(strtotime($validFrom) - strtotime($currentDate));

$years = floor($diff / (365*60*60*24));
$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

printf("%d years, %d months, %d days\n", $years, $months, $days);

//var_dump($cont["options"]["ssl"]["peer_certificate"]);

?>