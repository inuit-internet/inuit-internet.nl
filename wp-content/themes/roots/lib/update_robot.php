<?php
define('WP_INSTALLING', true);
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . '/wp-load.php' );

function fire_cronfile($url) { 
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,60);
    $rawdata=curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $alldata = 'response: ' . $rawdata . ' httpCode: ' . $httpCode;
    print curl_error($ch);
    curl_close($ch);
    return $alldata; 
}

function fire_dbrepair($repair_url) { 
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL,$repair_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_TIMEOUT, 240);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT,true);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,240);
    $rawdata=curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $alldata = 'response: ' . $rawdata . 'httpCode: ' . $httpCode;
    print curl_error($ch);
    curl_close($ch);
    return $alldata; 
}

function process_updates() {
    $ot_array = get_option('option_tree');
    $subscriptions = $ot_array['update-subscriptions'];
    $mailcontent = "";
    
    foreach($subscriptions as $subscription) {
        $label = $subscription['title'];
        $url = $subscription['url']; //if !empty > run cronfile
        $repair_url = $subscription['db-repair-url']; //if !empty > perform db optimize + repair
        
        if (!empty($url) && function_exists('fire_cronfile')) {
            $fireresult = fire_cronfile($url);
            echo $label . ' >> ' . $url . ' >> ' . $fireresult . '<br >';
            $mailcontent .= $label . ' >> ' . $url . ' >> ' . $fireresult . '<br><br>';
        }
        
        if (!empty($repair_url) && function_exists('fire_dbrepair')) {
            $dbresult = fire_dbrepair($repair_url);
            echo $label . ' >> ' . $repair_url . ' >> ' . $fireresult . '<br >';
            $mailcontent .= $label . ' >> ' . $repair_url . ' >> ' . $fireresult . '<br><br>'; 
        }
    }
    
    $to = 'sebradius@gmail.com';
    $subject = 'Cronjobs uitgevoerd';
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $headers[] = 'From: Inuit Internet Diensten Website <info@inuit-internet.nl>';
    if (function_exists('wp_mail')) {      
        wp_mail($to, $subject, $mailcontent, $headers);
    }  
}
 
process_updates();
exit();
?>