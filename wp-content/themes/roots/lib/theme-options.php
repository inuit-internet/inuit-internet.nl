<?php
/**
 * Initialize the options before anything else. 
 */
add_action( 'admin_init', 'custom_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'contextual_help' => array(
      'content'       => array( 
        array(
          'id'        => 'general_help',
          'title'     => 'General',
          'content'   => '<p>Help content goes here!</p>'
        )
      ),
      'sidebar'       => '<p>Sidebar content goes here!</p>',
    ),
    'sections'        => array(
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
	  array(
		'id'          => 'footer',
        'title'       => 'Footer'
	  ),
	  array(
		'id'          => 'contact',
        'title'       => 'Contact'
	  ),
      array(
		'id'          => 'uptime-robot',
        'title'       => 'Uptime Robot'
	  ),
      array(
		'id'          => 'backup-robot',
        'title'       => 'Backup Robot'
	  ),
      array(
		'id'          => 'update-robot',
        'title'       => 'Update Robot'
	  )
      
      
			// add sections
    ),
    'settings'        => array(	
		array(
        'label'       => 'Leesmeer link tekst',
        'id'          => 'readmore_text',
        'type'        => 'text',
		'section'     => 'general',
        'desc'        => ''
		),
		array(
        'id'          => '404_title',
        'label'       => '404 pagina titel',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => '404_content',
        'label'       => '404 pagina content',
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array ( 
            'id' => 'bedrijfsnaam',
            'label' => 'Bedrijfsnaam',
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'adres',
            'label' => 'Adres',
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'postcode',
            'label' => 'Postcode',
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'stad',
            'label' => 'Stad',
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'tel',
            'label' => 'Telefoon',
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'email',
            'label' => 'E-mail', 
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'kvk',
            'label' => 'KvK nummer', 
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'iban',
            'label' => 'IBAN', 
            'type' => 'text',
            'section' => 'contact'
        ),
        array (
            'id' => 'btw',
            'label' => 'BTW nummer', 
            'type' => 'text',
            'section' => 'contact'
        ),
	    array(
        'id'          => 'footer_newsletter_box',
        'label'       => 'Footer content box',
        'desc'        => '',
        'std'         => '',
        'type'        => 'wysiwyg',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => ''
      ),
	  array(
        'id'          => 'social_media',
        'label'       => 'Social Media',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'settings'    => array (
			array (
			'id' => 'url',
			'label' => 'URL',
			'type' => 'text'
			),
			array (
			'id' => 'link_title',
			'label' => 'Link titel',
			'type' => 'text'
			),
			array (
			'id' => 'screen_reader_text',
			'label' => 'Screen reader titel',
			'type' => 'text'
			)
		)
      ),
	  array(
        'id'          => 'footer_logos',
        'label'       => 'Footer logo\'s',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'footer',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
		'settings'    => array (
			array (
			'id' => 'logo_img',
			'label' => 'Afbeelding',
			'type' => 'upload'
			),
			array (
			'id' => 'logo_alt',
			'label' => 'Alt tekst',
			'type' => 'text'
			),
			array (
			'id' => 'logo_link_url',
			'label' => 'Logo link url',
			'type' => 'text'
			),
			array (
			'id' => 'logo_link_text',
			'label' => 'Logo link tekst',
			'type' => 'text'
			),
			array (
			'id' => 'logo_link_attr',
			'label' => 'Logo link attributes',
			'type' => 'text'
			)
		)
    ),
    array(
        'id'          => 'sidebar-items-default',
        'label'       => 'Default Sidebar Items',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'rows'        => '',
        'section'     => 'general',
        'taxonomy'    => '',
        'class'       => '',
        'settings'    => array (
            array (
                'id' => 'item-content',
                'label' => 'Content',
                'type' => 'wysiwyg',
                'desc' => ''
            ),
            array (
               'id' => 'page-links',
               'label' => 'Pagina linkbox',
               'type' => 'custom-post-type-checkbox',
               'post_type' => 'any',
               'desc' => ''
            ),
            array (
                'id' => 'item-classes',
                'label' => 'Classes',
                'type' => 'text',
                'desc' => ''
            )
        )
    ),
    array(
        'id'          => 'referenties',
        'label'       => 'Referenties',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'rows'        => '',
        'section'     => 'general',
        'taxonomy'    => '',
        'class'       => '',
        'settings'    => array (
            array (
                'id' => 'referentie',
                'label' => 'Referentie',
                'type' => 'wysiwyg',
                'desc' => ''
            )
        )
    ),
    array(
        'id'          => 'uptime-robot-subscriptions',
        'label'       => 'Uptime robot subscriptions',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'rows'        => '',
        'section'     => 'uptime-robot',
        'taxonomy'    => '',
        'class'       => '',
        'settings'    => array (
            array (
                'id' => 'url',
                'label' => 'Website URL',
                'type' => 'text',
                'desc' => 'website to check'
            ),
            array (
               'id' => 'alert-email-adres',
               'label' => 'Alert email adresses',
               'type' => 'text',
               'desc' => 'comma separated'
            )
        )
    ),
    array(
        'id'          => 'backup-subscriptions',
        'label'       => 'Backup robot subscriptions',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'rows'        => '',
        'section'     => 'backup-robot',
        'taxonomy'    => '',
        'class'       => '',
        'settings'    => array (
            array (
                'id' => 'url',
                'label' => 'Backup URL',
                'type' => 'text',
                'desc' => 'URL to fire backup'
            ),
            array (
               'id' => 'notify-email-adres',
               'label' => 'Notify email adresses',
               'type' => 'text',
               'desc' => 'comma separated'
            ),
            array (
               'id' => 'notify',
               'label' => 'Send backup notification?',
               'type' => 'checkbox',
               'choices' => array (
                    array(
                        'label' => 'Yes',
                        'value' => 'yes'
                    )
               ),
               'desc' => ''
            )
        )
    ),
    array(
        'id'          => 'update-subscriptions',
        'label'       => 'Update subscriptions',
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'rows'        => '',
        'section'     => 'update-robot',
        'taxonomy'    => '',
        'class'       => '',
        'settings'    => array (
            array (
                'id' => 'url',
                'label' => 'Cronfile URL',
                'type' => 'text',
                'desc' => 'URL to fire cronfile'
            ),
            array (
                'id' => 'db-repair-url',
                'label' => 'Database repair URL',
                'type' => 'text',
                'desc' => 'URL to fire db repear file'
            ),
        )
    )
    )
  );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}
?>