<?php
define('WP_INSTALLING', true);
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . '/wp-load.php' );

function update_cats($cat1,$cat2) { // set meta pub date if empty
    $posts = get_posts(array(
        'post_type' => 'nieuws',
        'posts_per_page' => -1,
        'category' => $cat1
    )
    ); 
    if (!empty($posts)) {
        $catarray = array();
        $catarray[] = $cat2;
        foreach($posts as $p) {
            wp_set_post_categories( $p->ID, $catarray, true );
            echo 'categorie ID '.$cat2.' toegevoegd aan post <strong>' . $p->post_name . '</strong><br />';
        }
    }
}

$submitted = $_POST['submitted'];
if ($submitted == 'submitted') {
    $cat1 = $_POST['cat1'];
    $cat2 = $_POST['cat2'];
    update_cats($cat1,$cat2);
}
?>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
    </head>
    <body>
        <div style="background:#efefef;padding:30px;margin-left:30px;">
            <h3>Extra categorie toevoegen aan alle artikelen met de geselecteerde uitgangscategorie</h3>
            <br />
            <?php
            $terms = get_terms( array(
                'taxonomy' => 'category',
                'hide_empty' => false,
                'offset' => 0,
                'get' => 'all',
            ) );
            ?>
            <form name="cats" id="cats" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
                <label for="cat1">Uitgangscategorie</label><br />
                <select name="cat1">
                    <?php
                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :
                    foreach ( $terms as $term ) :
                    ?>
                    <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                    <?php
                    endforeach;
                    endif;
                    ?>
                </select><br /><br />
                <label for="cat2">Toevoegen</label><br />
                <select name="cat2">
                    <?php
                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :
                    foreach ( $terms as $term ) :
                    ?>
                    <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                    <?php
                    endforeach;
                    endif;
                    ?>
                </select>
                <input type="hidden" name="submitted" value="submitted"/> 
                <br /><br />
                <input type="submit" value="Categorie toevoegen"/> 
            </form>
            <br />
            <a href="">Nieuwe toevoegen</a>
        </div>
    </body>
</html>