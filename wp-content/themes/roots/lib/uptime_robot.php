<?php
//$realpath = realpath(dirname(__FILE__));
require_once("/home/gruiters/domains/inuit-internet.nl/public_html/wp-load.php");

function check_website_status($url) {
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_NOBODY, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10); 
curl_setopt($ch, CURLOPT_TIMEOUT,10); //timeout in seconds
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_exec($ch);
$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
if (200==$retcode) {
    $check = 200; // 200 ok
    return $check;
} else {
    $check = 'error';
    return $check; //Down
}
}

function email_warning($to,$subject,$message,$label) {
    if (empty($to)|| empty($subject)|| empty($message)) {
          $to      = 'sebradius@gmail.com';
          $subject = "er is iets mis met monitor {$label}";
          $message = "er is iets mis met monitor {$label}";
    }
  
    $headers = 'From: website-monitor@inuit-internet.nl' . "\r\n" .
    'MIME-Version: 1.0' . "\r\n" .
    'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
    'Reply-To: noreply@inuit-internet.nl' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);
}

function process_subscriptions() {
    $ot_array = get_option('option_tree');
    $subscriptions = $ot_array['uptime-robot-subscriptions'];
    //var_dump($subscriptions);
    foreach ($subscriptions as $subscription) {
        $label = $subscription['title'];
        $labeldesig = $label.'_prevstate';
        $prevstate = get_option($labeldesig);
        //echo $prevstate; 
        $url = $subscription['url'];
        $alert = $subscription['alert-email-adres'];
        $to = "sebradius@gmail.com,$alert";
        $check = check_website_status($url);
        //echo $check;
        if ( ($check == 'error' && $prevstate == 200) || $check == 'error' && empty($prevstate) ) {
            update_option( $labeldesig, 'error' );
            $subject = "Website niet bereikbaar: {$label}";
            $message = "Hallo, <br><br> De monitor {$label} ({$url}) is momenteel NIET BEREIKBAAR.<br><br>We laten het weten wanneer we detecteren dat de website weer bereikbaar is.<br><br>Vriendelijke groet, <br /><br />Inuit Internet Diensten<br>http://www.inuit-internet.nl";
            email_warning($to,$subject,$message,$label);
        }
        elseif ( $check == '200' && $prevstate == 'error' ) {
            update_option( $labeldesig, 200 );
            $subject = "Website weer bereikbaar: {$label}";
            $message = "Hallo, <br><br> De monitor {$label} ({$url}) is momenteel weer bereikbaar.<br><br>Vriendelijke groet, <br /><br />Inuit Internet Diensten<br>http://www.inuit-internet.nl";
            email_warning($to,$subject,$message,$label);
        }  
        else  {
            continue;
        }        
    }
}

process_subscriptions();
exit();
?>