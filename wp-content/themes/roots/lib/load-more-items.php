<?php 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . '/wp-load.php' );

if (session_status() == PHP_SESSION_NONE) {
    session_start(); 
}

$output = '';
$siteurl = get_option('siteurl');
$start = $_GET['start'];
$type = $_GET['type'];
$nextstart = $start + 3;
  
if (!empty($_SESSION['selected_categories'])) {
    //var_dump($_SESSION['selected_categories']);
    
    $catarray = array(); 
    foreach ($_SESSION['selected_categories'] as $key => $value) {
        $catarray[] = $key;
    }
    $args = array(
        'post_type' => $type,
        'posts_per_page' => 3,
        'orderby' => 'menu_order',
        'post_status' => 'publish',
        'offset' => $start,   
        'sort_order' => 'desc',
        'category__in' => $catarray
    );
}

else {
    $args = array(
        'post_type' => $type,
        'posts_per_page' => 3,
        'orderby' => 'menu_order', 
        'offset' => $start,
        'post_status' => 'publish',
        'sort_order' => 'desc',
    );
}

$wp_query = new WP_Query( $args  );
$found = $wp_query->found_posts;

if ($found > 0) :
$output .= '<div class="row hide">';
ob_start(); 
while ( $wp_query ->have_posts() ) :
$wp_query ->the_post(); 
?>
<div class="column portfolio-item">
    <article class="article">
        <?php
        if (has_post_thumbnail()) : 
        $projectlink = get_post_meta($post->ID,'project_link_url_external',true);
        $projecthasdetails = get_post_meta($post->ID,'project_has_details',true);
        $target = '_blank';
        
        if ($projecthasdetails[0] == 'ja') {
            $projectlink = get_permalink($post->ID);
            $target = '_self'; 
        }
     
        if (!empty($projectlink)) :
        $projectlinktitle = get_post_meta($post->ID,'project_link_text',true);
        ?>
        <a href="<?php echo $projectlink; ?>" target="<?php echo $target; ?>" rel="nofollow" title="<?php if (!empty($projectlinktitle)) { echo $projectlinktitle; } else { _e('Bekijk dit project'); } ?>">
        <?php
        endif;
        $thumb_id = get_post_thumbnail_id($post->ID);
        $thumb_url = wp_get_attachment_image_src( $thumb_id, 'list-box-img', false );
        $alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
        ?>
            <figure>
                <img src="<?php echo $thumb_url[0]; ?>" height="<?php echo $thumb_url[2]; ?>" width="<?php echo $thumb_url[1]; ?>" alt="<?php echo $alt; ?>">
                <?php 
                if (!empty($projectlink)) : 
                ?>
                <figcaption></figcaption>
                <i class="fa fa-arrows-alt"></i>
                <?php 
                endif;  
                ?>
            </figure>
        <?php
        if (!empty($projectlink)) :
        ?>
        </a>
        <?php
        endif;
        endif;
        ?>
        <div class="content">
            <h4><?php $alttitle = get_post_meta($post->ID,'alt_page_title',true); if (!empty($alttitle)) {echo $alttitle;} else {the_title();} ?></h4>
            <?php
            $intro = get_post_meta($post->ID,'intro',true);
            if (!empty($intro)) {
            $firsttag = substr($intro, 0, 3);  
            if ($firsttag != '<p>') {
                $intro = '<p>' .$intro. '</p>';
            }
            echo $intro;
            }
            ?>
        </div><!-- /content -->
    </article>
</div><!-- /column -->
<?php
endwhile;
$output .= ob_get_clean();
if ($nextstart < $found) {
    $output .=  '<div class="newloader hidden"><i class="fa fa-circle-o-notch loader" data-grid-url="'.$siteurl.'/wp-content/themes/roots/lib/load-more-items.php" data-grid-type="'.$type.'" data-grid-start="'.$nextstart.'"></i></div>';
}  
else {
    $output .=  '<div class="newloader hidden"><span class="stoploading"></span></div>';
}            
$output .= '</div><!-- /row -->'; 
endif;  
echo $output;     
?>