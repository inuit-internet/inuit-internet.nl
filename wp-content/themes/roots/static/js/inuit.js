window.INUIT = new function(a) {
    "use scrict";
    var b = this,
        c = b.IN = function() {
            this.initBoxTrigger(), this.initNavigation(), this.initEqualheights(), this.initCarousel(), this.initScrollHeader(), this.initPlaceholders(), this.initRefslider(), this.initLazyLoader()
        };
    c.prototype.initPlaceholders = function() {
        var b = "placeholder" in document.createElement("input");
        b || (a("[placeholder]").focus(function() {
            var b = a(this);
            b.val() == b.attr("placeholder") && (b.val(""), b.removeClass("placeholder"))
        }).blur(function() {
            var b = a(this);
            ("" == b.val() || b.val() == b.attr("placeholder")) && (b.addClass("placeholder"), b.val(b.attr("placeholder")))
        }).blur(), a("[placeholder]").parents("form").submit(function() {
            a(this).find("[placeholder]").each(function() {
                var b = a(this);
                b.val() == b.attr("placeholder") && b.val("")
            })
        })), a(".gfield").each(function() {
            var b = a(this).find("label").text().toLowerCase().replace("*", "");
            b = "" + b, a(this).find("input[type=text],input[type=email],input[type=password],textarea,input[type=tel]").attr("placeholder", b)
        })
    }, equalheight = function(b) {
        var c, d = 0,
            e = 0,
            f = new Array;
        a(b).each(function() {
            if (c = a(this), a(c).height("auto"), topPostion = c.position().top, e != topPostion) {
                for (currentDiv = 0; currentDiv < f.length; currentDiv++) f[currentDiv].height(d);
                f.length = 0, e = topPostion, d = c.height(), f.push(c)
            } else f.push(c), d = d < c.height() ? c.height() : d;
            for (currentDiv = 0; currentDiv < f.length; currentDiv++) f[currentDiv].height(d)
        })
    }, c.prototype.initEqualheights = function() {
        equalheight(".homepage-intro-list li p"), a(window).resize(function() {
            equalheight(".homepage-intro-list li p")
        })
    }, c.prototype.initLazyLoader = function() {
        var b = a(window).height(),
            c = b / 2,
            d = a(window).scrollTop(),
            e = a(".box-items.portfolio");
        if (e.length) {
            var f = a(".loader"),
                g = a(".loading-bar"),
                h = f.attr("data-grid-type"),
                i = f.attr("data-grid-url");
            i && a(document).bind("scroll", function() {
                if (_cur_top = a(window).scrollTop(), window.scrollY > c && d < _cur_top && !a(".isloading").length && !a(".stoploading").length) {
                    a(".loader").addClass("loading"), g.addClass("isloading");
                    var b = g.find(".loader").attr("data-grid-start");
                    a.ajaxSetup({
                        cache: !1
                    }), a.ajax({
                        data: {
                            start: b,
                            type: h
                        },
                        url: i,
                        success: function(b) {
                            e.append(b);
                            var c = e.find(".newloader").html();
                            e.find(".newloader").remove(), g.html(c), a(".loader").removeClass("loading"), g.removeClass("isloading"), a(".box-items.portfolio .row:last").fadeIn("slow").removeClass("hide")
                        }
                    })
                }
                d = _cur_top
            })
        }
    }, c.prototype.initNavigation = function() {
        var b = (a(".nav-main"), a(".nav-link")),
            c = a(".wrap");
        b.click(function(d) {
            return d.preventDefault(), a("body").scrollTo("0", 0), b.toggleClass("active"), c.toggleClass("active"), a("html").toggleClass("active"), !1
        }), a(".nav-main> .menu > li").each(function() {
            var b = a(this).find(".submenu"),
                c = a(b).width(),
                d = a(b).height();
            a(this).hover(function() {
                a(b).length > 0 && !a(".nav-link").is(":visible") && (a(".link-border").css("visibility", "hidden"), b.css({
                    width: "0px",
                    height: "0px",
                    left: c / 2 + "px"
                }).show(0).animate({
                    width: c + "px",
                    height: d + "px",
                    left: -c / 4
                }, {
                    duration: 175,
                    complete: function() {
                        b.css({
                            background: "#efefef"
                        }).delay(250).find(">ul").show(0)
                    }
                }))
            }, function() {
                a(b).length > 0 && !a(".nav-link").is(":visible") && (b.hide(0).css({
                    background: "transparent"
                }).find(">ul").hide(0), a(".link-border").css("visibility", "visible"))
            })
        })
    }, c.prototype.initGoTop = function() {
        a("body").append('<a href="#header" title="Ga terug naar boven">T</a>');
        var b = a('a[title*="terug naar boven"]'),
            c = 0;
        a(document).bind("scroll", function() {
            window.scrollY > 20 && 0 == c && (c = 1, b.animate({
                opacity: 1
            })), window.scrollY < 20 && 1 == c && (c = 0, b.animate({
                opacity: 0
            }))
        }), b.click(function(b) {
            b.preventDefault(), a("body").scrollTo("0", 600)
        })
    }, c.prototype.initScrollHeader = function() {
        var b = 0,
            c = a(".header");
        a(document).bind("scroll", function() {
            window.scrollY > 0 && 0 == b && (b = 1, a(c).addClass("scrolling").find(".link-border").css("visibility", "hidden")), 0 == window.scrollY && 1 == b && (b = 0, a(c).removeClass("scrolling").find(".link-border").css("visibility", "visible"))
        })
    }, c.prototype.initBoxTrigger = function() {
        a(".homepage-intro-list li").each(function() {
            var b = a(this).find(".btn-1"),
                c = a(this),
                d = "0",
                e = b.outerHeight(!0);
            b.css("bottom", -e + "px"), a(c).find("a").length > 0 && a(this).hover(function() {
                a(b).animate({
                    bottom: d
                }, 1, "easeInExpo", function() {})
            }, function() {
                a(b).animate({
                    bottom: -e + "px"
                }, 50, "easeOutExpo", function() {})
            })
        })
    }, c.prototype.initCarousel = function() {
        a(".carousel").flexslider({
            namespace: "carousel-",
            animation: "fade",
            selector: ".carousel-images > li",
            slideshowSpeed: 7e3,
            animationSpeed: 600,
            useCSS: !0,
            pauseOnHover: !0,
            touch: !0,
            controlNav: !1,
            directionNav: !0
        }), a("a").each(function() {
            var b = new RegExp("/" + window.location.host + "/");
            b.test(this.href) || a(this).click(function(a) {
                a.preventDefault(), a.stopPropagation(), window.open(this.href, "_blank")
            })
        }), a("#content a[href^='https://']").attr("target", "_blank")
    }, c.prototype.initRefslider = function() {
        a(".referenties").flexslider({
            namespace: "referentie-",
            animation: "fade",
            selector: ".referentie-items > li",
            slideshowSpeed: 7e3,
            animationSpeed: 600,
            useCSS: !0,
            pauseOnHover: !0,
            touch: !1,
            controlNav: !1,
            directionNav: !1
        })
    }, c.prototype.initAccordion = function() {
        var b = a(".accordion .accordion-content").css({
            height: 0
        });
        a(".accordion > .accordion-heading > a").click(function() {
            if ($this = a(this), $target = $this.parent().next(), $target.hasClass("active")) a(this).removeClass("active"), b.removeClass("active").transition({
                height: 0
            }, 450, "snap");
            else {
                a(".accordion-heading a").removeClass("active");
                var c = a(".content", $target).height();
                b.removeClass("active").animate({
                    height: 0
                }, 400), $target.addClass("active").transition({
                    height: c
                }, 450, "snap"), a(this).addClass("active")
            }
            return !1
        })
    }, c.prototype.initTooltips = function() {
        a(".list-logos, .list-clients").tooltip({
            selector: "a",
            placement: "top"
        }), a(".carousel .carousel-images li").tooltip({
            selector: "span",
            placement: "right"
        })
    }
    /*c.prototype.initGoogleMaps = function() {
        function b() {
            var a = new google.maps.LatLng(51.647491, 5.630102),
                b = new google.maps.LatLng(51.65869, 5.626878),
                c = {
                    zoom: 12,
                    center: b,
                    disableDefaultUI: !0,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                },
                d = new google.maps.Map(document.getElementById("map"), c),
                e = ['<form action="https://maps.google.com/maps" method="get" target="_blank">', '<input type="hidden" class="hidden" name="daddr" value="Muntmeester 477, Uden" /> ', '<input type="submit" class="btn-1 routebtn" value="Plan mijn route" />', "</form>"].join(""),
                f = new google.maps.InfoWindow({
                    content: e
                }),
                g = new google.maps.Marker({
                    position: a,
                    map: d,
                    icon: "https://maps.google.com/mapfiles/ms/icons/blue-dot.png",
                    title: "Inuit Internet Diensten"
                });
            google.maps.event.addListener(g, "click", function() {
                f.open(d, g)
            });
            var h = [{
                stylers: [{
                    hue: "#36A6DA"
                }]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [{
                    lightness: 100
                }, {
                    visibility: "simplified"
                }]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [{
                    visibility: "on"
                }]
            }];
            d.setOptions({
                styles: h
            })
        }
        a("body").hasClass("contact") && (window.onload = b)
    }*/
}(jQuery), $(document).ready(function() {
    new INUIT.IN
});