/**
 * Loads new content when a placeholder is at a certain position in the viewport.
 */

define(function() {

    'use strict';

    var SETTINGS = {
        //TODO CHANGE THESE
        linkSelector: '.lazy-load-placeholder',
        contentSelector: '.search-result-content ul',
        classLoading: 'is-loading',
        offset: 200 // 200px below the viewport
    };

    var LazyLoader = function() {
        this.load_link = $(SETTINGS.linkSelector);
        this.content = $(SETTINGS.contentSelector);
        this.initLoadLink(this.load_link);

        // check if the load link is in the viewport at page-load.
        if (this.load_link) {
            var rect = this.load_link[0].getBoundingClientRect();
            if (rect.top > 0 && rect.top < window.innerHeight) {
                this.getContent({currentTarget: this.load_link});
            }
            this.scrollListener = this.onScroll.bind(this);
            $(window).on('scroll', this.scrollListener);
        }
    };

    LazyLoader.prototype.getContent = function() {
        // get link position
        if (this.load_link) {
            var y = this.load_link[0].getBoundingClientRect().top;
            if (y + this.settings.offset > window.innerHeight) {
                //load content
                this.getContent();
            }
        }
    };

    /**
     * Get the new content.
     * @memberof LazyLoader
     */
    LazyLoader.prototype.getContent = function() {
        var url = this.load_link[0].getAttribute('data-grid-url');
        if (url) {
            this.load_link.addClass(SETTINGS.classLoading);

            $.ajax(url, {
                success: function(response) {
                    this.processContent(response);
                }.bind(this)
            });
        }
    };

    /**
     * Add behaviour to the lazy loader link.
     * @param {Element} element The lazy load link.
     * @memberof LazyLoader
     */
    LazyLoader.prototype.initLoadLink = function(element) {
        // stop any clicks on the link.
        element.on('click', function(event) {
           event.preventDefault();
        });
    };

    /**
     * Remove event from the link and remove it from the DOM.
     * @memberof LazyLoader
     */
    LazyLoader.prototype.destroyLoadLink = function() {
        this.load_link.off('top.enter.viewport');
        this.load_link.remove();
        this.load_link = null;
    };

    /**
     * Process the loaded content.
     * Insert it into the page.
     * Look for the next lazy load link and insert it into the page.
     * @param {String} html The response text from the ajax request.
     * @memberof LazyLoader
     */
    LazyLoader.prototype.processContent = function(html) {
        var tmp = $('<div/>');
        tmp.innerHTML = html;

        // get the content and the next lazy-load-link.
        var content = $(SETTINGS.contentSelector, tmp),
            load_link = $(SETTINGS.linkSelector, tmp);

        if (content.length) {
            // insert the content
            content.insertAfter(this.load_link);

            // remove the 'old' lazy-load-link'.
            this.destroyLoadLink();
            if (load_link.length) {
                // initialize the new lazy-load-link'.
                load_link.insertAfter(content);
                this.initLoadLink(load_link);
            } else {
                // stop listening for scroll events.
                $(window).off('scroll', this.scrollListener);
            }

            this.content = content;

            tmp = null;
        }
    };

    return LazyLoader;
});