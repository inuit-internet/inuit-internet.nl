//
//  Basic.scss
//

// =============================================================================
// HTML5 display definitions
// =============================================================================
article, aside, details,
figcaption, figure, footer,
header, hgroup, nav, section {
    display: block;
}

audio, canvas, video {
    display: inline-block;
    *display: inline;
    *zoom: 1;
}

audio:not([controls]) {
    display: none;
}

[hidden] {
    display: none;
}

// =============================================================================
// Base
// =============================================================================
html {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
    font-size: 100%;
    -webkit-font-smoothing: antialiased;
}

body {
    font-family: $baseFontFamily;
    @include font-size(14);
    margin: 0;
    color: $textColor;
    background: $bodyBackground;
    line-height: 1.538461538em;
    border-top: 5px solid #F96236;
    -webkit-font-smoothing: antialiased;
}

::-moz-selection {
    background: lighten($c2, 10%);
    color: $white;
    text-shadow: none;
}

::selection {
    background: lighten($c2, 10%);
    color: $white;
    text-shadow: none;
}

// =============================================================================
// Links
// =============================================================================
a {
    color: $c1;
    text-decoration: none;
    font-family:"Avenir LT W01 35 Light";
}

a:focus {
    outline: thin dotted;
}

a:hover, a:active {
    outline: 0;
    text-decoration: underline;
    color: $linkColorHover;
}

/* =============================================================================
   Typography
   ========================================================================== */
abbr[title]         { border-bottom: 1px dotted; }
b, strong           { font-weight: bold; }
blockquote          { margin: 1em 40px; }
q                   { quotes: none; }
q:before, q:after   { content: ""; content: none; }
small               { font-size: 85%; }
sub, sup            { font-size: 75%; vertical-align: baseline; }
sup                 { top: -0.3em; position: relative; }

h1, h2, h3, h4, h5 {
    font-weight: normal;
    margin: 0 0 0.5em;

    strong {
        display: block;
        font-weight: normal;
    }
}

h1 {
    font-size: 5em; /* 70 / 14 */
    line-height: 1;
}

h2 {
    font-size: 2.571428571em; /* 20 / 14 */
    line-height: 1.2em;
}

h3 {
    font-size: 2.692307692em;
    line-height: 1em;
    margin: 0 0 0.8em;
}

h4 {
    font-size: 1em;
    line-height: 20px;
}

p {
    font-size: 1em;
    line-height: 1.785714286em;
    margin: 0 0 0.5em;
    font-family:"Avenir LT W01 35 Light";
}


h1, h2, h3, h4, h5 {
    font-family: "Avenir LT W01 35 Light";

    strong {
        font-family: "Avenir LT W01 85 Heavy";
    }
}

/* =============================================================================
   Embedded content
   ========================================================================== */
img {
    border: 0;
    -ms-interpolation-mode: bicubic;
    vertical-align: middle;
}

svg:not(:root){
    overflow: hidden;
}

/* =============================================================================
   Figures
   ========================================================================== */
figure {
    margin: 0;
}

/* =============================================================================
   Chrome Frame Prompt
   ========================================================================== */
.msg-warning {
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 500;
    margin: 0.2em 0;
    color: black;
    padding: 0.8em 0;
    text-align: center;
    height: 40px;
    border-bottom: 1px solid #0A7AA8;
    background: #fff url("../img/chromeframe.jpg") repeat-x left bottom;
}
    .msg-warning > strong {
        display: block;
        color: $c2;
        font-size: 18px;
        margin: 0 0 5px;
        font-weight: normal;
    }
    .msg-warning a {
        color: #000;
        font-weight: bold;
        text-decoration: underline;
    }